﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.

//defining main namespace
var jnbs = jnbs || {};

//adding esentioal function string
(function () {
    //startsWith
    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function (searchString, position) {
            position = position || 0;
            return this.substr(position, searchString.length) === searchString;
        };
    }
})();

(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        localStorage.clear();
        document.addEventListener( 'pause', onPause.bind( this ), false );
        document.addEventListener('resume', onResume.bind(this), false);

        $.mobile.toolbar.prototype.options.updatePagePadding = false;
        $.mobile.toolbar.prototype.options.hideDuringFocus = "";
        $.mobile.toolbar.prototype.options.tapToggle = false;
        //$.mobile.pushStateEnabled = false;
        //$.mobile.defaultPageTransition = 'none';

        // Fix for iOS status bar overlap
        if (window.device.platform === 'iOS' && parseFloat(window.device.version) >= 7.0) {
            StatusBar.overlaysWebView(false);
            StatusBar.backgroundColorByHexString("#0C62AA");
        }

        $("body>[data-role='panel']").panel();
        document.addEventListener("backbutton", onBackKeyDown, false);
        setCurrentCountry();
    };
    $(document).on('click', '#IrSend', function () {
          //$.mobile.changePage('views/Transfer/TransferSendTo.html');
        $.mobile.changePage('views/home/home.html');
    });

    $(document).on('click', '#IrSend', function () {
        $.mobile.changePage('views/Transfer/TransferSendTo.html');
        //$.mobile.changePage('views/home/home.html');
    });
    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };
    function onBackKeyDown() {
        // TODO: This application has been reactivated. Restore application state here.
        var sPath = window.location.pathname;
        if (sPath.endsWith("Send.html")) {
            // Muestro el pop
            $("#confirmSignout").popup("open");
        }
        else if (sPath.endsWith("index.html")) {
            navigator.app.exitApp();
        }
        else if (sPath.endsWith("recipientBillTargetList.html") || sPath.endsWith("RecipientListTarget.html") || sPath.endsWith("accountRecipientTargetList.html")) {
            $.mobile.changePage("../../Send/Send.html");
        }
        else if (sPath.endsWith("TransferSendTo.html")) {
            $.mobile.changePage("../Send/Send.html");
        }
        else if (sPath.endsWith("addRecipientsMainMenu.html")) {
            $.mobile.changePage("../Recipients/Recipients.html");
        }
        else
        {
            window.history.back();
        }
    };

    function setCurrentCountry() {
        Helpers.setGeoLocalization(function (response) {
            localStorage.CountryCode = response.CountryCode;
            localStorage.Region = response.Region;
            localStorage.CountryName = response.CountryName;
            console.log("datos de geo obtenidos");
        }, function (dataError) {         
            customErrorDisplay("error calling geolocalization service");
        });

    };

} )();
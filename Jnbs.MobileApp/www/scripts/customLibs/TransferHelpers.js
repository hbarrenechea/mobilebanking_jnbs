﻿var TransferHelpers = (function(_,$) {
    var getRecipientCurrency = function (countryId, callOk, callError) {
      //  var countryId = getCurrentData().address.addressCountry;
        ServiceConnection.getData({
            actionName: 'SystemConfiguration?countryId=' + countryId,
            formData: null,
            successCallback: function(response, status, xhr) {
                //localStorage.RecipientCurrencyCode = response.CurrencyCode;
                //callback();
                callOk(response);
            },
            errorCallback: function(dataError) {
                //showError("Error trying to calling recipient currency");
                callError("Error trying to calling recipient currency");
            },
            typeAction: "GET"

        });
    };

    var fillCurrency = function (htmlHolder, countryCode, callOk, callError, cacheCurrency) {
      
        var url = countryCode == null ? 'RefenceData?referenceType=Currencies'
            : 'CountryLookup?countryId=' + countryCode + '&fakeParam=';
        //localStorage.CountryCode
        if (!cacheCurrency) {
            var currencyVc = new ViewConector({
                container: htmlHolder,
                viewContentType: ViewContentType.SELECT,
                actionName: url,
                actionParameter: null,
                typeAction: "GET",
                configSettings: { value: "CurrencyCode", text: "CurrencyName" }
            });

            currencyVc.execute(callOk, callError);
            return currencyVc;
        }

        cacheCurrency.execute(callOk, callError);
        return cacheCurrency;
    };

    return {
        getRecipientCurrency: getRecipientCurrency,
        fillCurrency: fillCurrency
    }
})(_,$);
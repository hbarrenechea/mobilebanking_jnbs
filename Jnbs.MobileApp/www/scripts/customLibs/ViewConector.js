﻿ViewConector.counterInstances = [];
ViewConector.clearCacheByTag = function(tagName) {
    if (ViewConector.counterInstances && ViewConector.counterInstances.length > 0) {
        var list = ViewConector.counterInstances;
        list.forEach(function(vc) {
            if (vc.tagName == tagName) {
                if (vc.response)
                    vc.response = null;
            }
        });
    }
};
//function ViewConector(container, viewContentType, actionParameter, actionName, selectSettings) {
function ViewConector(request) {
    this.container = request.container;
    this.viewContentType = request.viewContentType;
    this.actionParameter = request.actionParameter;
    this.typeAction = request.typeAction;

    //set text message in loading box. Only use whern "enableLoading==true"
    this.messageLoading = request.messageLoading || "loading data...";
    //set enable show loading box.
    this.enableLoading = (request.enableLoading==undefined ? true:request.enableLoading );
    //set a function for map original response to another.
    this.convertResponse = request.convertResponse;

    //tagName represent a indentifier for this instance.
    if (request.tagName) {
        this.tagName = request.tagName;
        ViewConector.counterInstances.push(this);
    }

    if (request.actionName == undefined && this.viewContentType == ViewContentType.SELECT)
        this.actionName = "banking/consultaEnumerados/getEnumerados";
    else
        this.actionName = request.actionName;

    this.response = null;
    if (request.configSettings != undefined) {
        this.configSettings = {
            value: request.configSettings.value != undefined ? request.configSettings.value : "codigo",
            text: request.configSettings.text != undefined ? request.configSettings.text : "descripcion",
            collection: request.configSettings.collection != undefined ? request.configSettings.collection : undefined
        }
    } else {
        this.configSettings = {
            value: "codigo",
            text: "descripcion",
            collection: undefined
        }
    }

    this.template = request.template;
};


ViewConector.prototype.execute = function (callb, callError) {
    var me = this;

    var commonProcessSucess = function(rsp) {
        var list = rsp;
        if (me.convertResponse) list = me.convertResponse(rsp);
        me.populate(list, me.viewContentType);
        if (callb != undefined)
            callb(list);
        //  $.mobile.loading("hide");
        if (me.enableLoading == true) Helpers.progressBox.stop();
    };
    if (this.response == null) {
       
        if (this.enableLoading == true) Helpers.progressBox.start(this.messageLoading);

        //var me = this;
        ServiceConnection.getData({
            actionName: me.actionName,
            formData: me.actionParameter,
            typeAction: me.typeAction,
            successCallback: function(response, status, xhr) {
                //var list = response;
                //if (me.convertResponse) list = me.convertResponse(response);
                //me.populate(list, me.viewContentType);
                //if (callb != undefined)
                //    callb(list);
               
                //if (me.enableLoading == true) Helpers.progressBox.stop();
                commonProcessSucess(response);

            },
            errorCallback: function(dataError) {
                // $.mobile.loading("hide");
                if (me.enableLoading == true) Helpers.progressBox.stop();
                var text = JSON.stringify(dataError);
                if (callError) callError(text);

                // alert("vcnt->"+text);
            }
        });
    } else {
        //this.populate(this.response, this.viewContentType);
        commonProcessSucess(this.response);
    }
}


var findAttr = function getObjects(obj, key) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key));
        } else if (i == key) {
            objects.push(obj);
        }
    }
    return objects;
}

var findCollection = function getCollection(obj, key) {

    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            if (i == key)
                objects.push(obj);
            else
                objects = objects.concat(getCollection(obj[i], key));
        }
    }
    return objects;
}

ViewConector.prototype.getControlValue = function (obj, key) {
    var results = findAttr(obj, key);
    if (results.length == 0)
        return -1;
    else
        return $(results[0]).attr(key);
}

ViewConector.prototype.populate = function (response, viewContentType) {
    this.response = response;
    switch (viewContentType) {
        case ViewContentType.FORM:
            this.loadForm();
            break;
        case ViewContentType.SELECT:
            this.loadSelect();
            break;
        case ViewContentType.DYNAMIC:
            this.loadDynamic();
            break;
    }
}

ViewConector.prototype.loadForm = function () {
    var controls = this.container.find('.control');
    var me = this;
    $(controls).each(function (index) {
        
        var property = ($(this).attr('data-value')).split(".");
        if (property.length == 1)
            var value = me.getControlValue(me.response, $(this).attr('data-value'));
        else
            var value = me.getControlValue(findCollection(me.response, property[0]), property[1]);

        if (value != -1) {
            if ($(this).is("input")) {
                $(this).val(value);
                return true;
            }
            else {
                $(this).text(value);
                return true;
            }
        }
    });
};

ViewConector.prototype.loadSelect = function () {
    var me = this;
    var items = this.response;
    if (this.configSettings.collection != undefined) {
        collection = findCollection(items, this.configSettings.collection);
        if (collection.length != 0)
            items = $(collection[0]).attr(this.configSettings.collection);
    }
    me.emptySelect(this.container.selector,$);
    if (Array.isArray(items)) {
       
        $.each(items, function (i, item) {
            var itemOption = '<option value="' + $(item).attr(me.configSettings.value) + '">' + $(item).attr(me.configSettings.text) + '</option>';
            $(me.container.selector).append(itemOption);
        });
    }
    else {
        var itemOption = '<option value="' + $(items).attr(this.configSettings.value) + '">' + $(items).attr(this.configSettings.text) + '</option>';
        $(this.container.selector).append(itemOption);
    }

    var valFirst = $(this.container.selector + " option:first").val();
   $(this.container.selector).val(valFirst).change();
};

ViewConector.prototype.emptySelect = function(id, jq) {
    var opciones = [];
   
    jq(id + " > option").each(function() {

        var opt = jq(this).is(":disabled");
        console.log("is disabled " + opt);
        if (opt == false) {
            opciones.push(jq(this));
        }

    });

    console.log(opciones.length);

    opciones.forEach(function(item) {
        item.remove();
    });
};
ViewConector.prototype.renderTemplate = function (obj) {
    var template = $("<div>" + this.template + "</div>");
    var controls = template.find('.control');
    var obj = obj;
    var me = this;

    $(controls).each(function (index) {
        var property = ($(this).attr('data-value')).split(".");
        if (property.length == 1)
            var value = me.getControlValue(obj, $(this).attr('data-value'));
        else
            var value = me.getControlValue(findCollection(obj, property[0]), property[1]);
        if (value != -1) {
            if ($(this).is("input")) {
                $(this).val(value);
                return true;
            }
            else {
                $(this).text(value);
                return true;
            }
        }
    });

    $(this.container.selector).append(template.html());
}

ViewConector.prototype.loadDynamic = function () {

    var items = this.response;
    if (this.configSettings.collection != undefined) {
        collection = findCollection(items, this.configSettings.collection);
        if (collection.length != 0)
            items = $(collection[0]).attr(this.configSettings.collection);
    }

    this.container.empty();
    var me = this;
    if (Array.isArray(items) || items.length!=undefined) {
        $.each(items, function (i, item) {
            me.renderTemplate(item);
        });
    }
    else {
        this.renderTemplate(items);
    }
}

var ViewContentType =
{
    "FORM": "form",
    "SELECT": "select",
    "DYNAMIC": "dynamic" 
}
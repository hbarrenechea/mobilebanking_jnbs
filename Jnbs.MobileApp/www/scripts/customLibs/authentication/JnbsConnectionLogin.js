﻿jnbs.ConnectionLogin = (function(connectionData) {

    var createParamters = function() {
        var state = (Date.now() + Math.random()) * Math.random();
        state = state.toString().replace(".", "");
        var nonce = (Date.now() + Math.random()) * Math.random();
        nonce = nonce.toString().replace(".", "");

        return {
            //scope: 'openid profile email offline_access read write',
            scope: 'openid profile roles email offline_access read write all_claims',
            state: 'random_state',
            nonce: 'random_nonce'
        };
    };

    var tryLogin = function(successFunction, errorFunction) {
        $.oauth2({
            auth_url: connectionData.auth_url, // required
            response_type: 'code id_token token', // required - "code"/"token"
            //  token_url: '',          // required if response_type = 'code'
            //logout_url: '',         // recommended if available
            client_id: connectionData.client_id, // required
            client_secret: connectionData.client_secret,      // required if response_type = 'code'
            redirect_uri: connectionData.redirect_uri, // required - some dummy url
            other_params: createParamters()
//{ scope: 'openid', state: state, nonce: nonce }        // optional params object for scope, state, display...
        }, function (token, code, expires) {
            successFunction(token, code, expires);
        }, function(error, response) {
            errorFunction(error, response);
        });
    };


    return{
        login:tryLogin
    };
})({
    auth_url: envOpenIdQA.UrlBase + 'core/connect/authorize',
    client_id: 'sabra',
    client_secret: 'gruposabra',
    redirect_uri: 'http://localhost:21575/index.html'
});

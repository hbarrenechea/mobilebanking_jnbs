﻿var ErrorBox = (function (jQmobile, hand, $) {


    var closeError, _templateHtml, containerError, setTemplate,
        idErrorTemplateDefault = "#error-template";


    var pullContainer = function (id) {
        if (containerError) return;
        var currentId;
        if (id) {
            currentId = id;
        } else {
            currentId = idErrorTemplateDefault;
        }

        _templateHtml = $(currentId);
        if (_templateHtml == undefined) throw Error("error-template not found");
        containerError = hand.compile(_templateHtml.html());
    }


    var showError = function (viewError, msg) {

        pullContainer();

        //scroll up
        jQmobile.silentScroll(0);

        viewError.empty();
        viewError.show();
        var _errorContent = containerError({ errorMessage: msg });
        var errorContent = $(_errorContent).show();
        viewError.html(errorContent.html());
        viewError.click(function () {

            closeError(viewError);
        });



    };

    closeError = function (viewError) {
        viewError.empty();
        viewError.hide();
    };
    var getGenericFunction = function(viewError) {

        var showPartial = _.partial(showError, viewError);
        return showPartial;

    };
     setTemplate = function(templateId) {
        if (templateId)
            pullContainer(templateId);
        else {
            containerError = null;
        }

    };
    return {
        show: showError,
        close: closeError,
        getPartialErrorAction: getGenericFunction,
        setTemplate: setTemplate,
        showHtmlError: function (view, html) {
            view.empty();
            view.html(html);
            view.click(function () {

                closeError(view);
            });
        }

};
})($.mobile, Handlebars, jQuery);




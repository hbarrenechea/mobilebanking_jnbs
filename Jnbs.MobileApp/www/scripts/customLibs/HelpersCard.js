﻿var HelpersCard = (function (_) {

    var basicMap = function (cardList, options) {

        if (!_.isArray(cardList)) throw "cardList need to be Array type";

        //defaultFilter
        var customFilter = function (cardItem) {
            if (_.isBoolean(cardItem.IsConfirmed)) {
                return cardItem.IsConfirmed == true;
            }
            return cardItem.IsConfirmed.toLowerCase() == "true";
        };
        if (options && options.all && options.all == true)
            customFilter = function (cardItem) {
                return cardItem;
            };


        var itemNumber = 0;
        var finalCardList = cardList.filter(function (cardItem) {
            return customFilter(cardItem);
        }).map(function (cardItem) {
            var row = cardItem;
            row["cardDescription"] = cardItem.CardNumber;
            row["itemNumber"] = ++itemNumber;
            row["confirmedText"] = cardItem.IsConfirmed == true ? "" : "Unconfirmed";
            return row;
        });
        return finalCardList;
    };

    var basicMap2 = function (response, options) {
        var finalCardList2 = [];
        var obj = response;
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                var cardList = obj[key];

                if (!_.isArray(cardList)) throw "cardList need to be Array type";

                //defaultFilter
                var customFilter = function (cardItem) {
                    if (_.isBoolean(cardItem.IsConfirmed)) {
                        return cardItem.IsConfirmed == true;
                    }
                    return cardItem.IsConfirmed.toLowerCase() == "true";
                };
                if (options && options.all && options.all == true)
                    customFilter = function (cardItem) {
                        return cardItem;
                    };


                var itemNumber = 0;
               
                var itemsCardList = cardList.filter(function (cardItem) {
                    return customFilter(cardItem);
                }).map(function (cardItem) {
                    var row = cardItem;
                    row["cardDescription"] = cardItem.CardNumber;
                    row["itemNumber"] = ++itemNumber;
                    row["confirmedText"] = cardItem.IsConfirmed == true ? "" : "Unconfirmed";
                    row["group"] = key;
                    row["isFirst"] = itemNumber == 1 ? true : false;
                    return row;
                })

                $(itemsCardList).each(function (i, item) {
                    finalCardList2.push(item);
                });

                // work with key and value
            }
        }
         
        return finalCardList2;
    };


    return {


        basicMap: basicMap,
        basicMap2: basicMap2

    }

})(_);


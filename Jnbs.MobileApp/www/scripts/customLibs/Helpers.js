﻿var Helpers = (function (mobile) {
    var maskMoney = function (num) {
        return accounting.formatNumber(num, 2, ",", ".");
    };
    var maskCardNumberBase = function (maskStr, cardnumber, quantityDigitShow) {
        var str = cardnumber.toString();
        var ln = str.length;
        //  var quantityDigitShow = 4;
        var starIn = (ln) - quantityDigitShow;
        var showablePart = str.substr(starIn, quantityDigitShow);
        return maskStr + showablePart;
    };
    var maskCardNumber = function (num) {

        return maskCardNumberBase("", num, 4);
    };

    var maskDate = function (strDate) {
        var d1 = new Date(strDate);
        var options = {month: 'short', day: 'numeric', year:'numeric' };
        return d1.toLocaleDateString("en-US", options);
        //var a = $.format.date(d1, "MMM dd, yyyy");
    };

    var getUserProfile = function () {

        if (localStorage.UserProfile == undefined) throw Error("Don't exist UserProfile");
       
        var profile = JSON.parse(localStorage.UserProfile);
        var fullName = profile.FirstName + ' ' + profile.LastName;
        var address = profile.Address;
        var addressFull = (address.Address3 || "-") + ' - ' + (address.AddressCountry || "-");

        return {
            fullName: fullName,
            address: addressFull
        };
    };

    var isEmailFormatValid = function (emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    };
    var progressBox = function () {

        return {
            start: function (textShowing) {
                mobile.loading("show", {
                    text: textShowing,
                    textVisible: true
                });
            },
            stop: function () {
                mobile.loading("hide");
            }
        };

    };

    var reduceAddress = function (result) {
        var resultAdd = _.reduce(result, function (memo, item) {
            return memo + item + ", ";
        }, "");
        return resultAdd.substring(0, resultAdd.lastIndexOf(","));
    };

    var setGeoLocalization = function (mapSuccess, errorCall) {
     
        var Localization = function(countryCode, region, countryName) {
            this.CountryCode = countryCode;
            this.Region = region;
            this.CountryName = countryName
        };

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "https://freegeoip.net/json/",
            "method": "GET"

        }

        $.ajax(settings).done(function (response) {
            mapSuccess(new Localization(response.country_code, response.region_code, response.country_name));
            console.log("freegeoip OK.");
        }).error(function() {
            console.log("First try getting geo data: error freegeoip services");

            ServiceConnection.getData({
                actionName: "geo.json",
                formData: null,
                typeAction: "GET",
                successCallback: function (response, status, xhr) {
                   
                    
                    mapSuccess(new Localization(response.data.geo.country_code, response.data.geo.region, response.data.geo.country_name));
                    console.log("geo.json OK.");

                },
                errorCallback: function (dataError) {
                    console.log("Second try gettig geo data: error geo.json services");

                    errorCall(dataError);

                }
            });

            //errorCallback(error);
        });


    };



    return {
        maskMoney: maskMoney,
        getLastCardNumber: maskCardNumber,
        maskDate: maskDate,
        getUserProfile: getUserProfile,
        isEmailFormatValid: isEmailFormatValid,
        progressBox: progressBox(),
        reduceAddress: reduceAddress,
        setUserNameInMenu: function (view) {
            var container = view.find("#userName");
            if (container) {
                var data = getUserProfile();
                container.text(data.fullName);
            }
        },
        setGeoLocalization: setGeoLocalization

    };
})($.mobile);
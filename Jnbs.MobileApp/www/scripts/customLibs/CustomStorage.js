﻿var CustomStorage = (function () {


    var makerStorage = function (storage) {
        var normStr = function (key) { return "jnbs_" + key.toString().toLowerCase(); };

        var exist = function (key) {
            var kn = normStr(key);
            return (storage[kn] != undefined);
        };
        var add = function (key, value) {
            storage[normStr(key)] = value;
        };

        var get = function (key, converter) {
            // var kn = normStr(key);
            if (exist(key)) {
                var dataRaw = storage[normStr(key)];
                if (converter)
                    return converter(dataRaw);
                return dataRaw;
            } else {
                throw key + " -> object not defined in cache";
            }

        };

        var remove = function (key) {
            storage.removeItem(normStr(key));
        };

        return {
            add: add,
            get: get,
            exist: exist,
            remove: remove
        };

    }


    return {
        session: makerStorage(sessionStorage),
        persist: makerStorage(localStorage)
    };
})();
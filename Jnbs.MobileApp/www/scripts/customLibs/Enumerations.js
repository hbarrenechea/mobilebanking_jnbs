﻿var Enumerations = (function (_) {

    var transactionStatus = { //data from Ramiro

        "C": "CANCELLED",
        "D": "Collected",
        "H": "Pending Disburse to Account",
        "S": "Pending Disburse to Account",
        "U": "Ready for pickup",
        "X": "Disbursed to Account",
        "Y": "Awaiting Payment Confirmation",
        "Q": "Bill Paid",
        "J": "Sent to JNMT Card"
    };

    var transferType = {
        P2P: 0,
        PensionTransfer: 1,
        GlobalAccountTransfer: 2,
        ThirdPartyTransfer: 3,
        BillPayTransfer: 4,
        AccountTransfer: 5,
        InstitutionTransfer: 6,
        MobileTopUp: 10,
        MTCardPayment: 11
    };
    
    var institutionType = {
        Unspecified: -1,
        Financial: 0, //use for account payment
        Utility: 1,// use for bill payment
        Pension: 2,
        Regulatory: 3,
        MobileServiceProviders: 4,
        TransferToMobileServiceProvider: 5,
        OnlineFinancial: 6,
        Unknown: 7
    };

    var getTransferTypeList = function() {
        var keys = _.keys(transferType);
        return keys.map(function(key) {
            return {
                value: transferType[key],
                name: key
            };
        });
    };



    return {

        getTransactionStatusDescriptionById: function (statusId) {
            var text = transactionStatus[statusId.toUpperCase()];
            if (text) return text;
            try {
                throw Error("'" + statusId + "' not found");
            } catch (err) { }
        },
        getTransferTypeList: getTransferTypeList,

        getTransferTypeValueByName:function(typeName) {
            var result = _.chain(getTransferTypeList())
                .filter(function(item) {
                    return item.name.toLowerCase() == typeName.toLowerCase();
                })
                .first()
                .value();
            if (result) {
                return result.value;
            }
            throw Error("not found transfer Type " + typeName);
        },

        getTransferTypeById: function (id) {
            var result = _.chain(getTransferTypeList())
                .filter(function (item) {
                    return item.value == id;
                })
                .first()
                .value();
            if (result) {
                return result.name;
            }
            throw Error("not found transfer Type " + id);
        },

        getInstitutionTypeValueByName: function (name) {
            var result = _.chain(_.keys(institutionType))
              .map(function (key) {
                  return {
                      key: key.toLowerCase(),
                      value: institutionType[key]
                  };
              })
              .findWhere({
                  key: name.toLowerCase()
              }).value();


            if (result)
                return result.value;

            throw Error("not found institution type");

        }
    }
})(_);
﻿var HostEnviroment = function (url) {
    if (url === "default" || url === undefined) {
        this.UrlBase = null;
        throw "not defined url base for service";
        //;
    }
    else this.UrlBase = url;
};

// Entornos
var envApiQA = new HostEnviroment("http://qa.mcsystems.com:4443/Jnms.Services.App.Out/api/");
var envOpenIdQA = new HostEnviroment("https://qa.mcsystems.com:4151/");
var envGEOQA = new HostEnviroment("https://tools.keycdn.com/");
var envCVCQA = new HostEnviroment("https://qa.mcsystems.com:4150/payment/paymentgateway");
var envSignupQA = new HostEnviroment("https://qa.mcsystems.com:4152/api/");

var ServiceConnection = {
    HostEnv: envApiQA, //type HostEnviroment
    // atributos para implmentacion de control de certificado
    //server: "https://www.youtube.com",
    //fingerprint: "94 2f 19 f7 a8 b4 5b 09 90 34 36 b2 2a c4 7f 17 06 ac 6a 2e",

    getData: function (requestObject) {
        var host = (ServiceConnection.HostEnv || (new HostEnviroment("default"))).UrlBase;
        if (requestObject.actionName.indexOf("core") > -1)
            host = envOpenIdQA.UrlBase;
        if (requestObject.actionName.indexOf("geo.json") > -1)
            host = envGEOQA.UrlBase;

        if (requestObject.actionName.indexOf("signup") > -1)
            host = envSignupQA.UrlBase;

        ///  implmentacion para control de certificado
        //window.plugins.sslCertificateChecker.check(
        //        function (message) {
        //            get(requestObject, host);
        //        },
        //        function (message) {
        //            alert(message);
        //            requestObject.errorCallback(message)
        //            if (message == "CONNECTION_NOT_SECURE") {
        //                // There is likely a man in the middle attack going on, be careful!
        //            } else if (message.indexOf("CONNECTION_FAILED") > -1) {
        //                // There was no connection (yet). Internet may be down. Try again (a few times) after a little timeout.
        //            }
        //        },
        //        this.server,
        //        this.fingerprint);

        if (new Date(localStorage.expires_in) <= new Date())
            getNewTokenBefore(requestObject, host);
        else
            get(requestObject, host);
    }
};

var getNewTokenBefore = function (requestObject, host) {
    var url = envOpenIdQA.UrlBase + 'core/connect/token';
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": url,
        "method": "POST",
        "headers": {
            "authorization": "Basic c2FicmE6Z3J1cG9zYWJyYQ==",
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
            "grant_type": "refresh_token",
            "refresh_token": localStorage.refresh_token,
            "redirect_uri": "http://localhost:21575/index.html"
        }
    }

    $.ajax(settings).done(function (response, status, xhr) {
        var expiresTime = new Date();
        expiresTime.setSeconds(expiresTime.getSeconds() + parseInt(response.expires_in));
        localStorage.expires_in = expiresTime;
        localStorage.access_token = response.access_token;
        localStorage.refresh_token = response.refresh_token;
        get(requestObject, host);
    }).error(function (error) {
        ErrorBox.show($("#errorContent"), "error getting new token");
    });

}

String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

var get = function (requestObject, host) {

    //$.support.cors = true;
    //$.mobile.allowCrossDomainPages = true;
    var url = host + requestObject.actionName;
    var typePost = requestObject.typeAction || "POST";
    //  var contentType = requestObject.contentType || false;

    var headersData = {
        "Authorization": "Bearer " + localStorage.access_token
    };

    if (requestObject.headerContentType) {
        headersData["Content-Type"] = requestObject.headerContentType;
    }

    if (requestObject.useHeaderDefault && requestObject.useHeaderDefault == false)
        headersData["Authorization"] = null;


    var settings = {
        "async": true,
        "crossDomain": false,
        "url": url,
        "method": typePost,
        "headers": headersData,
        "processData": false,
        "mimeType": "multipart/form-data",
        "data": requestObject.formData,
        "contentType": false,
        "timeout": 60000
    }

    $.ajax(settings).done(function (response, status, xhr) {
        if (response.startsWith("\"")) {
            response = response.slice(1, -1);
            response = response.replace(/\\"/g, '"');
        }
        requestObject.successCallback(jQuery.parseJSON(response), status, xhr);
    }).error(function (error) {
        if (error.statusText == "timeout")
            alert("This operation is taking too long, please try again");
        requestObject.errorCallback(error);
    });

}


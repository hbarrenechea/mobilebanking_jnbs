﻿var postLoginWorkflow = (function () {
    var validateLocation, getTokenData, getUserInfo,
        getUserProfile, setCurrentCountry,runEndStep;


    runEndStep = function() {
        $.mobile.changePage("../Send/Send.html");

    };
    validateLocation = function () {

        ServiceConnection.getData({
            actionName: 'SystemConfiguration?countryId=' + localStorage.CountryCode + '&region=' + localStorage.Region,
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {
            
                localStorage.IsAllowedLocation = response;
                Helpers.progressBox.stop();
                runEndStep();
               
            },
            errorCallback: function (dataError) {
                Helpers.progressBox.stop();
                localStorage.IsAllowedLocation = false;
                ErrorBox.show($("#errorContent"), "error calling geo.json service");
            }
        });
    }

    getTokenData = function (getUserInfo) {

        Helpers.progressBox.start("loading data");
        ServiceConnection.getData({
            actionName: "core/connect/accessTokenValidation?token=" + localStorage.access_token,
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {
                getUserInfo(response.sub);
            },
            errorCallback: function (dataError) {

                ErrorBox.show($("#errorContent"), "error calling accessTokenValidation service");
                Helpers.progressBox.stop();
            }
        });
    }

    getUserInfo = function (sub) {


        ServiceConnection.getData({
            actionName: "accounts?openId=" + sub,
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {
                localStorage.OpenIdUser = response.OpenId;
                localStorage.MtsId = response.MtsId;
                getUserProfile(response.MtsId);

            },
            errorCallback: function (dataError) {

                Helpers.progressBox.stop();
                ErrorBox.show($("#errorContent"), "error calling accounts service");

            }
        });
    }

    getUserProfile = function (mstId) {



        ServiceConnection.getData({
            actionName: "emoneyprofile?number=" + mstId + "&searchType=1",
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {
                localStorage.UserProfile = JSON.stringify(response);
                //  Helpers.setUserNameInMenu($("#Send"));
                if (localStorage.IsAllowedLocation == undefined) setCurrentCountry();
                else {
                    Helpers.progressBox.stop();
                    runEndStep();
                }

            },
            errorCallback: function (dataError) {
                Helpers.progressBox.stop();

                ErrorBox.show($("#errorContent"), "error calling emoneyprofile service");
            }
        });
    }

    setCurrentCountry = function () {


        Helpers.progressBox.start("loading data");

        ServiceConnection.getData({
            actionName: "geo.json",
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {
                localStorage.CountryCode = response.data.geo.country_code;
                localStorage.Region = response.data.geo.region;
                validateLocation();
            },
            errorCallback: function (dataError) {
                Helpers.progressBox.stop();
                ErrorBox.show($("#errorContent"), "error calling geo.json service");

            }
        });
    }
    var init = function () {
        if (localStorage.UserProfile == undefined) getTokenData(getUserInfo);
        //else
        //    Helpers.setUserNameInMenu($("#Send"));
        //if (localStorage.IsAllowedLocation == undefined) setCurrentCountry();
    };
    return {
        initialize: init
    };
})();

$(document).on('pageshow', '#postLogin', function (e) {
    postLoginWorkflow.initialize($("#postLogin"));
});

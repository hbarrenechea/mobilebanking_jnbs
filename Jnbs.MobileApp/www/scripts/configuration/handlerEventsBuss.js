﻿var keyRecipientListSession = "recipientList";
var keyHistoryListSession = "historyList";
var keyRecipientCardListSession = "recipientCardList";
var keyCardListSession = "cardList";

var handlerEventsBuss = (function () {

    var recipientChange = "CLEAR_RECIPIENT_P2P",
        histChange = "CLEAR_HISTORY",
        cardChange = "CLEAR_RECIPIENT_CARD",
        ownCardChange = "CLEAR_CARDS";


    var removeFromSession = function (key) {
        if (CustomStorage.session.exist(key))
            CustomStorage.session.remove(key);
    };

    var currentPageNameIs = function(pageName) {
        var sPath = window.location.pathname.toLowerCase();
        return sPath.indexOf(pageName.toLowerCase()) >= 0;
    };

   

    var eventBuss =  {};
    _.extend(eventBuss, Backbone.Events);
    eventBuss.on(recipientChange, function () {
        ViewConector.clearCacheByTag("recipient");
        removeFromSession(keyRecipientListSession);
      

    });

    eventBuss.on(histChange, function () {
        ViewConector.clearCacheByTag("history");
        removeFromSession(keyHistoryListSession);
       
    });

    eventBuss.on(cardChange, function () {
        ViewConector.clearCacheByTag("recipientCard");
        removeFromSession(keyRecipientCardListSession);

    });

    eventBuss.on(ownCardChange, function () {
        ViewConector.clearCacheByTag("card");
        removeFromSession(keyCardListSession);

    });
    
    var launchEvent = function(eventName) {
        eventBuss.trigger(eventName);
    };
    var refreshAll = function() {
        var events = [histChange, cardChange, recipientChange];
        events.forEach(function(evt) {
            launchEvent(evt);
        });

        if (currentPageNameIs("History.html")) {
            $.mobile.changePage("../History/History.html", { reloadPage: true });
        } else {
            if (currentPageNameIs("Recipients.html")) {
                $.mobile.changePage("../Recipients/Recipients.html", { reloadPage: true });
            }
        }
      
    };
    return {
        RECIPIENT_CHANGE: recipientChange,
        HIST_CHANGE: histChange,
        CARD_CHANGE: cardChange,
        OWN_CARD_CHANGE:ownCardChange,
        trigger: launchEvent,
        refreshAll: refreshAll,
        setEvent: function (idEvent,handlerFunc) {
            eventBuss.on(idEvent, handlerFunc);
        }

    };
})();

//window.refreshAll = handlerEventsBuss.refreshAll;
﻿$(document).on('pageshow', '#Send', function (e) {
    var view = $('#Send');

    Helpers.setUserNameInMenu($("#Send"));
    //===SET HERE TO "false" ONLY FOR DEV.
    var enableValidation = true;
    if (enableValidation == false) {
        var msg = "atention here geovalidation is disabled for DEV. Enabled for production";
        ErrorBox.show($("#errorContent"), msg);
    }
    var canOperateInCurrentCountry = function () {

        if (enableValidation != undefined && enableValidation == true) {
            console.log("CAREFUL here. Because it can be disabled in DEV validation of country");
            return localStorage.IsAllowedLocation == "true";
        }

        return true;//always can operaton (CAREFUL!)
    };

    var basicCheck = function (successCallback) {

        if (localStorage.IsAllowedLocation == undefined) {
            ErrorBox.show($("#errorContent"), "please try again");
            return;
        }
        if (canOperateInCurrentCountry()) {
            successCallback();
        }
        else
            ErrorBox.show($("#errorContent"), "the operation is not allowed in your current location:" + localStorage.CountryCode + "-" + localStorage.Region);

    };

    view.find("#signout").click(function () {
        $.mobile.changePage("../../index.html");
        localStorage.clear();
    });
    view.find("#notSignout").click(function () {
        view.find("#confirmSignout").popup("close");
    });


    $('#send-p2p').click(function () {

        basicCheck(function () {
            $.mobile.changePage("../Transfer/TransferSendTo.html");
        });
    });
    $('#send-to-card').click(function () {

        basicCheck(function () {
            $.mobile.changePage("../Transfer/toCard/RecipientListTarget.html");
        });
    });

    $("#send-to-bill").click(function () {
        basicCheck(function () {
            $.mobile.changePage("../Transfer/billPay/recipientBillTargetList.html");
        });
    });

    $("#send-to-account").click(function () {
        basicCheck(function () {
            $.mobile.changePage("../Transfer/sendAccount/accountRecipientTargetList.html");
        });
    });
    var view = $("#Send");
    view.find("#btnRefresh").click(function () {
        handlerEventsBuss.refreshAll();
    });
});


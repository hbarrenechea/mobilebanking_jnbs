﻿var vcCards;
var vcSourceOfFunds;
var TransferPayment = (function () {
    var view, context, showError, populaSourceOfFounds;

    var initialize = function ($view, ctx) {
        view = $view;
        context = ctx;

    };
    var populeCards = function () {

        Helpers.progressBox.start("loading cards and source");
        vcCards = vcCards || new ViewConector({
            container: view.find('#SelectCard'),
            viewContentType: ViewContentType.SELECT,
            actionName: 'CreditCard?openId=' + localStorage.OpenIdUser + "&country=" + localStorage.CountryCode,
            actionParameter: null,
            typeAction: "GET",
            enableLoading:false,
            configSettings: { value: "AccountCardId", text: "cardDescription" /*"CardType"*/ },
            convertResponse: function (response) {

                return HelpersCard.basicMap(response);
              
            }
        });
        var lastStepCommon = function() {
            Helpers.progressBox.stop();
        };
        var lastStepError = function(err) {
            lastStepCommon();
            console.log("Cards || Source of founds "+err);
            showError("Error loading cards and source of founds");
        };
        vcCards.execute(
            function (response) {
                
                localStorage.Cards = JSON.stringify(response);
                
                populaSourceOfFounds(lastStepCommon, lastStepError);
                
            }, lastStepError);
    };

     populaSourceOfFounds = function (callSuccess,callError) {
        vcSourceOfFunds = vcSourceOfFunds || new ViewConector({
            container: view.find('#SelectSource'),
            viewContentType: ViewContentType.SELECT,
            actionName: 'refencedata?referenceType=SourceOfFunds',
            actionParameter: null,
            typeAction: "GET",
            enableLoading: false,

            configSettings: { value: "SourceCode", text: "SourceName" },
        });

        vcSourceOfFunds.execute(callSuccess,callError);
    };

    var render = function () {
        populeCards();
     //   populaSourceOfFounds();
        view.find("#PersonaSend").text(localStorage.RecipientName);
    };

    var setHandlers = function () {
        view.find("#btn-popup").click(function () {
                      
            var popupCtrl = view.find("#popupConfirm");

            popupCtrl.bind({
                popupafteropen: function (event, ui) {
                    view.find("#send-amount").text(Helpers.maskMoney(localStorage.transferAmuntTransfer) + " " + localStorage.CurrencyCodeTransfer);
                    view.find("#send-fee").text(Helpers.maskMoney(localStorage.CurrentTransferFee) + " " + localStorage.FeeCurrencyId);
                    view.find("#send-exchangeRate").text(localStorage.ExchangeRate);
                    view.find("#send-total").text(localStorage.SendTotal);
                    view.find(".amount-receives").text(localStorage.TheyGet);
                    view.find("#to-fullname").text(localStorage.RecipientName);
                    view.find("#to-address").text(localStorage.RecipientAddress);
                    var userProfile = JSON.parse(localStorage.UserProfile);
                    view.find("#sender-name").text(userProfile.FirstName + ' ' + userProfile.LastName);
                    view.find("#sender-address").text(userProfile.Address.Address3 + ' - ' + userProfile.Address.AddressCountry);
                }
            });
            popupCtrl.popup("open");
        });
        view.find('#SelectCard').on('change', function () {
            if ($(this).val() != null) {
                var cards = JSON.parse(localStorage.Cards);
                for (var i = 0; i < cards.length; i++) {
                    if (cards[i].AccountCardId == $(this).val()) {
                        localStorage.CardId = cards[i].RecurringDetailsReference;
                        break;
                    }
                }
            }
        });
        var redirectSuccess = function () {
            //EVENT CLEAN
            handlerEventsBuss.trigger(handlerEventsBuss.HIST_CHANGE);
            $.mobile.changePage("TransferSuccess.html");
        };
        var redirectError = function () {
            //EVENT CLEAN
            //handlerEventsBuss.trigger(handlerEventsBuss.HIST_CHANGE);
            $.mobile.changePage("TransferError.html");
        };
        var chargeCVCPage = function (transactionId) {
            var urlCheck = "confirmTransfer.html";
            var urlService = envCVCQA.UrlBase + '?transactionid=' + transactionId + '&accessToken=' + localStorage.access_token + "&countryId=" + localStorage.CountryCode;
            var externalWin = window.open(urlService, "_blank", "location=no,clearsessioncache=yes,hidden=yes");
            Helpers.progressBox.start("loading...");

            $(externalWin).on('loadstop', function (e) {
                var url = e.originalEvent.url;
                if (url.indexOf("barclaycardsmartpay") >= 0) {
                    Helpers.progressBox.stop(); // implement by yourself
                    externalWin.show();
                }
            });
            $(externalWin).on('loadstart', function (e) {
                var url = e.originalEvent.url;

                if (url.indexOf(urlCheck) >= 0) {
                    localStorage.TransactionData = url;
                    externalWin.close();
                    if (localStorage.TransactionData.getParam('status')==='success') {
                        redirectSuccess();
                    }
                    else {
                        redirectError();
                    }
                }
            });
        }
        view.find("#btn-confirm").click(function () {

            $.mobile.loading("show", {
                text: 'loading data...',
                textVisible: true
            });

            var recipient;

            for (var i = 0; i < RecipientsList.response.length; i++) {
                if (RecipientsList.response[i].PersonId == localStorage.RecipientId) {
                    recipient = RecipientsList.response[i];
                    break;
                }
            }

            var data = {
                "AccountCardId": view.find('#SelectCard').val(),
                "Payee": {
                    "OpenId": recipient.OpenId,
                    "PersonId": recipient.PersonId,
                    "Title": recipient.Title,
                    "FirstName": recipient.FirstName,
                    "MiddleName": recipient.MiddleName,
                    "LastName": recipient.LastName,
                    "EmailAddress": recipient.EmailAddress,
                    "ContactTelephone1": recipient.ContactTelephone1,
                    "ContactTelephone2": recipient.ContactTelephone2,
                    "MobileTelephone": recipient.MobileTelephone,
                    "Dob": recipient.DOB,
                    "PlaceOfBirth": recipient.PlaceOfBirth,
                    "CustomerCategory": recipient.CustomerCategory,
                    "Address": {
                        "Address1": recipient.Address.Address1,
                        "Address2": recipient.Address.Address2,
                        "Address3": recipient.Address.Address3,
                        "Address4": recipient.Address.Address4,
                        "AddressCountry": recipient.Address.AddressCountry
                    },
                    "SourceOfFunds": recipient.SourceOfFunds,
                    "Nationality": recipient.Nationality,
                    "Occupation": recipient.Occupation,
                    "Ssn": recipient.SSN,
                    "SsntrnRequired": recipient.SSNTRNRequired,
                    "Identification": {
                        "IdentificationType": recipient.Identification.IdentificationType,
                        "IdentificationNumber": recipient.Identification.IdentificationNumber,
                        "IdentificationIssuingCountry": recipient.Identification.IdentificationIssuingCountry,
                        "IdentificationIssueDate": recipient.Identification.IdentificationIssueDate,
                        "IdentificationExpirationDate": recipient.Identification.IdentificationExpirationDate,
                        "AdditionalIdentificationNumber": recipient.Identification.AdditionalIdentificationNumber
                    },
                    "AddressVerificationDate": recipient.AddressVerificationDate,
                    "IdVerificationDate": recipient.IdVerificationDate,
                    "AggregatedRiskType": recipient.AggregatedRiskType,
                    "MTCardNumber": recipient.MTCardNumber,
                    "NatureAndPurposeOfTransfers": recipient.NatureAndPurposeOfTransfers,
                    "ExpectedTransactionFrequency": recipient.ExpectedTransactionFrequency,
                    "Status": recipient.Status,
                    "HasCreditCard": false
                },
                "PaymentInfo": {
                    "AlternativePaymentReferenceNumber": null,
                    "PayerMTSId": localStorage.MtsId,
                    "PayeeMTSId": recipient.PersonId,
                    "PaymentCurrency": localStorage.CurrencyCodeTransfer,
                    "PaymentAmount": localStorage.AmountTransfer,
                    "PaymentDate": JSON.stringify(new Date()),
                    "PaymentLocation": localStorage.CountryCode +',',    
                    //IMPORTANTE SOLO PARA FUNCIONE
                    // LA TRANSFERENCIA SE HARDCODEA LA LINEA DE ABAJO EL VALOR "JM,".
                    //"PaymentLocation": "JM,",
                    "PaymentDataEntryLocation": null,
                    "DisbursementCountry": localStorage.RecipientCountry,
                    "PayeeCurrency": localStorage.RecipientCurrencyCode,
                    "PaymentFeeAdjustment": 0,
                    "RedeemFreePaymentFee": false,
                    "TransactionUserId": null,
                    "TransactionDataEntryUserId": null,
                    "PaymentMethod": 4,// pago hecho con tarjeta 
                    "PaymentChequeInstitutionId": null,
                    "PaymentChequeNumber": null,
                    "PaymentApprovalCode": null,
                    "EffectiveDate": JSON.stringify(new Date()),
                    "PaymentTransactionCode": 0,
                    "PaymentCustomerUpdateType": 0,
                    "SourceOfFunds": view.find('#SelectSource').val(),
                    "PurporseOfTransfer": view.find('#InputPayment').val()
                },
                "Notes": null,
                "ReccuringReference": localStorage.CardId
            };

            ServiceConnection.getData({
                actionName: "P2P",
                formData: JSON.stringify(data),
                headerContentType: "application/json",
                typeAction: "POST",
                successCallback: function (response, status, xhr) {
                    $.mobile.loading("hide");
                    if (response.Success) {
                        //chargeCVCPage(response.Body.TransactionId);
                        sessionStorage.TransactionId = response.Body.PaymentReferenceNumber;
                        redirectSuccess();
                    }
                    else {
                        //alert(response.Message);
                        ErrorBox.show(view.find("#errorInPopup"),response.Message);
                    }
                   
                  
                },
                errorCallback: function (dataError) {
                    $.mobile.loading("hide");
                    //alert(dataError);
                    // showError(JSON.stringify(dataError).substring(0, 120));
                    console.log("Error P2P Service: " + JSON.stringify(dataError).substring(0, 120));
                    ErrorBox.show(view.find("#errorInPopup"), "This operation is taking longer than expected, please wait and check your transaction history to check for the status");
                    redirectError();
                }
            });

        });
    };
    //public interface
    return {
        initialize: initialize,
        render: render,
        setHandlers: setHandlers,
        setDisplayError: function (showErr) {
            showError = showErr;
        }
    };
})();

$(document).on('pageshow', '#TransferPayment', function (e) {
    var view = $('#TransferPayment');
    TransferPayment.initialize(view);
    TransferPayment.setHandlers();
    TransferPayment.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));

    TransferPayment.render();
});
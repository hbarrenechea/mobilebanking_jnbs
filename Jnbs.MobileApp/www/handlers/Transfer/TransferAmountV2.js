﻿var vcCurrency;
var TransferAmount = (function () {
    var view, context;
    var initialize = function ($view, ctx) {
        view = $view;
        context = ctx;
    };
    var setHandlers = function () {

        var selectElm = view.find("#SelectCurrencyTransfer");
        var inputAmount = view.find("#InputAmountTransfer");

        var getInputAmount = function () {
            var value = inputAmount[0].value;
            if (Number(value) != undefined) {
                return Number(value);
            }
            return Number(0);
        };

        inputAmount.focus(function () {
            if (this.value == "0.0") this.value = "";
        });
        inputAmount.focusout(function () {
            if (this.value == "") this.value = "0.0";
        });
        
        var changeExchangeData = function () {
            localStorage.CurrencyCodeTransfer = selectElm.val();
            var url = "SystemConfiguration?";
            var paramService = {
                countryId: localStorage.CountryCode,
                regionId: "",
                transferCurrencyId: localStorage.CurrencyCodeTransfer,
                transferAmount: getInputAmount(),
                disbursementCountryId: localStorage.RecipientCountry,
                disbursementCurrency: localStorage.RecipientCurrencyCode,
                transfertype: "P2P"
            };

            var urlService = url + $.param(paramService);
            var exchangeConector = new ViewConector({
                actionName: urlService,
                actionParameter: null,
                viewContentType: ViewContentType.FORM,
                container: $('#exchangeData'),
                typeAction: 'GET'
            });

            exchangeConector.execute(callbackExchange);
        };

        selectElm.on('change', function () {
            if (getInputAmount() > 0) {
                changeExchangeData();
            }
        });
        inputAmount.on('input', function () {
            var amountNum = getInputAmount();
            if (selectElm[0].value != "" && amountNum > 0) {
                changeExchangeData();
            }
        });
        
        view.on('click', '#ConfirmAmount', function (e) {
            if (selectElm[0].value != "") {
                if (getInputAmount() > 0) {
                    localStorage.transferAmuntTransfer = getInputAmount();
              
                    $.mobile.changePage('TransferPayment.html');
                }
            }
        });
        
        var callbackExchange = function () {
            var dataExchangeText = '1 ' + selectElm.val() +' = ';
            $("#senderCurrency").text(dataExchangeText);
            var canTransfer = Helpers.maskMoney(getInputAmount() * $('#CurrentExchangeRate').text()) + ' ' + localStorage.RecipientCurrencyCode;
            $('#CantTransfer').text(canTransfer);
            localStorage.AmountTransfer = Helpers.maskMoney(getInputAmount());
            localStorage.TheyGet = canTransfer;
            localStorage.CurrentTransferFee = $("#CurrentTransferFee").val();
            localStorage.FeeCurrencyId = $("#FeeCurrencyId").val() == "" ? localStorage.CurrencyCodeTransfer : $("#FeeCurrencyId").val();
            localStorage.ExchangeRate = dataExchangeText + Helpers.maskMoney($("#CurrentExchangeRate").text()) + ' ' + $("#DisburseCountryDefaultCurrency").text();
            var sendTotal = Number(getInputAmount()) + Number(localStorage.CurrentTransferFee);
            localStorage.SendTotal = Helpers.maskMoney(sendTotal) + ' ' + localStorage.FeeCurrencyId;
        };
    };
    var populateCurrency = function () {

        vcCurrency = vcCurrency || new ViewConector({
            container: view.find('#SelectCurrencyTransfer'),
            viewContentType: ViewContentType.SELECT,
            actionName: 'CountryLookup?countryId='+localStorage.CountryCode+'&fakeParam=',
            actionParameter: null,
            typeAction: "GET",
            configSettings: { value: "CurrencyCode", text: "CurrencyName" }
        });

        vcCurrency.execute();
    };
    
    var render = function () {
        populateCurrency();
        view.find("#PersonaSend").text(localStorage.RecipientName);
    };


    var getCrossData = function () {
        return JSON.parse(CustomStorage.session.get("tansferAmountCross"));
    };
    //public interface
    return {
        initialize: initialize,
        render: render,
        setHandlers: setHandlers,
        //getCrossData: getCrossData
    };
})();

$(document).on('pageshow', '#TransferAmount', function (e) {
   
    var view = $("#TransferAmount");

    TransferAmount.initialize(view);

    TransferAmount.render();
    TransferAmount.setHandlers();

    if (localStorage.transferAmuntTransfer != undefined) {
        $('#InputAmountTransfer').val(localStorage.transferAmuntTransfer);

        $("#SelectCurrencyTransfer option[value='"+localStorage.CurrencyCodeTransfer+"']").attr('selected', 'selected');
        $('#SelectCurrencyTransfer').selectmenu('refresh');
        $('#SelectCurrencyTransfer').change();
 
    }
});
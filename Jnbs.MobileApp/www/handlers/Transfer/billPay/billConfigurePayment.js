﻿jnbs.billPayment.billConfigurePayment = (function () {
    var vcListCards;
    var vcSourceOfFundsCards;
    var showError, view, context, selectedModel,
        populaSourceOfFounds, setHandlers;

    var initialize = function ($view, ctx) {
        view = $view;
        context = ctx;
        setHandlers();
        selectedModel = new jnbs.transfer.models.BillModule.BillModel(context.billTarget);
    };
    var populeCards = function () {
        Helpers.progressBox.start("loading cards and source");
        vcListCards = vcListCards || new ViewConector({
            container: view.find('#SelectCard'),
            viewContentType: ViewContentType.SELECT,
            actionName: 'CreditCard?openId=' + localStorage.OpenIdUser + "&country=" + localStorage.CountryCode,
            actionParameter: null,
            typeAction: "GET",
            configSettings: { value: "AccountCardId", text: "cardDescription" },
            enableLoading: false,
            convertResponse: function (response) {

                return HelpersCard.basicMap(response);

            }
        });

        var lastStepCommon = function () {
            Helpers.progressBox.stop();
        };
        var lastStepError = function (err) {
            lastStepCommon();
            showError(err);
        };
        vcListCards.execute(
            function (response) {
                localStorage.Cards = JSON.stringify(response);
                populaSourceOfFounds(lastStepCommon, lastStepError);
            }, lastStepError);
    };

    populaSourceOfFounds = function (callSuccess, callError) {
        vcSourceOfFundsCards = vcSourceOfFundsCards || new ViewConector({
            container: view.find('#SelectSource'),
            viewContentType: ViewContentType.SELECT,
            actionName: 'refencedata?referenceType=SourceOfFunds',
            actionParameter: null,
            typeAction: "GET",
            enableLoading: false,

            configSettings: { value: "SourceCode", text: "SourceName" },
        });

        vcSourceOfFundsCards.execute(callSuccess, callError);
    };

    var render = function () {
        populeCards();

        view.find("#institutionTarget").text(context.billTarget.accountInstitution);
    };

    setHandlers = function () {
        view.find("#btn-popup").click(function () {

            var popupCtrl = view.find("#popupConfirm");
            //var address = context.rawItem.Address;
            var fullAddress = selectedModel.getTextAddress();
            //    address.Address1,
            //    address.Address2,
            //    address.Address3,
            //    address.Address4,
            //    address.AddressCountry
            //]);


            popupCtrl.bind({
                popupafteropen: function (event, ui) {
                    view.find("#send-amount").text(Helpers.maskMoney(localStorage.transferAmuntBill) + " " + localStorage.CurrencyCodeBill);
                    view.find("#send-fee").text(Helpers.maskMoney(localStorage.CurrentTransferFee) + " " + localStorage.FeeCurrencyId);
                    view.find("#send-exchangeRate").text(localStorage.ExchangeRate);
                    view.find("#send-total").text(localStorage.SendTotal);
                    view.find(".amount-receives").text(localStorage.TheyGet);
                    view.find("#to-fullname").text(selectedModel.get("accountInstitution"));
                    view.find("#to-address").text(fullAddress);
                    var userProfile = JSON.parse(localStorage.UserProfile);
                    view.find("#sender-name").text(userProfile.FirstName + ' ' + userProfile.LastName);
                    view.find("#sender-address").text(userProfile.Address.Address3 + ' - ' + userProfile.Address.AddressCountry);
                }
            });
            popupCtrl.popup("open");
        });
        view.find('#SelectCard').on('change', function () {
            //if ($(this).val() != null) {
            //    var cards = JSON.parse(localStorage.Cards);
            //    for (var i = 0; i < cards.length; i++) {
            //        if (cards[i].AccountCardId == $(this).val()) {
            //            localStorage.CardId = cards[i].RecurringDetailsReference;
            //            break;
            //        }
            //    }
            //}

            //---
            if ($(this).val() != null) {
                var currentVal = $(this).val();
                localStorage.CardId = _.chain(JSON.parse(localStorage.Cards))
                    .filter(function (item) {
                        return item.AccountCardId == currentVal;
                    })
                    .first()
                    .value().RecurringDetailsReference;
                //    localStorage.CardId = result.RecurringDetailsReference;
            }
        });
        var redirectSuccess = function () {
            //EVENT CLEAN
            handlerEventsBuss.trigger(handlerEventsBuss.HIST_CHANGE);
            $.mobile.changePage("../commonSuccessView.html");
        };
        var redirectError = function () {
            //EVENT CLEAN
            //handlerEventsBuss.trigger(handlerEventsBuss.HIST_CHANGE);
            $.mobile.changePage("../TransferError.html");
        };
        var chargeCVCPage = function (transactionId) {
            var urlCheck = "confirmTransfer.html";
            var urlService = envCVCQA.UrlBase + '?transactionid=' + transactionId + '&accessToken=' + localStorage.access_token + "&countryId=" + localStorage.CountryCode;
            var externalWin = window.open(urlService, "_blank", "location=no,clearsessioncache=yes,hidden=yes");
            Helpers.progressBox.start("loading...");

            $(externalWin).on('loadstop', function (e) {
                var url = e.originalEvent.url;
                if (url.indexOf("barclaycardsmartpay") >= 0) {
                    Helpers.progressBox.stop(); // implement by yourself
                    externalWin.show();
                }
            });

            $(externalWin).on('loadstart', function (e) {
                var url = e.originalEvent.url;

                if (url.indexOf(urlCheck) >= 0) {
                    localStorage.TransactionData = url;
                    externalWin.close();
                    if (localStorage.TransactionData.getParam('status') === 'success') {
                        redirectSuccess();
                    }
                    else {
                        redirectError();
                    }
                }
            });
        }
        view.find("#btn-confirm").click(function () {


            Helpers.progressBox.start("loading data...");

            var recipient = selectedModel.get("rawItem");

            var data = {
                "AccountCardId": view.find('#SelectCard').val(),

                //   "MTCardNumber": recipient.MTCardNumber,
                CustomerMTSId: localStorage.MtsId,
                "PaymentInfo": {
                    "AlternativePaymentReferenceNumber": null,
                    "PayerMTSId": localStorage.MtsId,
                    "PayeeMTSId": recipient.MTSId,
                    "PaymentCurrency": localStorage.CurrencyCodeBill,
                    "PaymentAmount": localStorage.AmountTransfer,
                    "PaymentDate": JSON.stringify(new Date()),
                    "PaymentLocation": localStorage.CountryCode + ',',
                    //IMPORTANTE SOLO PARA FUNCIONE
                    // LA TRANSFERENCIA SE HARDCODEA LA LINEA DE ABAJO EL VALOR "JM,".
                    //"PaymentLocation": "JM,",
                    "PaymentDataEntryLocation": null,
                    "DisbursementCountry": recipient.Address.AddressCountry,//localStorage.RecipientCountry,
                    "PayeeCurrency": localStorage.RecipientCurrencyCode,
                    "PaymentFeeAdjustment": 0,
                    "RedeemFreePaymentFee": false,
                    "TransactionUserId": null,
                    "TransactionDataEntryUserId": null,
                    "PaymentMethod": 4,// pago hecho con tarjeta 
                    "PaymentChequeInstitutionId": null,
                    "PaymentChequeNumber": null,
                    "PaymentApprovalCode": null,
                    "EffectiveDate": JSON.stringify(new Date()),
                    "PaymentTransactionCode": 0,
                    "PaymentCustomerUpdateType": 0,
                    "SourceOfFunds": view.find('#SelectSource').val(),
                    "PurporseOfTransfer": view.find('#InputPayment').val()
                },
                PaymentAccount: {
                    "AccountNumber": recipient.AccountNumber,
                    "AccountCurrency": recipient.AccountCurrency,
                    "AccountType": recipient.AccountType,
                    "AccountInstitution": recipient.AccountInstitution,
                    "InstitutionBranchId": "",
                    "OtherInstitutionName": "",
                    "PrimAccountHolderFirstName": recipient.PrimAccountHolderFirstName,
                    "PrimAccountHolderSurname": recipient.PrimAccountHolderSurname,
                    "SecAccountHolderFirstName": "",
                    "SecAccountHolderSurname": "",
                    "TelephoneNumber": recipient.TelephoneNumber,
                    "AccountMTSId": recipient.MTSId,
                    PrimAccountHolderAddress: recipient.Address

                },


                "Notes": null,
                "ReccuringReference": localStorage.CardId
            };

            ServiceConnection.getData({
                actionName: "BillPaymentTransfer",
                formData: JSON.stringify(data),
                headerContentType: "application/json",
                typeAction: "POST",
                successCallback: function (response, status, xhr) {
                    Helpers.progressBox.stop();
                    if (response.Success) {
                        //chargeCVCPage(response.Body.TransactionId);
                        sessionStorage.TransactionId = response.Body.PaymentReferenceNumber;
                        redirectSuccess();
                    }
                    else {
                        //alert(response.Message);
                        showError(response.Message);                        
                    }
                    // $.mobile.loading("hide");
                   

                },
                errorCallback: function (dataError) {

                    Helpers.progressBox.stop();

                    console.log("Bill Payment========start");
                    console.log(JSON.stringify(dataError));
                    console.log("Bill Payment========end");
                    showError("This operation is taking longer than expected, please wait and check your transaction history to check for the status");
                    redirectError();

                }
            });

        });
    };
    return {
        setDisplayError: function (errorFunc) {
            showError = errorFunc;
        },
        init: initialize,
        render: render
    };
})();


$(document).on('pageshow', '#billConfigurePayment', function (e) {

    var view = $("#billConfigurePayment");

    var app = jnbs.billPayment.billConfigurePayment;
    var context = {
        billTarget: jnbs.billPayment.recipientBillTargetList.getSelected()
    };
    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    app.init(view, context);
    app.render();

});
﻿memorizeSelectValueBill = function () {
    if (localStorage.transferAmuntBill != undefined && localStorage.CurrencyCodeBill != undefined) {
        $("#billConfigureAmount #InputAmountBill").val(localStorage.transferAmuntBill);
        $("#billConfigureAmount #SelectCurrencyBill").val(localStorage.CurrencyCodeBill);
        $("#billConfigureAmount #SelectCurrencyBill").selectmenu("refresh");
        $("#billConfigureAmount #SelectCurrencyBill").change();
    }
}

jnbs.billPayment.billConfigureAmount = (function () {
    var lastAmount = 0,
        context, showError,
        view, setHandlers, callbackExchange;

    var init = function ($view,$ctx) {
        view = $view;
        context = $ctx;

    };

     setHandlers = function () {

        var selectElm = view.find("#SelectCurrencyBill");
        var inputAmount = view.find("#InputAmountBill");

        var getInputAmount = function () {
            var value = inputAmount[0].value;
            if (Number(value) != undefined) {
                return Number(value);
            }
            return Number(0);
        };

        var changeExchangeData = function () {
            localStorage.CurrencyCodeBill = selectElm.val();
            var url = "SystemConfiguration?";
            var paramService = {
                countryId: localStorage.CountryCode,
                regionId: "",
                transferCurrencyId: localStorage.CurrencyCodeBill,
                transferAmount: getInputAmount(),
                disbursementCountryId: context.billTarget.address.addressCountry,//localStorage.RecipientCountry,
                disbursementCurrency: localStorage.RecipientCurrencyCode,//
                transfertype: Enumerations.getTransferTypeValueByName("BillPayTransfer")
            };

            var urlService = url + $.param(paramService);
            var exchangeConector = new ViewConector({
                actionName: urlService,
                actionParameter: null,
                viewContentType: ViewContentType.FORM,
                container: $('#exchangeData'),
                typeAction: 'GET',
                messageLoading: "calculating exchange..."
            });

            exchangeConector.execute(callbackExchange, function () {
                showError("error try to calculating exchange data");
            });
        };

        selectElm.on('change', function () {
            if (getInputAmount() > 0) {
                changeExchangeData();
            }
        });

        inputAmount.focus(function () {
            if (this.value == "0.0") this.value = "";
        });
        inputAmount.focusout(function () {
            if (this.value == "") this.value = "0.0";
        });
        inputAmount.on('input', function () {
            var amountNum = getInputAmount();
            if (selectElm[0].value != "" && amountNum > 0) {
                if (lastAmount != amountNum) {
                    changeExchangeData();
                    lastAmount = amountNum;
                }

            }
        });

        view.on('click', '#confirmAmount', function (e) {
            if (selectElm[0].value != "") {
                if (getInputAmount() > 0) {
                    localStorage.transferAmuntBill = getInputAmount();

                    $.mobile.changePage('billConfigurePayment.html');
                }
            }
        });

         callbackExchange = function () {
            var dataExchangeText = '1 ' + selectElm.val() + ' = ';
            $("#senderCurrency").text(dataExchangeText);
            var canTransfer = Helpers.maskMoney(getInputAmount() * $('#CurrentExchangeRate').text()) + ' ' + localStorage.RecipientCurrencyCode;
            $('#CantTransfer').text(canTransfer);
            localStorage.AmountTransfer = Helpers.maskMoney(getInputAmount());
            localStorage.TheyGet = canTransfer;
            localStorage.CurrentTransferFee = $("#CurrentTransferFee").val();
            localStorage.FeeCurrencyId = $("#FeeCurrencyId").val() == "" ? localStorage.CurrencyCodeBill : $("#FeeCurrencyId").val();
            localStorage.ExchangeRate = dataExchangeText + Helpers.maskMoney($("#CurrentExchangeRate").text()) + ' ' + $("#DisburseCountryDefaultCurrency").text();
            var sendTotal = Number(getInputAmount()) + Number(localStorage.CurrentTransferFee);
            localStorage.SendTotal = Helpers.maskMoney(sendTotal) + ' ' + localStorage.FeeCurrencyId;
        };
    };

    var render = function () {
        view.find("#institutionTarget").text(context.billTarget.accountInstitution);
        TransferHelpers.fillCurrency(view.find("#SelectCurrencyBill"), localStorage.CountryCode, memorizeSelectValueBill, null, null);
        setHandlers();
    };
    return {
        setDisplayError: function (errFunc) {
            showError = errFunc;
        },
        init: init,
        render: render
    };
})();



$(document).on('pageshow', '#billConfigureAmount', function (e) {

    var view = $("#billConfigureAmount");

    var app = jnbs.billPayment.billConfigureAmount;
    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    var context = {
        billTarget: jnbs.billPayment.recipientBillTargetList.getSelected()
    };
    app.init(view, context);
    app.render();

    //if (localStorage.transferAmuntBill != undefined) {
    //    $("#billConfigureAmount #InputAmount").val(localStorage.transferAmuntBill);
    //    $("#billConfigureAmount #SelectCurrency option[value='" + localStorage.CurrencyCodeBill + "']").attr('selected', 'selected');
    //    $("#billConfigureAmount #SelectCurrency").selectmenu('refresh');
    //}
});
﻿jnbs.billPayment = jnbs.billPayment || {};
jnbs.billPayment.recipientBillTargetList = (function () {
    var
        BillModel = jnbs.transfer.models.BillModule.BillModel,
        showError, view, populate, ListView, setSelected,
        BillCollection, PopupView,
        BillView, setupObjects;

    setSelected = function (dataStr) {
        CustomStorage.session.add("billSelected", dataStr);
    };

    setupObjects = function () {

        BillCollection = Backbone.Collection.extend({
            model: BillModel
        });
        PopupView = Backbone.View.extend({
            el: view.find("#popupRecipient"),
            events: {
                "click #amountAction": "navigateToAmountView"
            },
            template: Handlebars.compile(view.find("#popupRecipient").html()),
            initialize: function () {

            },
            render: function () {
                var pop = this.$el;
                pop.html(this.template(this.model.attributes));
                pop.popup("open");
            },
            navigateToAmountView: function () {
                setSelected(JSON.stringify(this.model.attributes));

                TransferHelpers.getRecipientCurrency(this.model.attributes.address.addressCountry, function (response) {
                    localStorage.RecipientCurrencyCode = response.CurrencyCode;
                    $.mobile.changePage("billConfigureAmount.html");
                }, function (err) {
                    showError(err);
                });

            }
        });
        BillView = Backbone.View.extend({
            el: view.find("#li-template").html(),
            templateContent: Handlebars.compile(view.find("#li-content-template").html()),
            events: {
                "click": "showPopup"
            },
            initialize: function () {

            },
            render: function () {
                var data = this.templateContent(this.model.attributes);
                this.$el.html(data);
                return this;
            },
            showPopup: function () {
                var pop = new PopupView({ model: this.model });
                pop.render();
            }
        });
        ListView = Backbone.View.extend({
            el: view.find("#listBillRecipient"),
            initialize: function () {

            },
            render: function () {
                this.$el.empty();

                this.collection.forEach(function (item) {
                    var itemView = new BillView({ model: item });
                    this.$el.append(itemView.render().el);

                }, this);
            }
        });
    };

    var init = function ($view) {
        view = $view;
        setupObjects();
    };

    populate = function () {

        var url = "PreviousRecipientAccounts?";
        var paramObj = {
            mtsId: localStorage.MtsId,
            institutionId: "all",
            institutionTypeId: Enumerations.getInstitutionTypeValueByName("utility"),
            searchStatus: 2
        };
        var urlService = url + $.param(paramObj);

        Helpers.progressBox.start("getting bills information");
        ServiceConnection.getData({
            actionName: urlService,
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {

                var list = response.map(function (item) {
                    var address = item.Address;
                    return new BillModel({
                        accountInstitution: item.AccountInstitution,
                        accountCurrency: item.AccountCurrency,
                        accountNumber: item.AccountNumber || "-",
                        holderFirstName: item.PrimAccountHolderFirstName || "-",
                        holderLastName: item.PrimAccountHolderSurname || "-",
                        address:
                        {
                            address1: address.Address1 || "-",
                            address2: address.Address2 || "-",
                            address3: address.Address3 || "-",
                            address4: address.Address4 || "-",
                            addressCountry: address.AddressCountry || "-"
                        },
                        rawItem:item
                    });
                });

                var billList = new BillCollection(list);
                var mainView = new ListView({ collection: billList });

                mainView.render();
                //  console.log(response);

                Helpers.progressBox.stop();
            },
            errorCallback: function (dataError) {

                Helpers.progressBox.stop();
                showError("there was an error when attempting to load bill recipients");
            }
        });
    }
    var render = function () {
        populate();
    };
    return {
        setDisplayError: function (errorFunc) {
            showError = errorFunc;
        },
        init: init,
        render: render,
        getSelected: function () {
            return CustomStorage.session.get("billSelected",
                function (data) {
                    return JSON.parse(data);
                });
        }
    };

})();



$(document).on('pageshow', '#billListView', function (e) {

    var view = $("#billListView");
    view.find("#recipientBillTargetListCloseLocation").click(function () {
        localStorage.AddRecipientBackTo = "../../Transfer/billPay/recipientBillTargetList.html";
        $.mobile.changePage("../../AddRecipients/addBill/billRecolectDataStep1View.html");
    });
    var app = jnbs.billPayment.recipientBillTargetList;
    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    app.init(view);
    //  app.clearData();
    app.render();
    // app.setHandlers();

    $('#billListView .ss-plus').click(function () {
        try{
            sessionStorage.removeItem('billAccountNumber');
            sessionStorage.removeItem('billCbInstitution');
            sessionStorage.removeItem('billFirstName');
            sessionStorage.removeItem('billLastName');
            sessionStorage.removeItem('billTelephone');
            sessionStorage.removeItem('billAddress');
            sessionStorage.removeItem('billCityOrTown');
            sessionStorage.removeItem('billPostalCode');
            sessionStorage.removeItem('billSelectCountry');
            sessionStorage.removeItem('billSelectStates');            
        }
        catch (error) {
            console.log(error);
        }
    })
});
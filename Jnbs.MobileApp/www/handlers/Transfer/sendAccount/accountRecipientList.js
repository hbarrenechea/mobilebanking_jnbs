﻿jnbs.accountPayment = jnbs.accountPayment || {};

jnbs.accountPayment.transferAccountList = (function(models) {
    var
        AccountModel = models.AccountModel,
        showError,
        view, setSelected,
        populate,
        setupObjects,
        AccountCollection, AccountView, ListView,
        PopupView;

    setSelected = function (dataStr) {
        CustomStorage.session.add("accountSelected", dataStr);
    };
    setupObjects = function () {

        AccountCollection = Backbone.Collection.extend({
            model: AccountModel
        });
        PopupView = Backbone.View.extend({
            el: view.find("#popupRecipient"),
            events: {
                "click #amountAction": "navigateToAmountView"
            },
            template: Handlebars.compile(view.find("#popupRecipient").html()),
            initialize: function () {

            },
            render: function () {
                var pop = this.$el;
                pop.html(this.template(this.model.attributes));
                pop.popup("open");
            },
            navigateToAmountView: function () {
                setSelected(JSON.stringify(this.model.attributes));

                TransferHelpers.getRecipientCurrency(this.model.attributes.address.addressCountry, function (response) {
                    localStorage.RecipientCurrencyCode = response.CurrencyCode;
                    $.mobile.changePage("accountConfigureAmount.html");
                }, function (err) {
                    showError(err);
                });

            }
        });
        AccountView = Backbone.View.extend({
            el: view.find("#li-template").html(),
            templateContent: Handlebars.compile(view.find("#li-content-template").html()),
            events: {
                "click": "showPopup"
            },
            initialize: function () {

            },
            render: function () {
                var data = this.templateContent(this.model.attributes);
                this.$el.html(data);
                return this;
            },
            showPopup: function () {
                var pop = new PopupView({ model: this.model });
                pop.render();
            }
        });
        ListView = Backbone.View.extend({
            el: view.find("#listAccountRecipients"),
            initialize: function () {

            },
            render: function () {
                this.$el.empty();

                this.collection.forEach(function (item) {
                    var itemView = new AccountView({ model: item });
                    this.$el.append(itemView.render().el);

                }, this);
            }
        });
    };
    populate = function () {

        var url = "PreviousRecipientAccounts?";
        var paramObj = {
            mtsId: localStorage.MtsId,
            institutionId: "all",
            institutionTypeId: Enumerations.getInstitutionTypeValueByName("financial"),
            searchStatus: 2
        };
        var urlService = url + $.param(paramObj);

        Helpers.progressBox.start("getting accounts information");
        ServiceConnection.getData({
            actionName: urlService,
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {

                var list = response.map(function (item) {
                    var address = item.Address;
                    return new AccountModel({
                        accountInstitution: item.AccountInstitution,
                        accountCurrency: item.AccountCurrency,
                        accountNumber: item.AccountNumber || "-",
                        holderFirstName: item.PrimAccountHolderFirstName || "-",
                        holderLastName: item.PrimAccountHolderSurname || "-",
                        telephone:item.TelephoneNumber||"-",
                        address:
                        {
                            address1: address.Address1 || "-",
                            address2: address.Address2 || "-",
                            address3: address.Address3 || "-",
                            address4: address.Address4 || "-",
                            addressCountry: address.AddressCountry || "-"
                        },
                        rawItem: item
                    });
                });

                var accountList = new AccountCollection(list);
                var mainView = new ListView({ collection: accountList });

                mainView.render();
                  console.log(response);

                Helpers.progressBox.stop();
            },
            errorCallback: function (dataError) {

                Helpers.progressBox.stop();
                showError("there was an error when attempting to load accounts recipients");
            }
        });
    }
    var init = function($view) {
        view = $view;
        setupObjects();
        populate();
    };
    return {
        setDisplayError: function (errorFunc) {
            showError = errorFunc;
        },
        init: init,
        getSelected:function() {
            return CustomStorage.session.get("accountSelected", function(d) {
                return JSON.parse(d);
            });
        }
    };
})(jnbs.transfer.models.AccountModule);

$(document).on('pageshow', '#accountListView', function (e) {

    var view = $("#accountListView");
    view.find("#accountRecipientTargetListCloseLocation").click(function () {
        localStorage.AddRecipientBackTo = "../Transfer/sendAccount/accountRecipientTargetList.html";
        $.mobile.changePage("../../AddRecipients/addAccount/addAccountDataStep1View.html");
    });
    $("#accountListView .ss-plus").click(function () {
        try {
            sessionStorage.removeItem('accountAccountNumber');
            sessionStorage.removeItem('accountcityOrTown');
            sessionStorage.removeItem('accountPostalCode');
            sessionStorage.removeItem('accountCbInstitution');
            sessionStorage.removeItem('accountCbAccountType');
            sessionStorage.removeItem('accountCbAccountCurrency');
            sessionStorage.removeItem('accountCbBranch');
        }
        catch (error) {
            console.log(error)
        }
    })

    if(localStorage.Region == "NY" && localStorage.CountryCode =="US")
        ErrorBox.show($("#errorContent"), "After confirming and making payment, please contact JN Member Care at 1-800-462-9003 and provide the transaction number and also to confirm details of the account the funds were sent to.");
    var appPage = jnbs.accountPayment.transferAccountList;
    appPage.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    appPage.init(view);

});
﻿memorizeSelectValueAccount = function () {
    if (localStorage.transferAmunt != undefined && localStorage.CurrencyCodeAccount != undefined) {
        $("#accountConfigureAmount #InputAmountAccount").val(localStorage.transferAmunt);
        $("#accountConfigureAmount #SelectCurrencyAccount").val(localStorage.CurrencyCodeAccount);
        $("#accountConfigureAmount #SelectCurrencyAccount").selectmenu("refresh");
        $("#accountConfigureAmount #SelectCurrencyAccount").change();
        //jnbs.accountPayment.configureAmount.setHandlers.changeExchangeData;
    }
}

jnbs.accountPayment.configureAmount = (function () {
    var showError = null, view, callbackExchange,
        render, setHandlers, lastAmount, context;

    var init = function ($view, $ctx) {
        view = $view;
        context = $ctx;
        render();
    };

    setHandlers = function () {


        var selectElm = view.find("#SelectCurrencyAccount");
        var inputAmount = view.find("#InputAmountAccount");

        var getInputAmount = function () {
            var value = inputAmount[0].value;
            if (Number(value) != undefined) {
                return Number(value);
            }
            return Number(0);
        };

        var changeExchangeData = function () {

            localStorage.CurrencyCodeAccount = selectElm.val();


            var url = "SystemConfiguration?";
            var paramService = {
                countryId: localStorage.CountryCode,
                regionId: "",
                transferCurrencyId: localStorage.CurrencyCodeAccount,
                transferAmount: getInputAmount(),
                disbursementCountryId: context.accountTarget.address.addressCountry,//localStorage.RecipientCountry,
                disbursementCurrency: localStorage.RecipientCurrencyCode,//
                transfertype: Enumerations.getTransferTypeValueByName("AccountTransfer")
            };

            var urlService = url + $.param(paramService);
            var exchangeConector = new ViewConector({
                actionName: urlService,
                actionParameter: null,
                viewContentType: ViewContentType.FORM,
                container: $('#exchangeData'),
                typeAction: 'GET',
                messageLoading: "calculating exchange..."
            });

            exchangeConector.execute(callbackExchange, function () {
                showError("error try to calculating exchange data");
            });
        };

        selectElm.on('change', function () {
            if (getInputAmount() > 0) {
                changeExchangeData();
            }

        });

        inputAmount.focus(function () {
            if (this.value == "0.0") this.value = "";
        });
        inputAmount.focusout(function () {
            if (this.value == "") this.value = "0.0";
        });
        inputAmount.on('input', function () {
            var amountNum = getInputAmount();
            if (selectElm[0].value != "" && amountNum > 0) {
                if (lastAmount != amountNum) {
                    changeExchangeData();
                    lastAmount = amountNum;
                }

            }
        });

        view.on('click', '#confirmAmount', function (e) {
            if (selectElm[0].value != "") {
                if (getInputAmount() > 0) {
                    localStorage.transferAmunt = getInputAmount();
                    localStorage.CurrencyCodeAccount = selectElm.val();
                    $.mobile.changePage('accountConfigurePayment.html');
                }
            }
        });

        callbackExchange = function () {
            var dataExchangeText = '1 ' + selectElm.val() + ' = ';
            $("#senderCurrency").text(dataExchangeText);
            var canTransfer = Helpers.maskMoney(getInputAmount() * $('#CurrentExchangeRate').text()) + ' ' + localStorage.RecipientCurrencyCode;
            $('#CantTransfer').text(canTransfer);
            localStorage.AmountTransfer = Helpers.maskMoney(getInputAmount());
            localStorage.TheyGet = canTransfer;
            localStorage.CurrentTransferFee = $("#CurrentTransferFee").val();
            localStorage.FeeCurrencyId = $("#FeeCurrencyId").val() == "" ? localStorage.CurrencyCodeAccount : $("#FeeCurrencyId").val();
            localStorage.ExchangeRate = dataExchangeText + Helpers.maskMoney($("#CurrentExchangeRate").text()) + ' ' + $("#DisburseCountryDefaultCurrency").text();
            var sendTotal = Number(getInputAmount()) + Number(localStorage.CurrentTransferFee);
            localStorage.SendTotal = Helpers.maskMoney(sendTotal) + ' ' + localStorage.FeeCurrencyId;

        };
    };

    render = function () {
        view.find("#institutionTarget").text(context.accountTarget.accountInstitution);
        TransferHelpers.fillCurrency(view.find("#SelectCurrencyAccount"), localStorage.CountryCode, memorizeSelectValueAccount, null, null);
        setHandlers();
    };
    return {
        setDisplayError: function (err) {
            showError = err;
        },
        init: init
    }
})();



$(document).on('pageshow', '#accountConfigureAmount', function (e) {

    var view = $("#accountConfigureAmount");



    var app = jnbs.accountPayment.configureAmount;
    var context = {
        accountTarget: jnbs.accountPayment.transferAccountList.getSelected()
    };
    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    app.init(view, context);



});
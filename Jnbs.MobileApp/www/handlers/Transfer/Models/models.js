﻿jnbs.transfer = {
    models: {}
};

jnbs.transfer.models.BillModule = (function(_,$) {
    var billModel = Backbone.Model.extend({
        defaults: {
            accountInstitution: "",
            accountCurrency: "",
            accountNumber: "",
            holderFirstName: "",
            holderLastName: "",
            address: {
                address1: "",
                address2: "",
                address3: "",
                address4: "",
                addressCountry:""
            }
        },
        getTextAddress: function () {
            var self = this.get("address");
            var str = [
                self.address1 || "-",
                self.address2 || "-",
                self.address3 || "-",
                self.address4 || "-",
                self.addressCountry || "-"
            ];
            return Helpers.reduceAddress(str);
        }
    });


    return {
        BillModel:billModel
    };
})(_,$);

jnbs.transfer.models.AccountModule = (function(models) {

    var accountModel = models.BillModule.BillModel.extend({
        defaults:
        {
            telephone:""
        }
    });
    return {
        AccountModel:accountModel
    };
})(jnbs.transfer.models);
﻿var RecipientsList = {};
var TransferSendTo = (function () {
    var view, showError, getRecipientCurrency;
    var setHandlerDrawPopup = function(resp) {
        if (resp.length == 0) {
            showError("No recipients");
            return;
        }
        view.find("#AddRecipientBackLocation").click(function () {
            localStorage.AddRecipientBackTo = "../Transfer/TransferSendTo.html";
            $.mobile.changePage("../AddRecipients/AddRecipientPersonStep1.html");
        });
        view.find(".open-popup").click(function() {
            var ctrl = $(this);
            var popupCtrl = view.find("#popupRecipient");
            localStorage.RecipientCountry = ctrl.find(".get-country").val();
            localStorage.RecipientId = ctrl.find(".get-id").val();
            var fullName = ctrl.find(".get-name").text(); //+ " " + ctrl.find(".get-last").val();

            //var fullName = ctrl.find(".get-name").text() + " " + ctrl.find(".get-last").val();
            var address1 = ctrl.find(".get-address1").val();
            var address2 = ctrl.find(".get-address2").val();
            var address3 = ctrl.find(".get-address3").val();
            var address4 = ctrl.find(".get-address4").val();
            var address = Helpers.reduceAddress([address1, address2, address3, address4, localStorage.RecipientCountry]);
            localStorage.RecipientAddress = address;
            localStorage.RecipientName = fullName;
            var dataView = {
                fullName: fullName,
                mail: ctrl.find(".get-mail").text(),
                telephone: ctrl.find(".get-tele").val(),
                address: address
            };

            popupCtrl.bind({
                popupafteropen: function (event, ui) {

                    view.find("#pop-fullname").text(dataView.fullName || "-");
                    view.find("#pop-mail").text(dataView.mail || "-");
                     view.find("#pop-tele").text(dataView.telephone || "-");
                     view.find("#pop-address").text(dataView.address || "-");
                    CustomStorage.session.add("sendToData", JSON.stringify(dataView));
                }
            });
                    
            popupCtrl.popup("open");
        });       

        
    };
    var initialize = function ($view) {
        view = $view;      
    };

   

    var render3 = function () {

        var cvResponse = function(response) {
            return response.map(function(item) {
                item.fullName = item.FirstName + " " + item.LastName;
                return item;
            });
        };

        var form = new FormData();
        form.append("UserId", 1);
        form.append("Status", 1);
        var mtCardHoldersOnly = null;
        if (mtCardHoldersOnly != null) {
            form.append("mtCardHoldersOnly", mtCardHoldersOnly);
        }
        RecipientsList = new ViewConector({
            container: view.find('#ListRecipients'),
            viewContentType: ViewContentType.DYNAMIC,
            tagName:"recipient",
            actionName: 'previousrecipients?personId=' + localStorage.MtsId + '&searchStatus=ActiveOnly',
            actionParameter: null,
            typeAction: "GET",
            convertResponse:cvResponse,
            template: '<li class="ui-li-has-thumb ui-nodisc-icon ui-alt-icon"><a  class="ui-btn ui-btn-icon-right ui-icon-carat-r open-popup" data-transition="pop" data-rel="popup"><div class="circle circle-p2p"><i class="ss-user"></i></div><h6 data-value="fullName" class="control get-name">Name recipients</h6><p data-value="EmailAddress" class="control get-mail text-gray"></p><input type="hidden" data-value="LastName" class="control get-last" /><input type="hidden" data-value="ContactTelephone1" class="control get-tele" /><input type="hidden" data-value="Address1" class="control get-address1" /><input type="hidden" data-value="Address2" class="control get-address2" /><input type="hidden" data-value="Address3" class="control get-address3" /><input type="hidden" data-value="Address4" class="control get-address4" /><input type="hidden" data-value="AddressCountry" class="control get-country" /><input type="hidden" data-value="PersonId" class="control get-id" /></a></li>'

        });
        RecipientsList.execute(setHandlerDrawPopup);
    };

    var setHandlers = function() {
        view.find("#btn-next-amount").click(function() {
            $.mobile.changePage("TransferAmount.html");
            getRecipientCurrency();
        });
    };

     getRecipientCurrency = function () {
        ServiceConnection.getData({
            actionName: 'SystemConfiguration?countryId=' + localStorage.RecipientCountry,
            formData: null,
            successCallback: function (response, status, xhr) {
                localStorage.RecipientCurrencyCode = response.CurrencyCode;
            },
            errorCallback: function (dataError) {
                console.log(dataError);
            },
            typeAction: "GET"

        });
    }
    //public interface
    return {
        initialize: initialize,
        render: render3,
        setHandlers: setHandlers,
        setDisplayError: function (showErr) {
            showError = showErr;
        }
        //getCrossData:function() {
        //    return JSON.parse( CustomStorage.session.get("sendToData"));
        //}
        // setHandlers: setHandlers
    };
})();

$(document).on('pageshow', '#TransferSendTo', function (e) {

    var view = $('#TransferSendTo');
    TransferSendTo.initialize(view);
    TransferSendTo.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));

    TransferSendTo.setHandlers();
    TransferSendTo.render();

    $("#TransferSendTo .ss-plus").click(function() {
        try{
            sessionStorage.removeItem('personFirstName');
            sessionStorage.removeItem('personLastName');
            sessionStorage.removeItem('personMiddleName');
            sessionStorage.removeItem('personTitle');
            sessionStorage.removeItem('personCityTown');
            sessionStorage.removeItem('personStreet');
            sessionStorage.removeItem('personMiddleName');
            sessionStorage.removeItem('personCountry');
            sessionStorage.removeItem('personStates');
            sessionStorage.removeItem('personTelephone1');
            sessionStorage.removeItem('personTelephone2');
            sessionStorage.removeItem('personEmail');
        }
        catch (error) {
            console.log(error)
        }
    })
});
﻿$(document).on('pageshow', '#TransferSuccess', function (e) {

    //var transId = getParameterByName("TransactionId", localStorage.TransactionData) || "-";
    //$("#transactionId").text(transId);
    $("#transactionId").text(sessionStorage.TransactionId);
});


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
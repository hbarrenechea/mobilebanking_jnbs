﻿var vcListCards;
var vcSourceOfFundsCards;
jnbs.sendToCard.configurePayment = (function (_) {
    //var vcListCards = {};
    //var vcSourceOfFundsCards = {};

    var view, context, showError, populaSourceOfFounds;

    var initialize = function ($view, ctx) {
        view = $view;
        context = ctx;

    };
    var populeCards = function () {
        Helpers.progressBox.start("loading cards and source");
        vcListCards = vcListCards || new ViewConector({
            container: view.find('#SelectCard'),
            viewContentType: ViewContentType.SELECT,
            actionName: 'CreditCard?openId=' + localStorage.OpenIdUser + "&country=" + localStorage.CountryCode,
            actionParameter: null,
            typeAction: "GET",
            configSettings: { value: "AccountCardId", text: "cardDescription" },
            enableLoading: false,
            convertResponse: function (response) {

                return HelpersCard.basicMap(response);
               
            }
        });

        var lastStepCommon = function () {
            Helpers.progressBox.stop();
        };
        var lastStepError = function (err) {
            lastStepCommon();
            console.log("Error -> Cards || Source of founds " + err);
            showError("Error loading cards/source of founds");
        };
        vcListCards.execute(
            function (response) {
                localStorage.Cards = JSON.stringify(response);
                populaSourceOfFounds(lastStepCommon, lastStepError);
            },lastStepError);
    };

    populaSourceOfFounds = function (callSuccess, callError) {
        vcSourceOfFundsCards = vcSourceOfFundsCards || new ViewConector({
            container: view.find('#SelectSource'),
            viewContentType: ViewContentType.SELECT,
            actionName: 'refencedata?referenceType=SourceOfFunds',
            actionParameter: null,
            typeAction: "GET",
            enableLoading: false,

            configSettings: { value: "SourceCode", text: "SourceName" },
        });

        vcSourceOfFundsCards.execute(callSuccess,callError);
    };

    var render = function () {
        populeCards();
        view.find("#PersonaSend").text(context.nameRecipient);
    };

    var setHandlers = function () {
        view.find("#btn-popup").click(function () {
           
            var popupCtrl = view.find("#popupConfirm");
            var address = context.rawItem.Address;
            var fullAddress = Helpers.reduceAddress([
                address.Address1,
                address.Address2,
                address.Address3,
                address.Address4,
                address.AddressCountry
            ]);
            popupCtrl.bind({
                popupafteropen: function (event, ui) {
                    view.find("#send-amount").text(Helpers.maskMoney(localStorage.transferAmuntCard) + " " + localStorage.CurrencyCodeCard);
                    view.find("#send-fee").text(Helpers.maskMoney(localStorage.CurrentTransferFee) + " " + localStorage.FeeCurrencyId);
                    view.find("#send-exchangeRate").text(localStorage.ExchangeRate);
                    view.find("#send-total").text(localStorage.SendTotal);
                    view.find(".amount-receives").text(localStorage.TheyGet);
                    view.find("#to-fullname").text(context.nameRecipient);
                    view.find("#to-address").text(fullAddress);
                    var userProfile = JSON.parse(localStorage.UserProfile);
                    view.find("#sender-name").text(userProfile.FirstName + ' ' + userProfile.LastName);
                    view.find("#sender-address").text(userProfile.Address.Address3 + ' - ' + userProfile.Address.AddressCountry);
                }
            });
            popupCtrl.popup("open");
        });
        view.find('#SelectCard').on('change', function () {
            if ($(this).val() != null) {
                var cards = JSON.parse(localStorage.Cards);
                for (var i = 0; i < cards.length; i++) {
                    if (cards[i].AccountCardId == $(this).val()) {
                        localStorage.CardId = cards[i].RecurringDetailsReference;
                        break;
                    }
                }
            }
        });
        var redirectSuccess = function () {
            //EVENT CLEAN
            handlerEventsBuss.trigger(handlerEventsBuss.HIST_CHANGE);
            $.mobile.changePage("SentSuccessfully.html");
        };
        var redirectError = function () {
            //EVENT CLEAN
            //handlerEventsBuss.trigger(handlerEventsBuss.HIST_CHANGE);
            $.mobile.changePage("../TransferError.html");
        };
        var chargeCVCPage = function (transactionId) {
            var urlCheck = "confirmTransfer.html";
            var urlService = envCVCQA.UrlBase + '?transactionid=' + transactionId + '&accessToken=' + localStorage.access_token + "&countryId=" + localStorage.CountryCode;
            var externalWin = window.open(urlService, "_blank", "location=no,clearsessioncache=yes,hidden=yes");
            Helpers.progressBox.start("loading...");

            $(externalWin).on('loadstop', function (e) {
                var url = e.originalEvent.url;
                if (url.indexOf("barclaycardsmartpay") >= 0) {
                    Helpers.progressBox.stop(); // implement by yourself
                    externalWin.show();
                }
            });

            $(externalWin).on('loadstart', function (e) {
                var url = e.originalEvent.url;

                if (url.indexOf(urlCheck) >= 0) {
                    localStorage.TransactionData = url;
                    externalWin.close();
                    if (localStorage.TransactionData.getParam('status') === 'success') {
                        redirectSuccess();
                    }
                    else {
                        redirectError();
                    }
                }
            });
        }
        view.find("#btn-confirm").click(function () {

         
            Helpers.progressBox.start("loading data...");

            var recipient=context.rawItem;

            var data = {
                "AccountCardId": view.find('#SelectCard').val(),

                    "MTCardNumber": recipient.MTCardNumber,
                "PaymentInfo": {
                    "AlternativePaymentReferenceNumber": null,
                    "PayerMTSId": localStorage.MtsId,
                    "PayeeMTSId": recipient.PersonId,
                    "PaymentCurrency": localStorage.CurrencyCodeCard,
                    "PaymentAmount": localStorage.AmountTransfer,
                    "PaymentDate": JSON.stringify(new Date()),
                    "PaymentLocation": localStorage.CountryCode + ',',
                    //IMPORTANTE SOLO PARA FUNCIONE
                    // LA TRANSFERENCIA SE HARDCODEA LA LINEA DE ABAJO EL VALOR "JM,".
                    //"PaymentLocation": "JM,",
                    "PaymentDataEntryLocation": null,
                    "DisbursementCountry": recipient.Address.AddressCountry,//localStorage.RecipientCountry,
                    "PayeeCurrency": localStorage.RecipientCurrencyCode,
                    "PaymentFeeAdjustment": 0,
                    "RedeemFreePaymentFee": false,
                    "TransactionUserId": null,
                    "TransactionDataEntryUserId": null,
                    "PaymentMethod": 4,// pago hecho con tarjeta 
                    "PaymentChequeInstitutionId": null,
                    "PaymentChequeNumber": null,
                    "PaymentApprovalCode": null,
                    "EffectiveDate": JSON.stringify(new Date()),
                    "PaymentTransactionCode": 0,
                    "PaymentCustomerUpdateType": 0,
                    "SourceOfFunds": view.find('#SelectSource').val(),
                    "PurporseOfTransfer": view.find('#InputPayment').val()
                },
                "Notes": null,
                "ReccuringReference": localStorage.CardId
            };

            ServiceConnection.getData({
                actionName: "MTCard",
                formData: JSON.stringify(data),
                headerContentType: "application/json",
                typeAction: "POST",
                successCallback: function (response, status, xhr) {
                    Helpers.progressBox.stop();
                    if (response.Success) {
                        //chargeCVCPage(response.Body.TransactionId);
                        sessionStorage.TransactionId = response.Body.PaymentReferenceNumber;
                        redirectSuccess();
                    }
                    else {
                        //alert(response.Message);
                        showError(response.Message);
                    }
                    // $.mobile.loading("hide");
                  

                },
                errorCallback: function (dataError) {
                    //  $.mobile.loading("hide");
                    Helpers.progressBox.stop();
                    //alert(dataError);
                    console.log("Error MTcard " + JSON.stringify(dataError).substring(0, 120));
                    showError("This operation is taking longer than expected, please wait and check your transaction history to check for the status");
                    redirectError();
                }
            });

        });
    };
    //public interface
    return {
        initialize: initialize,
        render: render,
        setHandlers: setHandlers,
        setDisplayError: function (showErr) {
            showError = showErr;
        }
    };
})(_);

$(document).on('pageshow', '#configurePayment', function (e) {
    var app = jnbs.sendToCard.configurePayment;
    var view = $('#configurePayment');
    app.initialize(view, jnbs.sendToCard.recipientListTarget.getRecipientSelected());
    app.setHandlers();
    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));

    app.render();
});
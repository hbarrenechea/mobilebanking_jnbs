﻿jnbs.sendToCard = jnbs.sendToCard || {};

jnbs.sendToCard.recipientListTarget = (function () {
    var showError, view, context, setupObjects, getCurrentData,setupModels,
        RecipientCardItemModel, RecipientCardList,
        RecipientCardListView,
        RecipientCardItemView;

    var getRecipientCurrency = function (callback) {
        var countryId = getCurrentData().address.addressCountry;
        ServiceConnection.getData({
            actionName: 'SystemConfiguration?countryId=' + countryId,
            formData: null,
            successCallback: function (response, status, xhr) {
                localStorage.RecipientCurrencyCode = response.CurrencyCode;
                callback();
            },
            errorCallback: function (dataError) {
                showError("Error trying to calling recipient currency");
            },
            typeAction: "GET"

        });
    }
    var saveCurrentData = function(data) {
        localStorage.recipientCardSelected = JSON.stringify(data);
    };
     getCurrentData = function() {
        return JSON.parse( localStorage.recipientCardSelected);
     };
    setupModels = function() {
        RecipientCardItemModel = Backbone.Model.extend({
            defaults: {
                nameRecipient: "",
                mailRecipient: ""

            }
        });
        RecipientCardList = Backbone.Collection.extend({ model: RecipientCardItemModel });
    };
    setupObjects = function () {

      
        var PopupView = Backbone.View.extend({
            template: Handlebars.compile(view.find("#popup-template").html()),
            el: view.find("#popupRecipient"),
            render: function () {
                this.$el.html(this.template(this.model.attributes));
                saveCurrentData(this.model.attributes);
                this.configureHandler();
                return;
            },
            configureHandler:function() {
                this.$el.find("#btn-next-amount").click(function() {
                    getRecipientCurrency(function() {
                        $.mobile.changePage("ConfigureAmount.html");
                    });
                });
            },
            getPopupCtrl: function () {
                return this.$el;
            }

        });
        RecipientCardItemView = Backbone.View.extend({
            el: view.find("#li-template").html(),
            template: Handlebars.compile(view.find("#li-content-template").html()),
            // templatePopup:Handlebars.compile(view.find("#popup-template").html()),
            events: {
                "click": "showDetailRecipient"
            },
            render: function () {
                var dataHtml = this.template(this.model.attributes);
                this.$el.html(dataHtml);
                return this;
            },
            showDetailRecipient: function (e) {
                //  alert(this.model.get("nameRecipient"));
                var self = this;

                var viewPopup = new PopupView({ model: self.model });
                var popupCtrl = viewPopup.getPopupCtrl();
                viewPopup.render();
                popupCtrl.popup("open");

            }
        });
        RecipientCardListView = Backbone.View.extend({
            el: view.find("#ListRecipientsCard"),
            render: function () {
                this.collection.forEach(function (itemModel) {
                    var itemView = new RecipientCardItemView({ model: itemModel });
                    this.$el.append(itemView.render().el);
                }, this);
            }
        });
    };
    var init = function ($view, $context) {
        view = $view;
        context = $context;
        setupModels();
        setupObjects();
    };
    var renderMainView = function (list) {
        var vlist = new RecipientCardListView({ collection: list });
        vlist.render();
    };
    var getFakeListData = function () {
        var list = new RecipientCardList();
        list.add(new RecipientCardItemModel({
            nameRecipient: "diego",
            mailRecipient: "h123@h123.com",
            countryName: "jamaica",
            phoneRecipient: "314447744",
            address: {
                address1: "d1",
                address2: "d2",
                address3: "d3",
                address4: "d4",
                addressCountry:"jm"
            }
        }));
        list.add(new RecipientCardItemModel({
            nameRecipient: "mjose",
            mailRecipient: "manca.jose@h123.com",
            countryName: "argentina",
            phoneRecipient: "147477555",
            address: {
                address1: "d1",
                address2: "d2",
                address3: "d3",
                address4: "d4",
                addressCountry: "arg"
            }
        }));

        return list;
    };
    var makeRecipientCardItemModel = function(item) {
        return new RecipientCardItemModel({
            nameRecipient: item.FirstName + " " + item.LastName,
            mailRecipient: item.EmailAddress || "-",
            phoneRecipient: item.ContactTelephone1 || "-",
            countryName: item.PlaceOfBirth,
            address: {
                address1: item.Address.Address1 || "-",
                address2: item.Address.Address2 || "-",
                address3: item.Address.Address3 || "-",
                address4: item.Address.Address4 || "-",
                addressCountry: item.Address.AddressCountry || "-"
            },
            rawItem: item
        });
    };
    var renderWithService = function () {
        var drawData = function(response) {
            var listModel = response.map(function(item) {
                return makeRecipientCardItemModel(item);

            });
            var collectionModel = new RecipientCardList(listModel);
            renderMainView(collectionModel);
        };

        if (CustomStorage.session.exist(keyRecipientCardListSession)) {
            var response = CustomStorage.session.get(keyRecipientCardListSession,
                function (r) { return JSON.parse(r); }
            );
            drawData(response);
            return;
        }

        var urlBase = "previousrecipients";
        var urlService = urlBase + "?" + $.param({ personId: localStorage.MtsId, searchStatus: 2, mtCardOnly: true });
        Helpers.progressBox.start("getting card recipients");
        ServiceConnection.getData({
            actionName: urlService,
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {
              
                if (response.length == 0) {
                    Helpers.progressBox.stop();
                    showError("No recipients");
                    return;
                }
                CustomStorage.session.add(keyRecipientCardListSession, JSON.stringify(response));
                drawData(response);
             
                Helpers.progressBox.stop();
            },
            errorCallback: function (dataError) {
              
                Helpers.progressBox.stop();
                showError("there was an error when attempting to load recipients");
            }
        });


    }
    var render = function () {
        renderWithService();
        //var lst = getFakeListData();
        // renderMainView(lst);
    };

    var addData = function (data) {
        var recipientData = {};
        if (sessionStorage.recipientData == undefined)
            sessionStorage.recipientData = JSON.stringify(recipientData);
        else
            recipientData = JSON.parse(sessionStorage.recipientData);

        _.each(_.keys(data), function (key) {
            var value = data[key];
            recipientData[key] = value;
        });
        sessionStorage.recipientData = JSON.stringify(recipientData);
        // localStorage.recipientData[key] = value;
    };

    return {
        init: init,
        setDisplayError: function(showErr) {
            showError = showErr;
        },
        render: render,
        addData: addData,
        getRecipientSelected: getCurrentData,
        createRecipientModel: function (recipientItem) {
            setupModels();
            return makeRecipientCardItemModel(recipientItem);
        }
    // setHandlers:setHandlers
};
})();

$(document).on('pageshow', '#RecipientListTarget', function (e) {

    var view = $("#RecipientListTarget");

    view.find("#RecipientListCloseLocation").click(function () {
        localStorage.AddRecipientBackTo = "../Transfer/toCard/RecipientListTarget.html";
        $.mobile.changePage("../../AddRecipients/AddRecipientCardStep1.html");
    });

    var app = jnbs.sendToCard.recipientListTarget;
    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    app.init(view);
    //  app.clearData();
    app.render();
   // app.setHandlers();
});
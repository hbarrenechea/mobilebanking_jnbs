﻿var vcCurrencyCard;
jnbs.sendToCard.configureAmount = (function () {

    var lastAmount = 0,
        view, context, showError;


    var initialize = function ($view, ctx) {
        view = $view;
        context = ctx;
    };
    var setHandlers = function () {

        var selectElm = view.find("#SelectCurrency");
        var inputAmount = view.find("#InputAmount");

        var getInputAmount = function () {
            var value = inputAmount[0].value;
            if (Number(value) != undefined) {
                return Number(value);
            }
            return Number(0);
        };

        var changeExchangeData = function () {
            localStorage.CurrencyCodeCard = selectElm.val();
            var url = "SystemConfiguration?";
            var paramService = {
                countryId: localStorage.CountryCode,
                regionId: "",
                transferCurrencyId: localStorage.CurrencyCodeCard,
                transferAmount: getInputAmount(),
                disbursementCountryId: context.address.addressCountry,//localStorage.RecipientCountry,
                disbursementCurrency: localStorage.RecipientCurrencyCode,//
                transfertype: "11"  //ToCard
            };

            var urlService = url + $.param(paramService);
            var exchangeConector = new ViewConector({
                actionName: urlService,
                actionParameter: null,
                viewContentType: ViewContentType.FORM,
                container: $('#exchangeData'),
                typeAction: 'GET',
                messageLoading: "calculating exchange..."
            });

            exchangeConector.execute(callbackExchange,function() {
                showError("error try to calculating exchange data");
            });
        };

        selectElm.on('change', function () {
            if (getInputAmount() > 0) {
                changeExchangeData();
            }
        });

        inputAmount.focus(function() {
            if (this.value == "0.0") this.value = "";
        });
        inputAmount.focusout(function () {
            if (this.value == "") this.value = "0.0";
        });
        inputAmount.on('input', function () {
            var amountNum = getInputAmount();
            if (selectElm[0].value != "" && amountNum > 0) {
                if (lastAmount != amountNum) {
                    changeExchangeData();
                    lastAmount = amountNum;
                }

            }
        });

        view.on('click', '#ConfirmAmount', function (e) {
            if (selectElm[0].value != "") {
                if (getInputAmount() > 0) {
                    localStorage.transferAmuntCard = getInputAmount();

                    $.mobile.changePage('ConfigurePayment.html');
                }
            }
        });

        var callbackExchange = function () {
            var dataExchangeText = '1 ' + selectElm.val() + ' = ';
            $("#senderCurrency").text(dataExchangeText);
            var canTransfer = Helpers.maskMoney(getInputAmount() * $('#CurrentExchangeRate').text()) + ' ' + localStorage.RecipientCurrencyCode;
            $('#CantTransfer').text(canTransfer);
            localStorage.AmountTransfer = Helpers.maskMoney(getInputAmount());
            localStorage.TheyGet = canTransfer;
            localStorage.CurrentTransferFee = $("#CurrentTransferFee").val();
            localStorage.FeeCurrencyId = $("#FeeCurrencyId").val() == "" ? localStorage.CurrencyCodeCard : $("#FeeCurrencyId").val();
            localStorage.ExchangeRate = dataExchangeText + Helpers.maskMoney($("#CurrentExchangeRate").text()) + ' ' + $("#DisburseCountryDefaultCurrency").text();
            var sendTotal = Number(getInputAmount()) + Number(localStorage.CurrentTransferFee);
            localStorage.SendTotal = Helpers.maskMoney(sendTotal) + ' ' + localStorage.FeeCurrencyId;
        };
    };
    var populateCurrency = function () {

        vcCurrencyCard = vcCurrencyCard || new ViewConector({
            container: view.find('#SelectCurrency'),
            viewContentType: ViewContentType.SELECT,
            actionName: 'CountryLookup?countryId=' + localStorage.CountryCode + '&fakeParam=',
            actionParameter: null,
            typeAction: "GET",
            configSettings: { value: "CurrencyCode", text: "CurrencyName" }
        });

        vcCurrencyCard.execute();
    };

    var render = function () {
        populateCurrency();
            view.find("#PersonaSend").text(context.nameRecipient);
    };
    return {
        init: initialize,
        render: render,
        setHandlers: setHandlers,
        //getCrossData: getCrossData
        setDisplayError: function (showErr) {
            showError = showErr;
        },
        setRecipientTarget: function (recip) {
            context = recip;
        }
    };
})();


$(document).on('pageshow', '#configureAmount', function (e) {

    var view = $("#configureAmount");
    var dataContext = null;
    var currentUrl = $.mobile.activePage.data('url');
    if (currentUrl.indexOf("redirectFrom=search") >= 0) {
        var dataTemp = JSON.parse(sessionStorage.selectRecipientCard);
        dataContext = jnbs.sendToCard.recipientListTarget.createRecipientModel(dataTemp).attributes;
    } else {
        dataContext = jnbs.sendToCard.recipientListTarget.getRecipientSelected();
    }

    var app = jnbs.sendToCard.configureAmount;
    app.init(view, dataContext);
    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    app.setHandlers();
    app.render();

    if (localStorage.transferAmuntCard != undefined) {
        $("#configureAmount #InputAmount").val(localStorage.transferAmuntCard);
        $("#configureAmount #SelectCurrency option[value='" + localStorage.CurrencyCodeCard + "']").attr('selected', 'selected');
        $("#configureAmount #SelectCurrency").selectmenu('refresh');
        $("#configureAmount #SelectCurrency").change();
    }

});

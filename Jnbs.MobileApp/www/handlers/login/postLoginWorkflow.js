﻿var postLoginWorkflow = (function () {
    var view,
        validateLocation, getTokenData, getUserInfo,
        getUserProfile, setCurrentCountry,
        runEndStep, toogleButtonAgain, executeRedirect, redirectToQuestions;

    redirectToQuestions = function () {
        ServiceConnection.getData({
            actionName: "core/connect/userinfo",
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {

               console.log(JSON.stringify(response));
                var data = {
                    userLogged: _.pick(response, "given_name", "family_name", "email", "nickname","phone_number")

                    //userLogged: { //SOLO POR TEST!!!!!!!!!
                    //    given_name: "Ramiro",
                    //    family_name: "Allen",
                    //    email:""
                    //}
                };

            

                CustomStorage.session.add("preRegisterUserData", JSON.stringify(data));
                $.mobile.changePage("../customer/wizardSignup/questionOne.html");

            },
            errorCallback: function (dataError) {

                Helpers.progressBox.stop();
                customErrorDisplay("post-register: error calling userInfo");
                console.log(textError(dataError));

                toogleButtonAgain(true);


            }
        });


    };

    var customErrorDisplay = function (msg) {
        view.find("#error").show();
        view.find("#textMessage").text(msg);
    };
    var textError = function (dataError) {
        var strError = JSON.stringify(dataError);
        var idx = strError.length * 0.75;
        return strError.substr(0, idx);
    }
    toogleButtonAgain = function (visible) {
        if (visible == true) {
            view.find("#area-button-again").show();
        } else {
            view.find("#area-button-again").hide();
        }
    };

    runEndStep = function () {

        Helpers.setUserNameInMenu($("#leftmenu"));
        //  $.mobile.changePage("../Send/Send.html");
        executeRedirect();

    };
    validateLocation = function () {

        ServiceConnection.getData({
            actionName: 'SystemConfiguration?openId='+localStorage.OpenIdUser +'&countryId=' + localStorage.CountryCode + '&region=' + localStorage.Region+'&getLocationInfo=""',
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {

                localStorage.IsAllowedLocation = response.IsTransferAllowed;  
                Helpers.progressBox.stop();
                runEndStep();

            },
            errorCallback: function (dataError) {
                Helpers.progressBox.stop();
                localStorage.IsAllowedLocation = false;
                runEndStep();
                //ErrorBox.show($("#errorContent"), "error calling geo.json service");
            }
        });
    }

    getTokenData = function (getUserInfo) {
        console.log(localStorage.access_token + " f1");

        Helpers.progressBox.start("loading data");
        ServiceConnection.getData({
            actionName: "core/connect/accessTokenValidation?token=" + localStorage.access_token,
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {
                localStorage.OpenIdUser = response.sub;
                getUserInfo(response.sub);
            },
            errorCallback: function (dataError) {

                customErrorDisplay("error calling accessTokenValidation service");
                console.log("accessTokenValidation error:");
                console.log(textError(dataError));
                Helpers.progressBox.stop();
                toogleButtonAgain(true);

            }
        });
    }

    getUserInfo = function (sub) {
        console.log(sub);
        console.log(localStorage.access_token + " f2");
        ServiceConnection.getData({
            actionName: "accounts?openId=" + sub,
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {

                if (response == null) {
                    redirectToQuestions();
                } else {
                    localStorage.MtsId = response.MtsId;
                    getUserProfile(response.MtsId);
                }


            },
            errorCallback: function (dataError) {

                Helpers.progressBox.stop();
                customErrorDisplay("there was a problem");
                console.log(textError(dataError));

                toogleButtonAgain(true);


            }
        });
    }

    getUserProfile = function (mstId) {



        ServiceConnection.getData({
            actionName: "emoneyprofile?number=" + mstId + "&searchType=1",
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {
                localStorage.UserProfile = JSON.stringify(response);
                console.log("se consiguio userProfile");
                if (localStorage.IsAllowedLocation == undefined) setCurrentCountry();
                else {
                    Helpers.progressBox.stop();
                    runEndStep();
                }

            },
            errorCallback: function (dataError) {
                Helpers.progressBox.stop();
                toogleButtonAgain(true);
                customErrorDisplay("error calling emoneyprofile service");
                console.log("emoneyprofile ERROR: ");
                console.log(textError(dataError));

            }
        });
    }

    setCurrentCountry = function () {


        Helpers.progressBox.start("loading data");


        Helpers.setGeoLocalization(function (response) {
            localStorage.CountryCode = response.CountryCode;
            localStorage.Region = response.Region;
            console.log("datos de geo obtenidos");
            validateLocation();
        }, function (dataError) {
            Helpers.progressBox.stop();
            customErrorDisplay("error calling geolocalization service");
            toogleButtonAgain(true);

        });

    }
    var init = function ($view, currentUrl) {
        view = $view;
        if (currentUrl.indexOf("redirectToHistoryFilter") >= 0) {
            executeRedirect = function () {
                $.mobile.changePage("../History/History.html?showFilterPage=1");
            };
        }
        else {
            executeRedirect = function () {
                $.mobile.changePage("../Send/Send.html");
            };
        }
        toogleButtonAgain(false);
        view.find("#btnAgain").click(function () {
            $.mobile.changePage("../../index.html");

        });
        if (localStorage.UserProfile == undefined) {
            getTokenData(getUserInfo);
        }
        else {
            if (localStorage.IsAllowedLocation == undefined) {
                //setCurrentCountry();
                validateLocation();
            }
            else {
                runEndStep();
            }
        }

    };
    return {
        initialize: init
    };
})();

$(document).on('pageshow', '#postLogin', function (e) {

    var currentUrl = $.mobile.activePage.data('url');

    postLoginWorkflow.initialize($("#postLogin"), currentUrl);
});

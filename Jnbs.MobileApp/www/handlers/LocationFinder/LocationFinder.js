﻿var geoPoints;
var map;
var colours;
var markers = [];
$(document).on("pageshow", "#LocationFinder", function () {

    getJNPoints();

    $("#search-point").click(function () {
        renderRightMenu();
    });
   
});

var getJNPoints = function () {
    if (geoPoints == undefined) {
        Helpers.progressBox.start("loading...");
        ServiceConnection.getData({
            actionName: 'NSystemConfiguration',
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {
                geoPoints = response;
                colours = randomColor({
                    count: geoPoints.length
                });
       
                if (navigator.connection.type === Connection.NONE || (window.google !== undefined && window.google.maps)) {
                    renderMap();
                } else {
                    $.getScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyA_z-7SYoz_m_RZde4jKMVU46i0PubSDDE&sensor=true&callback=renderMap');
                }


            },
            errorCallback: function (dataError) {
                Helpers.progressBox.stop();
                ErrorBox.show($("#errorContent"), "Error getting geo information");
            }
        });
    } else {
        Helpers.progressBox.start("loading...");
        renderMap();
    }

}


var renderMap = function () {
    var options = {
        enableHighAccuracy: true,
        timeout: 30000,
        maximumAge: 0
    };
  
    navigator.geolocation.getCurrentPosition(function success(pos) {
        map = createMap(pos.coords.latitude, pos.coords.longitude);
        renderPoints();
        Helpers.progressBox.stop();
    },
    function error(err) {
        Helpers.progressBox.stop();
        ErrorBox.show($("#errorContent"), "An error occurs getting your location. Please activate the GPS on your phone and try again.");
    }, options);
    }

    function createMap(latitude, longitude) {
        var latlng = new google.maps.LatLng(latitude, longitude);
        var myOptions = {
            zoom: 9,
            center: latlng
    };
    var divmap = document.getElementById("map-canvas");
    var map = new google.maps.Map(divmap,
                myOptions);

        var bikeLayer = new google.maps.BicyclingLayer();
        bikeLayer.setMap(map);
    return map;
    }

    var filterPoints = function (text) {
        var filteredPoints = [];
        text = text.replace(" ", ",");
        var keys = text.split(",");
        $.each(geoPoints, function (i, point) {
            var obj = JSON.stringify(point);
            var flag = false;
            $(keys).each(function (i, key) {
                if (flag) {
                    if (obj.toLowerCase().indexOf(key.toLowerCase()) > -1) flag = true;
                    else {
                        flag = false;
                        return false;
                    }
                }
                else
                    if (i == 0)
                        if (obj.toLowerCase().indexOf(key.toLowerCase()) > -1) flag = true;
            });
            if (flag) {
                point.Colour = colours[i];
                filteredPoints.push(point);
                addPointToMap(point);
            }
        });

        if (filteredPoints.length > 0) {
            filteredPoints.sort(function (a, b) {
                return a.AddressCountry.toLowerCase() == localStorage.CountryName.toLowerCase() ? -1 : 1;
            })
            map.setCenter(new google.maps.LatLng(filteredPoints[0].Latitude, filteredPoints[0].Longitude));
        }
        return filteredPoints;
    };

var renderRightMenu = function () {

    var textToFind = $("#search-text").val();
    if (textToFind != "") {
        Helpers.progressBox.start("loading...");
        removeMarkers();
        var filteredPoints = filterPoints(textToFind);
        var vcPoints = new ViewConector({
                container: $("#points-list"),
                viewContentType: ViewContentType.DYNAMIC,
                template: '<li class="pointList"><input type="hidden" class="control title" data-value="LocationName"></input><div class="circle"><input type="hidden" data-value="Colour" class="control colour-circle"></input><i class="ss-location"></i></div><h4 class="control" data-value="LocationName"></h4><p class="control" data-value="Address1"></p><p class="control" data-value="Address2"></p><span class="control" data-value="Address4"></span>&#44;&#32<span class="control" data-value="AddressCountry"></span></li>'
    });
    vcPoints.response = filteredPoints;
    vcPoints.execute(function () {
        paintCircles();
        Helpers.progressBox.stop();
});

}
};

var paintCircles = function () {
    $.each($(".colour-circle"), function (key, value) {
        var colour = $(this).val();
        $(this).parent().css('background-color', colour);
    });
}


var renderPoints = function () {
    $.each(geoPoints, function (i, point) {
        point.Colour = colours[i];
        addPointToMap(point);
});
};


function addPointToMap(point) {

    var latLng = new google.maps.LatLng(point.Latitude, point.Longitude);

    var marker = new google.maps.Marker({
        position: latLng,
            animation: google.maps.Animation.DROP,
            icon: pinSymbol(point.Colour),
            title: point.LocationName
});

infowindow = new google.maps.InfoWindow({
    content: ""
});

map.addListener('click', function () {
    infowindow.close();
});

marker.setMap(map);

marker.addListener('click', function () {
    if (map.getZoom() < 15) {
            map.setZoom(15);
}

        map.panTo(marker.getPosition());

        infowindow.setContent(point.LocationName + ' ' +point.Address1 + ' ' +point.Address2 + ' ' +point.Address3);
        infowindow.open(map, marker);
    });
    markers.push(marker);
    }

    function pinSymbol(color) {
        return {
                path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
            fillColor: color,
            fillOpacity: 1,
            strokeColor: '#000',
            strokeWeight: 1,
            scale: 1,
};
};

function removeMarkers() {
    for (i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    markers =[];
    }


$(document).on("click", ".pointList", function (e) {
    e.preventDefault();
    var title = $(this).find(".title").val();

    var marker = getMarker(title);

    google.maps.event.trigger(marker, 'click');
    $("#right-menu").panel("close");
});


var getMarker = function (title) {
    for (i = 0; i < markers.length; i++) {
        if (markers[i].title == title)
            return markers[i];
    }
    return null;
}
﻿var vcDebitCardsList;

//jnbs.creditCards = jnbs.creditCards || {};
//jnbs.creditCards.registration = jnbs.creditCards.registration || {};

jnbs.creditCards.registration.cardList = (function () {
    var cardModel = null, view = null, cardCollection = null,
        listContent = null, getCardCollectionLocal = null,
        showCardDetail = null, CardItemView = null, CardListView = null,
        setCardSelected = null, isDataCached = null;

    setCardSelected = function (cardModel) {
        CustomStorage.session.add("cardItemSelected", JSON.stringify(cardModel.attributes));
    };

    var setup = function () {
        cardModel = Backbone.Model.extend({
            defaults: {
                accountCardId: "",
                cardDescription: "",
                recurringDetailsReference: "",
                isConfirmed: false,
                cardType: ""
            },
            initialize: function () {
                this.set("cardStatus", this.getStatus());
                this.set("logoCard", this.getLogoByCardType())

            },
            getLogoByCardType: function () {
                switch (this.get("cardType")) {
                    case "visa":
                        return 'visa';
                    case "mc":
                        return 'masterCard';
                    case "amex":
                        return 'americanExpress';
                    default:
                        return "ss-creditcard";
                }
            },
            getStatus: function () {

                if (this.get("isConfirmed") == true)
                    return "Confirmed";
                return "Unconfirmed";
            },
            hasId: function (id) {
                return this.get("accountCardId") == id;
            }
        });

        cardCollection = Backbone.Collection.extend({
            model: cardModel,
            findCardById: function (id) {
                var item = this.chain()
                            .filter(function (item) {
                                return item.hasId(id);
                            })
                            .first().value();

                if (item) {
                    return item;
                }
                throw "card not exist";
            }

        });

        CardItemView = Backbone.View.extend({
            tagName: "li",
            className: "ui-li-has-thumb ui-nodisc-icon ui-alt-icon",
            template: Handlebars.compile(view.find("#cardItemTemplate").html()),
            events: {
                "click": "showDetail"
            },
            render: function () {

                //if (this.model.attributes["isFirst"] == true)
                //    this.$el.html("<h6>" + this.model.attributes["group"] + "</h6>");
                
                this.$el.append(this.template(this.model.attributes));

                
                return this;
            },
            showDetail: function (e) {
                var id = this.model.get("accountCardId");
                showCardDetail(id);
            }

        });

        CardListView = Backbone.View.extend({
            el: view.find(view.find('#list-cards')),
            render: function () {
                this.$el.empty();
                this.collection.each(function (item) {
                    var itemView = new CardItemView({ model: item });
                    if (item.attributes["isFirst"] == true)
                        var header = "<h5> "+ item.attributes["group"] +" </h5>";
                    this.$el.append(header);
                    var html = itemView.render().el;
                    this.$el.append(html);
                }, this);

                this.postRender();
            },
            postRender: function () {
                this.$el.find('.ss-creditcard').each(function () {
                    //var icon = $(this).find('.ss-creditcard');
                    var icon = $(this);
                    var name = $(this).closest('li').find('h4');
                    var lowerName = name.text().toLowerCase();
                    switch (lowerName) {
                        case "visa":
                            icon.addClass('visa');
                            icon.removeClass('ss-creditcard');
                            break;
                        case "mc":
                            icon.addClass('masterCard');
                            icon.removeClass('ss-creditcard');
                            break;
                        case "amex":
                            icon.addClass('americanExpress');
                            icon.removeClass('ss-creditcard');
                            break;
                        default:
                    }
                });
                this.$el.find('.label.warning').each(function() {
                    if ($(this).text().length == 0 ) {
                        $(this).remove();
                    }
                });
                //this.$el.text('canada');
            }
        });

    };

    var saveDataLocal = function (response) {
        CustomStorage.session.add(keyCardListSession, JSON.stringify(response));

    };

    showCardDetail = function (id) {
        var cardCollection = getCardCollectionLocal();
        var card = cardCollection.findCardById(id);
        setCardSelected(card);

        $.mobile.changePage("dcCardsDetails.html");
    };
    isDataCached = function () {
        return CustomStorage.session.exist(keyCardListSession);
    };
    getCardCollectionLocal = function () {
        var response = CustomStorage.session.get(keyCardListSession, function (d) { return JSON.parse(d); });
        var cardList = response.map(function (data) {


            return new cardModel({
                accountCardId: data.AccountCardId,
                cardDescription: data.cardDescription,
                recurringDetailsReference: data.RecurringDetailsReference,
                isConfirmed: data.IsConfirmed,
                cardType: data.CardType,
                confirmedText: data.confirmedText,
                group: data.group,
                isFirst: data.isFirst
            });
        });

        var list = new cardCollection(cardList);

        return list;
    };

    var loadCards = function () {

        var drawCards = function (lst) {
            var listView = new CardListView({ collection: lst });
            listView.render();
        };

        if (isDataCached()) {
            drawCards(getCardCollectionLocal());
            return;
        }

        Helpers.progressBox.start("Loading...");
        ServiceConnection.getData({
            actionName: 'CreditCard?openId=' + localStorage.OpenIdUser + "&country=" + localStorage.CountryCode,
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {                
                
                var resp2 = HelpersCard.basicMap2(response, { all: true });
              
                saveDataLocal(resp2);
                var lst1 = getCardCollectionLocal();
           
                drawCards(lst1);

                Helpers.progressBox.stop();
            },
            errorCallback: function (dataError) {
                Helpers.progressBox.stop();

                showError("there was an error when attempting to load historical");
            }
        });
    };
    init = function ($view) {
        view = $view;
        setup();
        loadCards();
        view.find("#addCard").click(function () {
            var appRegApp = jnbs.creditCards.registration.addCard;
            appRegApp.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
            appRegApp.init(view);
        });

        listContent = view.find('#list-cards');
        //vcDebitCardsList = vcDebitCardsList || new ViewConector({
        //    container: listContent,
        //    viewContentType: ViewContentType.DYNAMIC,
        //    actionName: 'CreditCard?openId=' + localStorage.OpenIdUser,
        //    actionParameter: null,
        //    typeAction: "GET",
        //    tagName: "card",
        //    enableLoading: true,
        //    template: '<li class="ui-li-has-thumb ui-nodisc-icon ui-alt-icon"> \
        //              <a  class="ui-btn ui-btn-icon-right ui-icon-carat-r showDetailCurrentCard" > \
        //                 <div class="circle"><div class="ss-creditcard"></div></div> \
        //                 <h4 data-value="CardType" class="control"></h4> \
        //                 <p data-value="cardDescription" class="text-gray control"></p> \
        //                 <p data-value="confirmedText" class="label warning control"></p> \
        //                <input type="hidden" class="control itemId" data-value="AccountCardId"/>\
        //              </a> \
        //           </li>',
        //    convertResponse: function (response) {
        //        return HelpersCard.basicMap(response, { all: true });

        //    }
        //});
        //vcDebitCardsList.execute(function (response) {

        //    saveDataLocal(response);
        //    var detailButton = view.find(".showDetailCurrentCard");

        //    detailButton.click(function (event) {
        //        event.stopPropagation();
        //        event.preventDefault();

        //        var id = $(this).find(".itemId").val();
        //        showCardDetail(id);
        //        return false;
        //    });

        //    view.find("#addCard").click(function () {
        //        var appRegApp = jnbs.creditCards.registration.addCard;
        //        appRegApp.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
        //        appRegApp.init(view);
        //    });

        //    //$('#list-cards li').each(function () {
        //    view.find('#list-cards .ss-creditcard').each(function () {
        //        //var icon = $(this).find('.ss-creditcard');
        //        var icon = $(this);
        //        var name = $(this).closest('li').find('h4');
        //        switch (name.text()) {
        //            case "visa":
        //                icon.addClass('visa');
        //                icon.removeClass('ss-creditcard');
        //                break;
        //            case "mc":
        //                icon.addClass('masterCard');
        //                icon.removeClass('ss-creditcard');
        //                break;
        //            case "amex":
        //                icon.addClass('americanExpress');
        //                icon.removeClass('ss-creditcard');
        //                break;
        //            default:
        //        }
        //    });

        //});

    };
    return {
        CardModel: cardModel,
        CardCollection: cardCollection,
        setCardSelected: setCardSelected,
        getCardModelSelected: function () {
            var raw = CustomStorage.session.get("cardItemSelected", function (d) { return JSON.parse(d); });
            return new cardModel(raw);
        },
        saveDataLocal: saveDataLocal,
        getCardCollectionLocal: getCardCollectionLocal,
        init: init
    }
})();
$(document).on('pageshow', '#cardCollectionView', function (e) {

    var currentUrl = $.mobile.activePage.data('url');

    if (currentUrl.indexOf("changeCard") >= 0) {
        vcDebitCardsList = null;

    }
    var view = $("#cardCollectionView");
    var appPage = jnbs.creditCards.registration.cardList;
    appPage.init(view);








});

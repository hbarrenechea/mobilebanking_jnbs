//jnbs.creditCards = jnbs.creditCards || {};
//jnbs.creditCards.registration = jnbs.creditCards.registration || {};

jnbs.creditCards.registration.addCard = (function () {
    var init, view, showError, showRegistrationPage, parseResult;

    parseResult = function (result) {
        // var url = "http://localhost/confirmTransfer.html?message=Your%20debit/credit%20card%20has%20been%20registered.%20To%20confirm%20registration,%20check%20your%20bank%20statment%20and%20confirm%20the%20amount%20charged.%20The%20amount%20will%20be%20refunded%20after%20confirmation.&status=success";
        var part1 = decodeURIComponent(result.split("?")[1]);
        var r1 = part1.match(/message=(.*)&/);
        var message = r1[1];
        var r2 = part1.match(/status=(.*)/);
        var status = r2[1];

        return {
            rawMessage: message,
            getMessageResult: function () {
                if (status.toLowerCase() == "success") {
                    return "Success: " + message;
                }
                return "ERROR: " + message;
            }
        };
    };
    showRegistrationPage = function () {
        //   var urlService = "https://qa.mcsystems.com:4150/payment/creditcardt"+"?accessToken=" + localStorage.access_token;
        //se usara mock
        var p1 = $.param({
            accessToken: localStorage.access_token,
            countryId: localStorage.CountryCode
        });
        var urlService = "https://qa.mcsystems.com:4150/payment/creditcard?" + p1;

        var externalWin = window.open(urlService, "_blank", "location=no,clearcache=yes,hidden=yes");
        var urlCheck = "confirmTransfer.html";
        Helpers.progressBox.start("loading...");
        $(externalWin).on('loadstop', function (e) {
            externalWin.show();
            Helpers.progressBox.stop(); // implement by yourself
        });
        $(externalWin).on('loadstart', function (e) {
            var url = e.originalEvent.url;

            console.log("start with " + url);
            if (url.indexOf(urlCheck) >= 0) {
                externalWin.close();
                var result = parseResult(url);
                showError(result.getMessageResult());
                handlerEventsBuss.trigger(handlerEventsBuss.OWN_CARD_CHANGE);
                $.mobile.changePage("dcCards.html?changeCard=1");
            }
        });
    };
    init = function ($view) {
        view = $view;
        showRegistrationPage();

    };

    return {
        init: init,
        setDisplayError: function (errorFunc) {
            showError = errorFunc;
        }

    };
})();

//$(document).on('pageshow', '#cardRegistration', function (e) {
//    var view = $("#cardRegistration");
//    //   var cardSelect = jnbs.creditCards.registration.addCard;
//    var appPage = jnbs.creditCards.registration.addCard;
//   // appPage.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
//    appPage.init(view);
//});
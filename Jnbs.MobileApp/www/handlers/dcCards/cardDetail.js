﻿jnbs.creditCards.registration.cardDetail = (function () {
    var view, currenCardModel, setupObjects, configHandlers,
        render, CardDetailView, deleteCard, redirectCardHome, confirmCard, afterDeleteCard,
        showError, popupDelete, popupConfirm, closePopupDelete;

    closePopupDelete = function () {
      //  popupDelete.popup("close");
    };

    afterDeleteCard = function () {
        console.log("after delete card...");
        handlerEventsBuss.trigger(handlerEventsBuss.OWN_CARD_CHANGE);
        closePopupDelete();
        redirectCardHome();
        console.log("End after delete card");
    };
    redirectCardHome = function () {
        $.mobile.changePage("dcCards.html?changeCard=1");
        //  CustomStorage.session.add("changeCard", 1);
    };
    confirmCard = function (referenceNumber, amount) {
        var data = {
            OpenId: localStorage.OpenIdUser,
            ReferenceNumber: currenCardModel.get("recurringDetailsReference"),
            ConfirmationAmount: amount
        };
        var closePopup = function () {
            //popupConfirm.popup("close");
        };
        Helpers.progressBox.start("Confirm");
        ServiceConnection.getData({
            actionName: "CreditCard",
            formData: JSON.stringify(data),
            headerContentType: "application/json",
            typeAction: "PUT",
            successCallback: function (response, status, xhr) {

                Helpers.progressBox.stop();
                if (response.Message.indexOf('confirmed') > -1) {
                    closePopup();
                    handlerEventsBuss.trigger(handlerEventsBuss.OWN_CARD_CHANGE);
                    redirectCardHome();
                }
                else {
                    popupConfirm.popup("close");
                    showError(response.Message);
                }
            },
            errorCallback: function (dataError) {

                Helpers.progressBox.stop();


                closePopup();
                showError("Error confirm card. Try again later");
                console.log("confirm Card param: " + JSON.stringify(data));
                console.log("confirm Card err: " + JSON.stringify(dataError));

            }
        });
    };
    deleteCard = function (referenceNumber, afterAction) {

        //if (1 == 1) {
        //    afterAction();
        //    return;
        //}

        var data = {
            OpenId: localStorage.OpenIdUser,
            ReferenceNumber: currenCardModel.get("recurringDetailsReference")
        };

        Helpers.progressBox.start("Deleting");
        ServiceConnection.getData({
            actionName: "CreditCard?" + $.param(data),
            formData: null,
            typeAction: "DELETE",
            successCallback: function (response, status, xhr) {

                Helpers.progressBox.stop();
                afterAction();



            },
            errorCallback: function (dataError) {

                Helpers.progressBox.stop();


                showError("Error deleting card. Try again later");
                console.log("Del Card param: " + JSON.stringify(data));
                console.log("Del Card: " + JSON.stringify(dataError));

            }
        });
    };
    setupObjects = function () {
        CardDetailView = Backbone.View.extend({
            el: view.find("#contentCard"),
            events: {
                "click .confirm": "confirmAction"

            },
            template: Handlebars.compile(view.find(".card-data").html()),
            render: function () {
                var cardHtml = this.template(this.model.attributes);
                this.$el.empty();
                this.$el.append(cardHtml);
                this.renderStatus();

                return this;
            },
            renderStatus: function () {
                if (this.model.get("isConfirmed") == true) {
                    this.$el.find(".confirm").hide();
                } else {
                    this.$el.find(".confirm").show();
                }
            },
            confirmAction: function () {
                // alert("confirm card " + this.model.get("cardDescription"));
                popupConfirm.bind({
                    popupbeforeposition: function (event, ui) {
                        popupConfirm.find("#amountConfirm").val("");
                        view.find("#confirm-cardType").text(currenCardModel.get("cardType"));
                        view.find("#confirm-cardNumber").text(currenCardModel.get("cardDescription"));
                    }
                });

                popupConfirm.popup("open");
            }
        });
    };
    render = function () {
        var cardView = new CardDetailView({
            model: currenCardModel
        });
        cardView.render();


    };
    configHandlers = function () {
        popupDelete.bind({
            popupbeforeposition: function (event, ui) {
                view.find("#del-cardType").text(currenCardModel.get("cardType"));
                view.find("#del-cardNumber").text(currenCardModel.get("cardDescription"));
            }
        });

        view.find("#delete-action").click(function (event) {
            event.stopPropagation();
            event.preventDefault();

            deleteCard(currenCardModel.get("recurringDetailsReference"), afterDeleteCard);
            return false;

        });

        view.find("#removeCard").click(function () {
            popupDelete.popup("open");
        });

        view.find(".confirm-action").click(function () {
            var amount = Number(popupConfirm.find("#amountConfirm").val());

            if (amount > 0)
                confirmCard(currenCardModel.get("recurringDetailsReference"), amount);
        });
    };
    var init = function ($view, $cardModel) {
        view = $view;
        currenCardModel = $cardModel;
        popupDelete = view.find("#deleteCard");
        popupConfirm = view.find("#confirmCard");
        setupObjects();
        render();
        configHandlers();

    };
    return {
        init: init,
        setDisplayError: function (errorFunc) {
            showError = errorFunc;
        }

    };
})();

$(document).on('pageshow', '#cardDetail', function (e) {
    var view = $("#cardDetail");
    var cardSelect = jnbs.creditCards.registration.cardList.getCardModelSelected();
    var appPage = jnbs.creditCards.registration.cardDetail;
    appPage.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    appPage.init(view, cardSelect);
});
﻿jnbs.history = (function () {

    var showError, view, context, populateHistoryFake,
        setHandlers,
        ItemHistoryModel, PopupDateModel,
        ItemHistoryCollection,
        ItemHistoryView, redirectFilter,
        HistoryListView,
        createEntityBackbone,
        renderMainView,
            lastCollection, PopupView;


    redirectFilter = function () {
        $.mobile.changePage("historyFilterPage.html");
    };
    var createHistory = function (paymentHistory) {
        var result = paymentHistory.map(function (payment) {
            var getReceive = function () {
                if (payment.ReceivingPerson) {
                    console.log("persona receive");
                    var p1 = payment.ReceivingPerson;
                    return p1.FirstName + " " + p1.LastName;
                }
                if (payment.ReceivingAccount) {
                    console.log("Account receive");
                    var acc = payment.ReceivingAccount;
                    return acc.PrimAccountHolderFirstName + " " + acc.PrimAccountHolderSurname;
                }

                return "receive";
            };

            var getReceiveAddress = function () {

                var addressObj = null;
                if (payment.ReceivingPerson) {

                    var p1 = payment.ReceivingPerson;
                    addressObj = p1.Address;
                } else {
                    if (payment.ReceivingAccount) {

                        var acc = payment.ReceivingAccount;
                        addressObj = acc.Address;
                    }
                }

                if (addressObj == null) return "-";

                var addressList = [
                    addressObj.Address1 || "-", addressObj.Address2 || "-", addressObj.Address3 || "-",
                    addressObj.Address4 || "-"
                ];
                return Helpers.reduceAddress(addressList);

            };


            var totalSend = function (p) {
                return Number(p.TransactionAmount) + Number(p.TransferFeeAmount);
            }
            return {
                receiveName: getReceive(),
                amountTransaction: Helpers.maskMoney(payment.TransactionAmount),
                statusTransaction: payment.TransferStatusId,
                dateTransaction: Helpers.maskDate(payment.TransactionDate),
                transferTypeId: payment.TransferType,
                transferType: Enumerations.getTransferTypeById(payment.TransferType),
                dataPopup: {
                    sendFullName: context.currentUser.fullName,
                    sendAddress: context.currentUser.address,
                    sendAmount: Helpers.maskMoney(payment.TransactionAmount) + " " + payment.TransactionCurrencyId,
                    sendFee: Helpers.maskMoney(payment.TransferFeeAmount) + " " + payment.TransferFeeCurrencyId,
                    sendExchangeRate: Helpers.maskMoney(1) + " " + payment.TransactionCurrencyId + " = " + Helpers.maskMoney(payment.ExchangeRate) + " " + payment.DisbursementCurrencyId,
                    sendTotal: Helpers.maskMoney(totalSend(payment)) + " " + payment.TransactionCurrencyId,
                    receiveAmount: Helpers.maskMoney(payment.DisbursementAmount) + " " + payment.DisbursementCurrencyId,
                    transactionId: payment.TransactionId,
                    receiveFullName: getReceive(),
                    receiveAddress: getReceiveAddress(),
                    receiveAccount: payment.ReceivingAccount != null ? payment.ReceivingAccount.AccountNumber : "",
                    receiveAccountType: payment.ReceivingAccount != null ? payment.ReceivingAccount.AccountType : "",
                    receivePersonMobileTelephone: payment.ReceivingPerson != null ? payment.ReceivingPerson.MobileTelephone : "",
                    receivePersonCardNumber: payment.ReceivingPerson != null ? payment.ReceivingPerson.MTCardNumber : "",
                    receiveInstitution: payment.ReceivingAccount != null ? payment.ReceivingAccount.InstitutionBranchId : "",
                    receiveInstitutionName: payment.ReceivingAccount != null ? payment.InstitutionName : ""
                } //EXAMPLE
            };
        });

        return result;
    };


    // private

    renderMainView = function (data) {
        lastCollection = new ItemHistoryCollection();

        _.each(data, function (item) {

            var modelInstance = new ItemHistoryModel({

                receiveName: item.receiveName,
                amountTransaction: item.amountTransaction,
                statusTransaction: item.statusTransaction,
                dateTransaction: item.dateTransaction,
                transferTypeId: item.transferTypeId,
                transferType: item.transferType,
                dataPopup: item.dataPopup
            });
            lastCollection.add(modelInstance);
        });

        var mainView = new HistoryListView({
            el: view.find("#history-list"),
            collection: lastCollection
        });

        mainView.render();

    };


    createEntityBackbone = function () {
        /*
   * Defining Model and Views
   */

        ItemHistoryModel = Backbone.Model.extend({});
        PopupDateModel = Backbone.Model.extend({});
        ItemHistoryCollection = Backbone.Collection.extend({
            model: ItemHistoryModel
        });

        //defining view popup
        PopupViewP2P = Backbone.View.extend({

            template: Handlebars.compile(view.find("#p2p-popup").html()),
            render: function () {
                var dataHtml = this.template(this.model.toJSON());
                this.$el.empty();
                this.$el.html(dataHtml);
                this.$el.popup("open");
            }
        });

        PopupViewPensionTransfer = Backbone.View.extend({

            template: Handlebars.compile(view.find("#pensiontransfer-popup").html()),
            render: function () {
                var dataHtml = this.template(this.model.toJSON());
                this.$el.empty();
                this.$el.html(dataHtml);
                this.$el.popup("open");
            }
        });

        PopupViewGlobalAccountTransfer = Backbone.View.extend({

            template: Handlebars.compile(view.find("#globalaccounttransfer-popup").html()),
            render: function () {
                var dataHtml = this.template(this.model.toJSON());
                this.$el.empty();
                this.$el.html(dataHtml);
                this.$el.popup("open");
            }
        });

        PopupViewThirdPartyTransfer = Backbone.View.extend({

            template: Handlebars.compile(view.find("#thirdpartytransfer-popup").html()),
            render: function () {
                var dataHtml = this.template(this.model.toJSON());
                this.$el.empty();
                this.$el.html(dataHtml);
                this.$el.popup("open");
            }
        });

        PopupViewBillPaytransfer = Backbone.View.extend({

            template: Handlebars.compile(view.find("#billpaytransfer-popup").html()),
            render: function () {
                var dataHtml = this.template(this.model.toJSON());
                this.$el.empty();
                this.$el.html(dataHtml);
                this.$el.popup("open");
            }
        });

        PopupViewAccountTransfer = Backbone.View.extend({

            template: Handlebars.compile(view.find("#accounttransfer-popup").html()),
            render: function () {
                var dataHtml = this.template(this.model.toJSON());
                this.$el.empty();
                this.$el.html(dataHtml);
                this.$el.popup("open");
            }
        });

        PopupViewInstitutionTransfer = Backbone.View.extend({

            template: Handlebars.compile(view.find("#institutiontransfer-popup").html()),
            render: function () {
                var dataHtml = this.template(this.model.toJSON());
                this.$el.empty();
                this.$el.html(dataHtml);
                this.$el.popup("open");
            }
        });
        
        PopupViewMobileTopup = Backbone.View.extend({

            template: Handlebars.compile(view.find("#mobiletopup-popup").html()),
            render: function () {
                var dataHtml = this.template(this.model.toJSON());
                this.$el.empty();
                this.$el.html(dataHtml);
                this.$el.popup("open");
            }
        });

        PopupViewMTCardPayment = Backbone.View.extend({

            template: Handlebars.compile(view.find("#mtcardpayment-popup").html()),
            render: function () {
                var dataHtml = this.template(this.model.toJSON());
                this.$el.empty();
                this.$el.html(dataHtml);
                this.$el.popup("open");
            }
        });
        

        //defining view item history
        ItemHistoryView = Backbone.View.extend({
            tagName: "li",
            className: "ui-li-has-thumb ui-first-child ui-nodisc-icon ui-alt-icon",
            template: Handlebars.compile($("#item-history-template").html()),
            templateEmpty: Handlebars.compile($("#item-history-empty-template").html()),
            events: {
                "click .hist-open-popup": "openHistoryPopup"
                
            },
            render: function () {
                var dataHtml = this.template(this.model.toJSON());
                this.$el.html(dataHtml);
                return this;

            },
            renderEmpty:function(){ 
                // this.$el.html(this.templateEmpty());
                // return this;
                showError("No results found");
            },
            openHistoryPopup: function (e) {
                var typeTransfer = $(e.target).closest("li").attr("class").split(' ').pop();
                var modelPop = this.model.get("dataPopup");
                switch (typeTransfer) {
                    case "pensiontransfer": var viewPopup = new PopupViewPensionTransfer({
                        model: new PopupDateModel(modelPop),
                        el: view.find("#pensiontransfer-popup")
                    });
                        break;
                    case "p2p": var viewPopup = new PopupViewP2P({
                        model: new PopupDateModel(modelPop),
                        el: view.find("#p2p-popup")
                    });
                        break;
                    case "globalaccounttransfer": var viewPopup = new PopupViewGlobalAccountTransfer({
                        model: new PopupDateModel(modelPop),
                        el: view.find("#globalaccounttransfer-popup")
                    });
                        break;
                    case "thirdpartytransfer": var viewPopup = new PopupViewThirdPartyTransfer({
                        model: new PopupDateModel(modelPop),
                        el: view.find("#thirdpartytransfer-popup")
                    });
                        break;
                    case "billpaytransfer": var viewPopup = new PopupViewBillPaytransfer({
                        model: new PopupDateModel(modelPop),
                        el: view.find("#billpaytransfer-popup")
                    });
                        break;
                    case "accounttransfer": var viewPopup = new PopupViewAccountTransfer({
                        model: new PopupDateModel(modelPop),
                        el: view.find("#accounttransfer-popup")
                    });
                        break;
                    case "institutiontransfer": var viewPopup = new PopupViewInstitutionTransfer({
                        model: new PopupDateModel(modelPop),
                        el: view.find("#institutiontransfer-popup")
                    });
                        break;
                    case "mobiletopup": var viewPopup = new PopupViewMobileTopup({
                        model: new PopupDateModel(modelPop),
                        el: view.find("#mobiletopup-popup")
                    });
                        break;
                    case "mtcardpayment": var viewPopup = new PopupViewMTCardPayment({
                        model: new PopupDateModel(modelPop),
                        el: view.find("#mtcardpayment-popup")
                    });
                        break;
                }
                
                viewPopup.render();
            }

        });

        //defining view item list history
        HistoryListView = Backbone.View.extend({

            render: function () {
                this.$el.empty();
                if (this.collection.length == 0) {
                    var liView = new ItemHistoryView();
                    liView.renderEmpty();
                    //this.$el.append(innerHtml);
                } else {
                    this.collection.forEach(function (item) {
                        var liView = new ItemHistoryView({ model: item });
                        if (item.attributes.transferTypeId == 4)
                        {
                            var a = "";
                        }
                        $(liView.el).addClass(Enumerations.getTransferTypeById(item.attributes.transferTypeId).toLowerCase());
                        var innerHtml = liView.render().el;
                        this.$el.append(innerHtml);
                    }, this);
                }
            },

        });
        /*
        * ============
        */
    };

    //var parseResponse = function (responseItems) {
    //    $(responseItems).each(function(index, item){
    //        if (item.TransferType == Enumerations.getTransferTypeValueByName("MTCardPayment") && item.TransferStatusId == "X")
    //            item.TransferStatusId = "J";
    //        else
    //            item.TransferStatusId = (item.TransferType == Enumerations.getTransferTypeValueByName("ThirdPartyTransfer") && item.TransferStatusId == "U") ? "H" : item.TransferStatusId;
                         
    //    });
    //    return responseItems;
    //};

    var populateWithService = function () {

        var drawData = function (resp) {
            var resultHistory = createHistory(resp);
            renderMainView(resultHistory);
            if (context.showFilterPage && context.showFilterPage == true) {
                redirectFilter();
                delete context.showFilterPage;
            }
        };

        //if result was cache
        //if (CustomStorage.session.exist(keyHistoryListSession) && context.hasFilter()==false) {
        //    var resp = CustomStorage.session.get(keyHistoryListSession, function (r) { return JSON.parse(r); });
        //    drawData(resp);
        //    return;
        //}

        //if not cache then...continue below

        var url = "TransactionHistory?";
        var queryStr = {
            mtsId: localStorage.MtsId,
            count: "",
            page: "",
            recipientMtsId: "",
            startDate: "",
            endDate: "",
            transactionId: "",
            transferType: ""
        };

        if (context.hasFilter()) {
            queryStr.startDate = context.filter.startDate;
            queryStr.endDate = context.filter.endDate;
            queryStr.transactionId = context.filter.transactionId;
            queryStr.transferType = context.filter.transferType;
        };


        var urlService = url + $.param(queryStr);

        Helpers.progressBox.start("getting history");
        ServiceConnection.getData({
            actionName: urlService,
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {
                var resp = response.TransactinHistoryResult.PaymentHistory;
                //resp = parseResponse(resp);
                if(context.hasFilter()==false)
                    CustomStorage.session.add(keyHistoryListSession, JSON.stringify(resp));

                drawData(resp);
                Helpers.progressBox.stop();
            },
            errorCallback: function (dataError) {
                Helpers.progressBox.stop();

                showError("there was an error when attempting to load historical");
            }
        });

    };
    //public implementation
    var init = function ($view, $context) {
        view = $view;
        context = $context;
        Helpers.setUserNameInMenu(view);
        setHandlers();
    };


    var render = function () {
        //setHandlerRefreshData();
        createEntityBackbone();
        populateWithService();

    };


    setHandlers = function () {
        view.find("#showFilter").click(function () {
            // $.mobile.changePage("historyFilterPage.html");
            redirectFilter();
        })
    };
    return {
        render: render,
        initialize: init,
        setDisplayError: function (showErr) {
            showError = showErr;
        }
    };
})();


$(document).on('pageshow', '#history-page', function (e) {

    var view = $("#history-page");
    var app = jnbs.history;
    var context = {
        currentUser: Helpers.getUserProfile()
    };

    //var s = Enumerations.getTransferTypeList();

    var currentUrl = $.mobile.activePage.data('url');
    //define default properties context
    context.hasFilter = function () { return false; };
    context.showFilterPage = false;
    //---apply conditional values
    if (currentUrl.indexOf("setFilter") >= 0) {
        context.filter = jnbs.historyFilterPage.getFilterInput();
        context.hasFilter = function () { return true; };
    }
    if (currentUrl.indexOf("showFilterPage") >= 0) {
        context.showFilterPage = true;
    }


    app.initialize(view, context);
    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));

    app.render(); 
});
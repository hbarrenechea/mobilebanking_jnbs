﻿jnbs.historyFilterPage = (function (_) {
    var view, createEntities, FilterModel, InputView, currentInputModel,
        showError, currentViewBb,
    getOption = _.template("<option <%= other %> value='<%= value %>' ><%= text %></option>"),
    useFilter = null;

    createEntities = function () {
        FilterModel = Backbone.Model.extend({
            defaults: {
                startDate: "",//yyyy-mm-dd
                endDate: "",
                transactionId: "",
                transferType: "",
                enableFilter: false
            },
            counterFilled: 0,
            initialize: function () {
                var self = this;

            },
            parseDate: function (strDate) {
                var arr = strDate.split('-');//yyy-mm-dd
                var year = Number(arr[0]);
                var month = Number(arr[1]) - 1;
                var day = Number(arr[2]);

                return new Date(year, month, day);

            },

            saveLocal: function () {
                CustomStorage.session.add("historyFilterData", JSON.stringify(this.attributes));

            },
            clearCacheModel:function() {
                CustomStorage.session.remove("historyFilterData");
            },
            getCachedModel: function () {

                if (CustomStorage.session.exist("historyFilterData")) {
                    var data = CustomStorage.session.get("historyFilterData",
                        function (d) {
                            return JSON.parse(d);
                        });
                    return new FilterModel(data);

                }
                return new FilterModel({ enableFilter: false });



            },
            validate: function (attrs) {
                var counterFilled = 0;
                if (attrs.startDate)
                    counterFilled++;
                if (attrs.endDate)
                    counterFilled++;
                if (attrs.transactionId)
                    counterFilled++;
                if (attrs.transferType)
                    counterFilled++;

                if (counterFilled == 0)
                    return "must be filled at last one filter";
                if (attrs.startDate.length > 0 || attrs.endDate.length > 0) {
                    if (!(attrs.startDate && attrs.endDate))
                        return "if you set a date, you must set the other also";

                    var dTo = this.parseDate(attrs.endDate);
                    var dFrom = this.parseDate(attrs.startDate);
                    if (!(dFrom <= dTo))
                        return "start date must be less than end date";
                }
            }
        });

        InputView = Backbone.View.extend({
            el: view,
            events: {
                "focus #transactionNumber": "clearTransaction",
                "change #transferTypeList": "changeTransferType",
                "click #applyFilter": "clickApply",
                "blur #transactionNumber": "changeTransaction",
                "click #dateFrom": "showPickerFrom",
                "click #dateTo": "showPickerTo",
                "click #useFilter": "clearFilter"

            },

            prevTransactionId: "",
            initialize: function () {
                var self = this;
                this.model.on("change:transactionId", function (item) {
                    var transId = item.get("transactionId");
                    var ctrl = self.$el.find("#transactionNumber");

                    ctrl.val(transId);


                });

                this.render();

                var previousData = self.model.getCachedModel();
                if (previousData.get("enableFilter") == true) {
                    useFilter.show();
                }
                if (previousData.get("enableFilter") == false) {
                    useFilter.hide();

                }
            },
            showPicker: function (inputDate, setModel) {



                inputDate.pickadate({
                    format: 'yyyy-mm-dd',
                    formatSubmit: 'yyyy-mm-dd',
                    onSet: function (context) {
                        setModel(inputDate.val());
                    }
                });
            },
            showPickerTo: function (e) {
                var inputDate = $(e.target);
                var self = this;
                this.showPicker(inputDate, function (val) {
                    self.model.set({ endDate: val });
                });
            },
            showPickerFrom: function (e) {
                var inputDate = $(e.target);
                var self = this;
                this.showPicker(inputDate, function (val) {
                    self.model.set({ startDate: val });
                });
            },
            render: function () {
                var cb = this.$el.find("#transferTypeList");

                cb.empty();
                cb.append(getOption({ other: "selected='selected' disabled", value: -1, text: "-Select-" }));
                _.chain(Enumerations.getTransferTypeList())
                 .map(function (item) {
                     return getOption({ other: "", value: item.value, text: item.name });
                 })
                 .each(function (opt) {
                     cb.append(opt);
                 });
            },
            changeDateFrom: function (e) {
                this.model.set({ startDate: this.$el.find("#dateTo").val() });
            },
            changeDateTo: function (e) {
                this.model.set({ endDate: this.$el.find("#dateFrom").val() });
            },
            clearTransaction: function (e) {
                //this.prevTransactionId = this.model.get("transactionId");
                this.model.set({ "transactionId": "" });
            },
            changeTransferType: function (e) {
                var value = $(e.target).find("option:selected").val();

                this.model.set({ "transferType": (value != "-1" && value != "" ? value : null) });


            },
            changeTransaction: function (e) {
                var newValue = $(e.target).val();
                if (newValue == "")
                    this.model.set({ "transactionId": this.prevTransactionId });
                else this.model.set({ "transactionId": newValue });
            },
            clickApply: function (e) {

                if (!this.model.isValid()) {
                    showError(this.model.validationError);
                    return;
                }

                handlerEventsBuss.trigger(handlerEventsBuss.HIST_CHANGE);
                //  CustomStorage.session.add("historyFilterData", JSON.stringify(this.model.attributes));
                this.model.set("enableFilter", true);
                this.model.saveLocal();
                $.mobile.changePage("History.html?setFilter=1");


            },
            clearFilter: function (e) {
                //var value = this.model.get("enableFilter");
                //this.model.set("enableFilter", !value);

                //if (this.model.get("enableFilter") == false)
                //    $.mobile.changePage("History.html");
                this.model.clearCacheModel();
                $.mobile.changePage("History.html");
            
            }
        });
    };



    var init = function ($view) {
        view = $view;
        //  populateTransferType();
        useFilter = view.find("#useFilter");
        currentViewBb = null;
        currentInputModel = null;
        createEntities();
        currentInputModel = new FilterModel();
        
        currentViewBb = new InputView({ model: currentInputModel });


    };
    return {
        initialize: init,
        setErrorDisplay: function (func) {
            showError = func;
        },
        getFilterInput: function () {
            return CustomStorage.session.get("historyFilterData", function (data) { return JSON.parse(data); });
        }
    };
})(_);

$(document).on('pageshow', '#historyFilterPage', function (e) {

    var view = $("#historyFilterPage");
    var app = jnbs.historyFilterPage;
    app.setErrorDisplay(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    app.initialize(view);


});
﻿var vcCountriesFrom;
var vcCountriesTo;
var countryFrom;
var countryTo;
var currencyFrom;
var currencyTo;
var isSend;
$(document).on('pageshow', '#calculateFees', function (e) {
     $(".tabs-menu a").click(function (event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        console.log(tab);
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
     });
     if (localStorage.OpenIdUser != null && localStorage.OpenIdUser != undefined) {
         $('#btnClose').attr('href', '../../views/Send/Send.html');
     }

     populateCountryFrom();
     populateCountryTo();

     $("#selectStatesFromSend").change(function () {
         var countryId = $(this).find("option:selected").val();
         if (countryId != -1) {
             var vcCurrencyFrom = new ViewConector({
                 container: $('#selectCurrencyFromSend'),
                 viewContentType: ViewContentType.SELECT,
                 actionName: 'CountryLookup?countryId=' + countryId + '&fakeParam=',
                 actionParameter: null,
                 typeAction: "GET",
                 configSettings: {
                     value: "CurrencyCode", text: "CurrencyName"
                 }
             });

             vcCurrencyFrom.execute();
         }
     });

     $("#selectStatesFromRece").change(function () {
         var countryId = $(this).find("option:selected").val();
         if (countryId != -1) {
             var vcCurrencyFrom = new ViewConector({
                 container: $('#selectCurrencyFromRece'),
                 viewContentType: ViewContentType.SELECT,
                 actionName: 'CountryLookup?countryId=' + countryId + '&fakeParam=',
                 actionParameter: null,
                 typeAction: "GET",
                 configSettings: {
                     value: "CurrencyCode", text: "CurrencyName"
                 }
             });

             vcCurrencyFrom.execute();
         }
     });

     $("#calculateSend").click(function () {
         countryFrom = $("#selectStatesFromSend").val();
         countryTo = $("#selectStatesToSend").val();
         currencyFrom = $("#selectCurrencyFromSend").val();
         amount = getInputAmount();
         isSend = true;
         getRecipientCurrency();
     });

     $("#amountSend").focus(function () {
         if (this.value == "0.0") this.value = "";
     });
     $("#amountSend").focusout(function () {
         if (this.value == "") this.value = "0.0";
     });

     $("#calculateReceive").click(function () {
         countryFrom = $("#selectStatesFromRece").val();
         countryTo = $("#selectStatesToRece").val();
         currencyFrom = $("#selectCurrencyFromRece").val();
         amount = getInputAmountRece();
         isSend = false;
         getRecipientCurrency();
     });

     $("#amountReceive").focus(function () {
         if (this.value == "0.0") this.value = "";
     });
     $("#amountReceive").focusout(function () {
         if (this.value == "") this.value = "0.0";
     });
});

var populateCountryFrom = function () {
    vcCountriesFrom = vcCountriesFrom || new ViewConector({
        container: $('#selectStatesFromSend'),
        viewContentType: ViewContentType.SELECT,
        actionName: 'CountryLookup?allowedCountries&searchType=0&allow=true',
        actionParameter: null,
        typeAction: "GET",
        configSettings: { value: "CountryCode", text: "CountryName" }
    });

    vcCountriesFrom.execute(
        function () {
            var vcCountriesFromRece = new ViewConector({
                container: $('#selectStatesFromRece'),
                viewContentType: ViewContentType.SELECT,
                actionName: 'CountryLookup?allowedCountries&searchType=0',
                actionParameter: null,
                typeAction: "GET",
                configSettings: {
                    value: "CountryCode", text: "CountryName"
                }
            });
            vcCountriesFromRece.response = vcCountriesFrom.response;
            vcCountriesFromRece.execute();
        }
        , function (error) {
        showError("error when try to loading country");
      
    });
    
    
};

var populateCountryTo = function () {
    vcCountriesTo = vcCountriesTo || new ViewConector({
        container: $('#selectStatesToSend'),
        viewContentType: ViewContentType.SELECT,
        actionName: 'CountryLookup?allowedCountries&searchType=1&allow=true',
        actionParameter: null,
        typeAction: "GET",
        configSettings: { value: "CountryCode", text: "CountryName" }
    });

    vcCountriesTo.execute(
        function () {
            var vcCountriesToRece = new ViewConector({
                container: $('#selectStatesToRece'),
                viewContentType: ViewContentType.SELECT,
                actionName: 'CountryLookup?allowedCountries&searchType=1',
                actionParameter: null,
                typeAction: "GET",
                configSettings: { value: "CountryCode", text: "CountryName" }
            });
            vcCountriesToRece.response = vcCountriesTo.response;
            vcCountriesToRece.execute();
        }
        , function (error) {
            showError("error when try to loading country");
        });
};

$(document).on('pageshow', '#calculateFeesResults', function (e) {
    if (localStorage.OpenIdUser != null && localStorage.OpenIdUser != undefined) {
        $('#btnCloseResults').attr('href', '../../views/Send/Send.html');
    }
     calculateFees();
});



var getRecipientCurrency = function () {
    Helpers.progressBox.start("searching");
    ServiceConnection.getData({
        actionName: 'SystemConfiguration?countryId=' + countryTo,
        formData: null,
        successCallback: function (response, status, xhr) {
            currencyTo = response.CurrencyCode;
            $.mobile.changePage("calculateFeesResults.html");
            Helpers.progressBox.stop();
        },
        errorCallback: function (dataError) {
            Helpers.progressBox.stop();
        },
        typeAction: "GET"

    });
};

var getInputAmount = function () {
  
    var value = $("#amountSend").val();

    if (Number(value) != undefined) {
        return Number(value);
    }
    return Number(0);
};

var getInputAmountRece = function () {

    var value = $("#amountReceive").val();

    if (Number(value) != undefined) {
        return Number(value);
    }
    return Number(0);
};

var cvResponse = function (response) {
    if (isSend) {
        response.TransferFee = Helpers.maskMoney(response.CurrentTransferFee) + " " + currencyFrom;
        response.AmountTransfer = Helpers.maskMoney(amount) + " " + currencyFrom;
        response.ExchangeRate = "1 " + currencyFrom + " = " + response.CurrentExchangeRate + " " + currencyTo;
        response.Total = Helpers.maskMoney(response.CurrentTransferFee + amount) + " " + currencyFrom;
        response.Receive = Helpers.maskMoney(response.CurrentExchangeRate * amount) + " " + currencyTo;
        return response;
    } else {
        var amountToSend = amount / response.CurrentExchangeRate;
        response.TransferFee = Helpers.maskMoney(response.CurrentTransferFee) + " " + currencyFrom;
        response.AmountTransfer = Helpers.maskMoney(amountToSend) + " " + currencyFrom;
        response.ExchangeRate = "1 " + currencyFrom + " = " + response.CurrentExchangeRate + " " + currencyTo;
        response.Total = Helpers.maskMoney(response.CurrentTransferFee + amountToSend) + " " + currencyFrom;
        response.Receive = Helpers.maskMoney(amount) + " " + currencyTo;
        return response;
    }
}
          

var calculateFees = function () {
    var url = "SystemConfiguration?";
    var paramService = {
        countryId: countryFrom,
        regionId: "",
        transferCurrencyId: currencyFrom,
        transferAmount: amount,
        disbursementCountryId: countryTo,//localStorage.RecipientCountry,
        disbursementCurrency: currencyTo,//
        transfertype: "P2P"  //ToCard
    };

    var urlService = url + $.param(paramService);
    var exchangeConector = new ViewConector({
        actionName: urlService,
        actionParameter: null,
        viewContentType: ViewContentType.FORM,
        container: $('#exchangeData'),
        typeAction: 'GET',
        messageLoading: "calculating exchange...",
        convertResponse:cvResponse,
    });

    exchangeConector.execute(function (response) {
        if (response.TransferCurrencyFeeRateId === 0 && response.CurrentTransferFee === 0 && response.FeeCurrencyId === null) {
            var view = $("#calculateFeesResults");
            view.find("#formSubmit").css("display", "none");
            view.find("#exchangeData").css("display", "none");
            ErrorBox.show(view.find("#errorContent"), "Sorry. This transaction is currently not supported by JN Money Transfer.");
            
            //ErrorBox.show($("#calculateFeesResults").find("#errorInPopup"), "This transaction is currently not supported by JN Money Transfer.");
        }
        else {
            var view = $("#calculateFeesResults");
            view.find("#formSubmit").css("display", "block");
            view.find("#exchangeData").css("display", "block");
        }
    }, function () {
        showError("error try to calculating exchange data");
    });
}
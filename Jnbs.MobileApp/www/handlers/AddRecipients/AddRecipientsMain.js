﻿jnbs.addRecipients = (function () {

    var view, getDataRecollected, showError;
    var addData = function (data) {
        var recipientData = {};
        if (sessionStorage.recipientData == undefined)
            sessionStorage.recipientData = JSON.stringify(recipientData);
        else
            recipientData = JSON.parse(sessionStorage.recipientData);
     
        _.each(_.keys(data), function (key) {
            var value = data[key];
            recipientData[key] = value;
        });
        sessionStorage.recipientData = JSON.stringify(recipientData);
       // localStorage.recipientData[key] = value;
    };

    var init = function ($view) {
        view = $view;
    };

    var setHandlers = function () {
        view.find("#add-person").click(function () {
            localStorage.AddRecipientBackTo = "AddRecipientsMainMenu.html";
            $.mobile.changePage("AddRecipientPersonStep1.html");
        });
        view.find("#add-card").click(function () {
            localStorage.AddRecipientBackTo = "AddRecipientsMainMenu.html";
            $.mobile.changePage("AddRecipientCardStep1.html");
        });
        view.find("#add-bill").click(function () {
            localStorage.AddRecipientBackTo = "AddRecipientsMainMenu.html";
            $.mobile.changePage("addBill/billRecolectDataStep1View.html");
        });
        view.find("#add-account").click(function () {
            localStorage.AddRecipientBackTo = "AddRecipientsMainMenu.html";
            $.mobile.changePage("addAccount/addAccountDataStep1View.html");
        });
        
    };

    getDataRecollected=function(){
        return JSON.parse(sessionStorage.recipientData);

    };
    var saveRecipient = function (callb,dataToPost) {
        var data = null;
        if (dataToPost)
            data = dataToPost;
        else  data = getDataRecollected();
       

        var jsonParam1 = {
            "Person": {
                "OpenId": null,
                "PersonId": null,
                "Title": data.title,
                "FirstName": data.firstName,
                "MiddleName": data.middleName,
                "LastName": data.lastName,
                "EmailAddress": data.email,
                "ContactTelephone1": data.telephone1,
                "ContactTelephone2": data.telephone2 | "",
                "MobileTelephone": null,
                "Dob": null,
                "PlaceOfBirth": null,
                "CustomerCategory": 0,
                "Address": {
                    "Address1": data.street,
                    "Address2": data.cityTown,
                    "Address3": data.countryState,
                    "Address4": data.postalCode,
                    "AddressCountry": data.country
                },
                "SourceOfFunds": null,
                "Nationality": null,
                "Occupation": null,
                "Ssn": null,
                "SsntrnRequired": false,
                "Identification": {
                    "IdentificationType": 0,
                    "IdentificationNumber": null,
                    "IdentificationIssuingCountry": null,
                    "IdentificationIssueDate": "\/Date(-62135578800000)\/",
                    "IdentificationExpirationDate": "\/Date(-62135578800000)\/",
                    "AdditionalIdentificationNumber": null
                },
                "AddressVerificationDate": null,
                "IdVerificationDate": null,
                "AggregatedRiskType": null,
                "MTCardNumber": (data.MTCardNumber?data.MTCardNumber:null),
                "NatureAndPurposeOfTransfers": null,
                "ExpectedTransactionFrequency": null,
                "Status": null,
                "HasCreditCard": false
            },
            "SenderMtsd": localStorage.MtsId
        };
        var urlService = "previousrecipients";


        ServiceConnection.getData({
            actionName: urlService,
            formData: JSON.stringify(jsonParam1),
             typeAction: "POST",
            headerContentType: "application/json",
            successCallback: function (response, status, xhr) {
                callb();

            },
            errorCallback: function (dataError) {
                if (dataToPost) {
                    
                }
                showError("has error when try save recipients");
            }
        });
    
    };

    return {
        addData: addData,
        init: init,
        setHandlers: setHandlers,
        clearData: function () {
            if (sessionStorage.recipientData)
                delete sessionStorage.recipientData;
        },
        saveRecipient: saveRecipient,
        setDisplayError:function(showErr) {
            showError = showErr;
        }
    };

})();

//AddRecipientsFormStep1.html
$(document).on('pageshow', '#addRecipients', function (e) {

    var view = $("#addRecipients");

    var app = jnbs.addRecipients;
    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    app.init(view);
    app.clearData();

    app.setHandlers();
});
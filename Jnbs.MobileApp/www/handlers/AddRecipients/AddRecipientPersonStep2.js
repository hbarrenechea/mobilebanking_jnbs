﻿var vcCountries;
jnbs.addRecipients2 = (function () {
    var showError,recipeModule, view, context, saveCurrentData,
        setupMVObjects, RecipientModel, formSubmit;

    var setupValidation = function () {
        formSubmit = view.find("#formSubmit");

        formSubmit.validate({
            rules: {
                selectCountry: "required",
                selectStates: "required",
                cityTown: "required",
                street: "required"
            },
            messages: {
                cityTown: "City Town Name is required",
                street: "Street is required"
            },

            errorElement: "em",
            errorPlacement: function (error, element) {

                error.addClass("error-form");

                element.parent().parent().append(error);
            },

            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
            }
        });
    };
    //----backbone
     setupMVObjects = function () {
          RecipientModel = Backbone.Model.extend({
             defaults:{
                 country:"",countryState:"",cityTown:"",street:""
             },
             validate: function (attrs) {
                 if (!attrs.country)
                     return "Country is mandatory";
                 if(!attrs.countryState || attrs.countryState=="-1")
                     return "state is mandatory";
                 if (!attrs.cityTown)
                     return "cityTown is mandatory";
                 if (!attrs.street)
                     return "street is mandatory";
               

             }
         });
     };
    //-----
      saveCurrentData = function () {
       
         var model = new RecipientModel();
         model.set({
             country: view.find("#selectCountry option:selected").val(),
             countryName:view.find("#selectCountry option:selected").text(),
             countryState: view.find("#selectStates option:selected").text(),
             cityTown: view.find("#cityTown").val(),
             street: view.find("#street").val(),
             postalCode:view.find("#postalCode").val()
         });

         return model;
     };
    var populateCountry = function () {
        vcCountries = vcCountries || new ViewConector({
            container: view.find('#selectCountry'),
            viewContentType: ViewContentType.SELECT,
            actionName: 'CountryLookup',
            actionParameter: null,
            typeAction: "GET",
            configSettings: { value: "CountryCode", text: "CountryName" }
        });

        vcCountries.execute(null, function (error) {
            showError("error when try to loading country");
        });
    };
    var populateStateByCountry = function (countryId) {
        if (countryId == "-1") return;
        var p1 = $.param({ countryId: countryId });
        var urlBase = "CountryLookup";
        var urlService = urlBase + "?" + p1;
        // <option value="-1" disabled selected>Select</option>
        var cb = view.find('#selectStates');
        cb.empty();
        cb.append('<option value="-1" disabled selected>Select</option>');
        var vc = new ViewConector({
            container: view.find('#selectStates'),
            viewContentType: ViewContentType.SELECT,
            actionName: urlService,
            actionParameter: null,
            typeAction: "GET",
            configSettings: { value: "StateId", text: "StateName" }
        });

        vc.execute(function (resp) {
            if (sessionStorage.personStates != undefined) {
            $("#addRecipientStep2 #selectStates").val(sessionStorage.personStates);
            $("#addRecipientStep2 #selectStates").selectmenu("refresh");
            $("#addRecipientStep2 #selectStates").change();
            }

            if (resp && Array.isArray(resp) && resp.length == 0) {
                showError("The selected country not associated states");
            }
        }, function (error) {
            showError("error when try to loading states");
        });
    };

    var init = function ($view, $recipeModule) {
        view = $view;
        recipeModule = $recipeModule;
        setupValidation();
        setupMVObjects();
    }
    var render = function () {
        populateCountry();
    };

    var setHandlers = function () {
        view.find("#selectCountry").change(function () {
            var countryId = view.find("#selectCountry option:selected").val();
            populateStateByCountry(countryId);
        });

        view.find("#nextView3").click(function () {
            //AddRecipientsFormStep3.html
            if (formSubmit.valid() == false) return;
            var modelInput = saveCurrentData();
            if (modelInput.isValid()) {
                recipeModule.addData(modelInput.attributes);

                sessionStorage.personCityTown = $('#addRecipientStep2 #cityTown').val();
                sessionStorage.personStreet = $('#addRecipientStep2 #street').val();
                sessionStorage.personMiddleName = $('#addRecipientStep2 #postalCode').val();
                sessionStorage.personCountry = $("#addRecipientStep2 #selectCountry").val();
                sessionStorage.personStates = $("#addRecipientStep2 #selectStates").val();

                $.mobile.changePage("AddRecipientPersonStep3.html");
            }
            else {
                showError(modelInput.validationError);
            }
        });
        view.find("#CloseAddRecipientsStep2").click(function () {
            $.mobile.changePage(localStorage.AddRecipientBackTo);
        });
    };
    return {
        init: init,
        render: render,
        setHandlers: setHandlers,
        setDisplayError: function (showErr) {
            showError = showErr;
        }
    };

})();


$(document).on('pageshow', '#addRecipientStep2', function (e) {

    var view = $("#addRecipientStep2");

    var app = jnbs.addRecipients2;
    app.init(view, jnbs.addRecipients, {});
    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    app.setHandlers();
    app.render();

    if (sessionStorage.personCityTown != undefined) {

        $('#addRecipientStep2 #cityTown').val(sessionStorage.personCityTown);
        $('#addRecipientStep2 #street').val(sessionStorage.personStreet);
        $('#addRecipientStep2 #postalCode').val(sessionStorage.personMiddleName);

        $("#addRecipientStep2 #selectCountry").val(sessionStorage.personCountry);
        $("#addRecipientStep2 #selectCountry").selectmenu("refresh");
        $("#addRecipientStep2 #selectCountry").change();        

        

        //$("#addRecipientStep2 #selectStates").val(sessionStorage.personStates);
        //$("#addRecipientStep2 #selectStates").selectmenu("refresh");
        //$("#addRecipientStep2 #selectStates").change();
        
    }
});
﻿jnbs.billPayment.saveData = (function () {
    var showError;
    var addBill = function(data,callSuccess,callError) {
        var modelObject = {
            "Account": {
                "MTSId": null,
                "AccountNumber": data.accountNumber,
                "AccountType": "UTL",
                "PrimAccountHolderFirstName": data.accountHolder.firstName,
                "PrimAccountHolderSurname": data.accountHolder.lastName,
                "Email": null,
                "SecAccountHolderFirstName": null,
                "SecAccountHolderSurname": null,
                "AccountCurrency": data.accountCurrency,
                "AccountInstitution": data.idInstitution,
                "Gender": null,
                "MobileNumber": null,
                "WorkNumber":null ,
                "DOB": null,
                "OldAccountNumber": null,
                "OtherInstitutionName": null,
                "InstitutionBranchId": null,
                "TelephoneNumber": data.accountHolder.telephone,
                "Address": {
                    "Address1": data.accountHolder.address || "-",
                    "Address2": data.accountHolder.cityOrTown || "-",
                    "Address3": data.accountHolder.stateId || "-",
                    "Address4": data.accountHolder.postalCode || -"",
                    "AddressCountry": (data.accountHolder.countryId.toUpperCase()=="AG"?"JM":data.accountHolder.countryId)
                }
            },
           
            "SenderMTSId": localStorage.MtsId,
            "RecipientMTSId": null
        };

        var urlService = "PreviousRecipientAccounts";


        ServiceConnection.getData({
            actionName: urlService,
            formData: JSON.stringify(modelObject),
            typeAction: "POST",
            headerContentType: "application/json",
            successCallback: function (response, status, xhr) {
                callSuccess(response);

            },
            errorCallback: function (dataError) {

                callError(dataError);
                //  showError("has error when try save Bill");
            }
        });
    };
    return {
        
        post: addBill
      
    }
})();

////AddRecipientsFormStep1.html
//$(document).on('pageshow', '#addRecipients', function (e) {

//    var view = $("#addRecipients");

//    var app = jnbs.billPayment.saveData;
//    //app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
//    //app.init(view);
//    //app.clearData();

//    //app.setHandlers();
//});
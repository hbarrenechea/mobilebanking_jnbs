﻿jnbs.billPayment.recolectData = (function () {
    var view, saveData, showError, pageStep1, pageStep2, pageStep3;
    saveData = function (id, data) {
        CustomStorage.session.add(id, JSON.stringify(data));
    };

    pageStep1 = {
        formSubmit: null,
        vcInstitution: null,
        cbInstitution: null,
        setupValidation: function () {

            this.formSubmit.validate({
                rules: {
                    cbInstitution: "required",
                    accountNumber: "required",

                },
                messages: {
                    cbInstitution: "Institution is required",
                    accountNumber: "Account number is required",

                },

                errorElement: "em",
                errorPlacement: function (error, element) {

                    error.addClass("error-form");

                    element.parent().parent().append(error);
                },

                highlight: function (element, errorClass, validClass) {
                    $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
                }
            });
        },
        setHandlers: function () {
            var self = this;
            view.find("#btnNext1").click(function () {

                if (self.formSubmit.valid() == false) return;

                var idInstitution = self.cbInstitution.find("option:selected").val();
                var institutionAccountNumber = view.find("#accountNumber").val();
                var data = {
                    idInstitution: idInstitution,
                    accountNumber: institutionAccountNumber
                };
                sessionStorage.billAccountNumber = data.accountNumber;
                sessionStorage.billCbInstitution = data.idInstitution;
                saveData("addBill1", data);
                $.mobile.changePage("billRecolectDataStep2View.html");
            });
            view.find("#billRecolectDataCloseStep1").click(function () {
                $.mobile.changePage(localStorage.AddRecipientBackTo);
            });
        },
        populate: function () {
            var paramGet = {
                institutionTypeId: Enumerations.getInstitutionTypeValueByName("utility"),
                countryId: (localStorage.CountryCode.toUpperCase() == "AR" ? "JM" : localStorage.CountryCode)
            };
            var vcInstitution = new ViewConector({
                container: view.find('#cbInstitution'),
                viewContentType: ViewContentType.SELECT,
                actionName: 'SystemConfiguration?' + $.param(paramGet),
                actionParameter: null,
                typeAction: "GET",
                configSettings: { value: "InstitutionId", text: "InstitutionName" }
            });

            vcInstitution.execute(function (response) {
                if (sessionStorage.billCbInstitution != undefined) {
                    $("#billRecolectDataStep1View #cbInstitution").val(sessionStorage.billCbInstitution);
                    $("#billRecolectDataStep1View #cbInstitution").selectmenu("refresh");
                    $("#billRecolectDataStep1View #cbInstitution").change();
                }
                console.log(response);
            }, function (error) {
                showError("error when try to loading institutions");
            });
        },
        init: function ($view) {
            view = $view;
            this.formSubmit = view.find("#formSubmit");
            this.cbInstitution = view.find("#cbInstitution");
            this.populate();
            this.setupValidation();
        }
    };

    pageStep2 = {
        formSubmit: null,
        setupValidation: function () {

            this.formSubmit.validate({
                rules: {
                    telephone: "required",
                    firstName: "required",
                    lastName: "required"
                },
                messages: {
                    telephone: "telephone is required",
                    firstName: "First Name is required",
                    lastName: "Last name is required"

                },

                errorElement: "em",
                errorPlacement: function (error, element) {

                    error.addClass("error-form");

                    element.parent().parent().append(error);
                },

                highlight: function (element, errorClass, validClass) {
                    $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
                }
            });
        },
        init: function ($view) {
            view = $view;
            var self = this;
            this.formSubmit = view.find("#formSubmit");
            this.setupValidation();
            view.find("#btnNext2").click(function () {
                if (self.formSubmit.valid() == false) return;

                var firstName = view.find("#firstName").val();
                var lastName = view.find("#lastName").val();
                var telephone = view.find("#telephone").val();
                saveData("addBill2", {
                    firstName: firstName,
                    lastName: lastName,
                    telephone: telephone
                });

                sessionStorage.billFirstName = $('#billRecolectDataStep2View #firstName').val();
                sessionStorage.billLastName = $('#billRecolectDataStep2View #lastName').val();
                sessionStorage.billTelephone = $('#billRecolectDataStep2View #telephone').val();

                $.mobile.changePage("billRecolectDataStep3View.html");
            });

        }
    };

    pageStep3 = {
        formSubmit: null,
        setupValidation: function () {

            this.formSubmit.validate({
                rules: {
                    address: "required",
                    cityOrTown: "required",
                    selectCountry: "required"
                },
                messages: {
                    address: "address is required",
                    cityOrTown: "city Name is required",
                    selectCountry: "country is required"

                },

                errorElement: "em",
                errorPlacement: function (error, element) {

                    error.addClass("error-form");

                    element.parent().parent().append(error);
                },

                highlight: function (element, errorClass, validClass) {
                    $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
                }
            });
        },
        populateCountry: function () {
            var vcCountries = new ViewConector({
                container: view.find('#selectCountry'),
                viewContentType: ViewContentType.SELECT,
                actionName: 'CountryLookup',
                actionParameter: null,
                typeAction: "GET",
                configSettings: { value: "CountryCode", text: "CountryName" }
            });

            vcCountries.execute(null, function (error) {
                showError("error when try to loading country");
            });
        },
        populateStateByCountry: function (countryId) {
            if (countryId == "-1") return;
            var p1 = $.param({ countryId: countryId });
            var urlBase = "CountryLookup";
            var urlService = urlBase + "?" + p1;
            // <option value="-1" disabled selected>Select</option>
            var cb = view.find('#selectStates');
            cb.empty();
            cb.append('<option value="-1" disabled selected>Select</option>');
            var vc = new ViewConector({
                container: view.find('#selectStates'),
                viewContentType: ViewContentType.SELECT,
                actionName: urlService,
                actionParameter: null,
                typeAction: "GET",
                configSettings: { value: "StateId", text: "StateName" }
            });

            vc.execute(function (resp) {
                if (sessionStorage.billSelectStates != undefined) {
                    $("#billRecolectDataStep3View #selectStates").val(sessionStorage.billSelectStates);
                    $("#billRecolectDataStep3View #selectStates").selectmenu("refresh");
                    $("#billRecolectDataStep3View #selectStates").change();
                }
                if (resp && Array.isArray(resp) && resp.length == 0) {
                    showError("The selected country not associated states");
                }
            }, function (error) {
                showError("error when try to loading states");
            });
        },
        init: function ($view, saveDataModule) {
            view = $view;
            var self = this;
            this.formSubmit = view.find("#formSubmit");
            self.populateCountry();
            view.find("#selectCountry").change(function () {
                var countryId = view.find("#selectCountry option:selected").val();
                self.populateStateByCountry(countryId);
            });

            view.find("#btnPushData").click(function () {

                if (self.formSubmit.valid() == false) return;

                sessionStorage.billAddress = $('#billRecolectDataStep3View #address').val();
                sessionStorage.billCityOrTown = $('#billRecolectDataStep3View #cityOrTown').val();
                sessionStorage.billPostalCode = $('#billRecolectDataStep3View #postalCode').val();
                sessionStorage.billSelectCountry = $("#billRecolectDataStep3View #selectCountry").val();
                sessionStorage.billSelectStates = $("#billRecolectDataStep3View #selectStates").val();
               

                var data1 = CustomStorage.session.get("addBill1", function (d) {
                    return JSON.parse(d);
                });

                var data2 = CustomStorage.session.get("addBill2", function (d) {
                    return JSON.parse(d);
                });

                var data3 = {
                    countryId: view.find("#selectCountry option:selected").val(),
                    stateId: view.find("#selectStates option:selected").val(),
                    address: view.find("#address").val(),
                    cityOrTown: view.find("#cityOrTown").val(),
                    postalCode: view.find("#postalCode").val()

                };

                var accountHolder = {};
                _.extend(accountHolder, data2);
                _.extend(accountHolder, data3);
                var dataFinal = data1;
                dataFinal.accountCurrency = "JMD";
                dataFinal.accountHolder = accountHolder;
                Helpers.progressBox.start("processing");
                saveDataModule.post(dataFinal, function (response) {
                    Helpers.progressBox.stop();

                    if (response.Success == true) {

                        $.mobile.changePage("../AddRecipientsMainMenu.html");
                    }
                    else {
                        showError(response.Message || "Error");
                    }
                }, function (err) {
                    showError("has error when try save a new Bill institution");
                    console.log("Error saving new Bill: " + JSON.stringify(err));
                    Helpers.progressBox.stop();

                });


            });
            this.setupValidation();

        }
    };

    console.log("Add Bill->Binding");
    _.bindAll(pageStep1, "init", "populate", "setHandlers", "setupValidation");
    _.bindAll(pageStep2, "init", "setupValidation");
    _.bindAll(pageStep3, "init", "populateCountry", "populateStateByCountry", "setupValidation");
    console.log("Add Bill->End binding");



    return {
        pageStep1: pageStep1,
        pageStep2: pageStep2,
        pageStep3: pageStep3,
        setDisplayError: function (showErr) {
            showError = showErr;
        },
        reset:function() {
            view = null;
            showError = null;
        }
    };


})();



$(document).on('pageshow', '#billRecolectDataStep1View', function (e) {

    var view = $("#billRecolectDataStep1View");

    var appPage1 = jnbs.billPayment.recolectData.pageStep1;
    var app = jnbs.billPayment.recolectData;
    app.reset();
    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    appPage1.init(view);
    appPage1.setHandlers();

    if (sessionStorage.billAccountNumber != undefined) {
        $('#billRecolectDataStep1View #accountNumber').val(sessionStorage.billAccountNumber);
       
        //$("#billRecolectDataStep1View #cbInstitution").val(sessionStorage.billCbInstitution);
        //$("#billRecolectDataStep1View #cbInstitution").selectmenu("refresh");
        //$("#billRecolectDataStep1View #cbInstitution").change();
    }
});



$(document).on('pageshow', '#billRecolectDataStep2View', function (e) {

    var view = $("#billRecolectDataStep2View");
    view.find("#billRecolectDataCloseStep2").click(function () {
        $.mobile.changePage(localStorage.AddRecipientBackTo);
    });
    var appPage2 = jnbs.billPayment.recolectData.pageStep2;
    var app = jnbs.billPayment.recolectData;
    app.reset();

    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    appPage2.init(view);
    // appPage2.setHandlers();

    if (sessionStorage.billAccountNumber != undefined) {
        $('#billRecolectDataStep2View #firstName').val(sessionStorage.billFirstName);
        $('#billRecolectDataStep2View #lastName').val(sessionStorage.billLastName);
        $('#billRecolectDataStep2View #telephone').val(sessionStorage.billTelephone);
       
    }
});

$(document).on('pageshow', '#billRecolectDataStep3View', function (e) {

    var view = $("#billRecolectDataStep3View");
    view.find("#billRecolectDataCloseStep3").click(function () {
        $.mobile.changePage(localStorage.AddRecipientBackTo);
    });
    var appPage3 = jnbs.billPayment.recolectData.pageStep3;
    var app = jnbs.billPayment.recolectData;
    app.reset();

    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    appPage3.init(view, jnbs.billPayment.saveData);
    // appPage2.setHandlers();

    if (sessionStorage.billAccountNumber != undefined) {
        $('#billRecolectDataStep3View #address').val(sessionStorage.billAddress);
        $('#billRecolectDataStep3View #cityOrTown').val(sessionStorage.billCityOrTown); 
        $('#billRecolectDataStep3View #postalCode').val(sessionStorage.billPostalCode);

        $("#billRecolectDataStep3View #selectCountry").val(sessionStorage.billSelectCountry);
        $("#billRecolectDataStep3View #selectCountry").selectmenu("refresh");
        $("#billRecolectDataStep3View #selectCountry").change();
        
        
    }

});



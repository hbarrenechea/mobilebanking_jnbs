﻿jnbs.addRecipients3 = (function () {
    var showError, recipeModule, view, context, setupMvObjects,
        RecipientModel, formSubmit,
        saveCurrentData;

    setupMvObjects = function () {
        RecipientModel = Backbone.Model.extend({
            defaults: {
                telephone1:"",
                telephone2:"",
                email:""
            },

            validate: function (attrs) {
                if (!attrs.telephone1)
                    return "Telephone is mandatory";
                if (!attrs.email)
                    return "email is required";
                if (!Helpers.isEmailFormatValid(attrs.email))
                    return "email format is not valid";
            }
        });
    };
    var setupValidation = function () {
        formSubmit = view.find("#formSubmit");

        formSubmit.validate({
            rules: {
                telephone1: "required",
                email: { 
                    required:true,
                    email:true 
                }
            },
            messages: {
                telephone1: "City Town Name is required",
                email: {
                    required: "email is required",
                    email: "email is not valid"
                }
            },

            errorElement: "em",
            errorPlacement: function (error, element) {

                error.addClass("error-form");

                element.parent().parent().append(error);
            },

            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
            }
        });
    };

    saveCurrentData = function () {

        var model = new RecipientModel();
        model.set({
            telephone1: view.find("#telephone1").val(),
            telephone2: view.find("#telephone2").val(),
            email: view.find("#email").val(),
          
        });

        sessionStorage.personTelephone1 = view.find("#telephone1").val();
        sessionStorage.personTelephone2 = view.find("#telephone2").val();
        sessionStorage.personEmail = view.find("#email").val();

        return model;
    };


    var init = function ($view,$recipeModule, $context) {
        view = $view;
        context = $context;
        recipeModule = $recipeModule;
        setupMvObjects();
        setupValidation();
    };
    var render = function () {

    };

    var setHandlers = function () {
        view.find("#list-recipients").click(function () {

            if (formSubmit.valid() == false) return;

            var url = "../Transfer/TransferSendTo.html";
            var modelInput = saveCurrentData();
            if (modelInput.isValid()) {
                recipeModule.addData(modelInput.attributes);
                recipeModule.saveRecipient(function () {
                    handlerEventsBuss.trigger(handlerEventsBuss.RECIPIENT_CHANGE);
                    $.mobile.changePage(url);
                });
                
            }
            else {
                showError(modelInput.validationError);
            }
        });
        view.find("#CloseAddRecipientsStep3").click(function () {
            $.mobile.changePage(localStorage.AddRecipientBackTo);
        });
    };
    return {
        render: render,
        init: init,
        setHandlers: setHandlers,
        setDisplayError: function (showErr) {
            showError = showErr;
        }
    };
})();


$(document).on('pageshow', '#addRecipientStep3', function (e) {

    var view = $("#addRecipientStep3");

    var app = jnbs.addRecipients3;
    app.init(view, jnbs.addRecipients, {});
    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));

    app.setHandlers();
    app.render();

    if (sessionStorage.personTelephone1 != undefined) {
        $('#addRecipientStep3 #telephone1').val(sessionStorage.personTelephone1);
        $('#addRecipientStep3 #telephone2').val(sessionStorage.personTelephone2);
        $('#addRecipientStep3 #email').val(sessionStorage.personEmail);
       
    }
});
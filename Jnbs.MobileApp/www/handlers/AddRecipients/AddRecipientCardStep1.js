﻿jnbs.MTCardRecipient = (function () {
    var initialize, render, showError, SearchModel,
        PopupView, moduleRecipient,
        PopupDataModel, view, formSubmit;
    var redirectListRecipients = function (data) {
        sessionStorage.selectRecipientCard = JSON.stringify(data);
        $.mobile.changePage("../Transfer/toCard/ConfigureAmount.html?redirectFrom=search");
    }

    var setupValidation = function () {
        formSubmit = view.find("#formSubmit");

        formSubmit.validate({
            rules: {
                firstName: "required",
                lastName: "required",
                cardNumber: "required",
            },
            messages: {
                firstName: "first Name is required",
                lastName: "Last Name is required",
                cardNumber: "Card Number is required"
            },

            errorElement: "em",
            errorPlacement: function (error, element) {

                error.addClass("error-form");

                element.parent().parent().append(error);
            },

            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
            }
        });
    };

    var setupObjects = function () {
        PopupDataModel = Backbone.Model.extend({
            defaults: {
                nameRecipient: "jhon",
                countryName: "argentina",
                cardNumberMasked: "****-****-****-1234",
                phoneRecipient: "3111144444",
                address: {
                    address1: "dir1", address2: "dir2", address3: "dir3", address4: "dir4"
                }
            }

        });

        PopupView = Backbone.View.extend({
            template: Handlebars.compile(view.find("#popup-template").html()),
            el: view.find("#popupMTcard"),
            events: {
                "click #addRecipientCard": "addCard"
            },
            addCard: function (e) {
                //alert("card added");
                var data = this.model.attributes.rawRecipient;
                redirectListRecipients(data);
                //  moduleRecipient.saveRecipient(redirectListRecipients, data);
            },
            render: function () {
                var data = this.template(this.model.attributes);
                this.$el.html(data);
            },
            show: function () {
                this.$el.popup("open");
            },
            close: function () {
                this.$el.popup("close");
            }
        });
        SearchModel = Backbone.Model.extend({
            defaults: {
                firstName: "",
                lastName: "",
                cardNumber: 0
            },
            cardNumberMaxLength: 7,
            validate: function (attrs) {

                if (!attrs.firstName)
                    return "First Name is mandatory";
                if (!attrs.lastName)
                    return "Last Name is mandatory";
                if (!attrs.cardNumber)
                    return "Card Number is mandatory";
                if (attrs.cardNumber.toString().length != this.cardNumberMaxLength)
                    return "Card Number must be last " + this.cardNumberMaxLength + " digits";

            }
        },
            { //static
                createrFromInput: function () {
                    return new SearchModel({
                        firstName: view.find("#firstName").val(),
                        lastName: view.find("#lastName").val(),
                        cardNumber: view.find("#cardNumber").val()
                    });
                }
            }
        );
    };
    var doSearch = function (inputData, successAction, errorAction) {
        var callService = function () {
            //var url = "http://qa.mcsystems.com:4443/Jnms.Services.App.Out/api/previousrecipients?";
            var url = "previousrecipients?";

            var urlService = url +
                $.param({
                    //  mtsId: localStorage.MtsId,
                    firstName: inputData.firstName,
                    lastName: inputData.lastName,
                    mtCardNumber: inputData.cardNumber
                });

            Helpers.progressBox.start("searching");
            ServiceConnection.getData({
                actionName: urlService,
                formData: null,
                typeAction: "GET",
                successCallback: function (response, status, xhr) {
                    //successAction(response);
                    //nameRecipient: "jhon",
                    //countryName: "argentina",
                    //cardNumberMasked: "****-****-****-1234",
                    //phoneRecipient: "3111144444",
                    //address: {
                    //    address1: "dir1", address2: "dir2", address3: "dir3", address4: "dir4"
                    //}
                    var lst = [response];
                    var result = lst.map(function (item) {

                        //for ramiro.
                        if (!item.MTCardNumber) item.MTCardNumber = inputData.cardNumber;
                        item.getMaskCardNumber = function () {
                            var cn = this.MTCardNumber;
                            var last4 = cn.substring(cn.length - 4);
                            return "****-****-****-" + last4;

                        };
                        item.getAddresses = function () {
                            var self = this;
                            return {
                                address1: self.Address.Address1 || "-",
                                address2: self.Address.Address2 || "-",
                                address3: self.Address.Address3 || "-",
                                address4: self.Address.Address4 || "-",
                                addressCountry: self.Address.AddressCountry || "-"
                            };
                        };
                        //var self = item;
                        return {
                            nameRecipient: item.FirstName + " " + item.LastName,
                            countryName: item.PlaceOfBirth,
                            cardNumberMasked: item.getMaskCardNumber(),
                            phoneRecipient: item.ContactTelephone1,
                            address: item.getAddresses(),
                            //dataForSave: {
                            //    title: self.Title,
                            //    firstName: self.FirstName,
                            //    middleName: self.MiddleName,
                            //    lastName: self.LastName,
                            //    email: self.EmailAddress||"-",
                            //    telephone1: self.ContactTelephone1,
                            //    telephone2: self.ContactTelephone2 | "",
                            //    street: self.Address.Address1,
                            //    cityTown: self.Address.Address2,
                            //    countryState: self.Address.Address3,
                            //    postalCode: self.Address.Address4,
                            //    country: self.Address.AddressCountry,
                            //    MTCardNumber: self.MTCardNumber
                            //},
                            rawRecipient: item
                        };

                    });
                    if (result && result.length > 0) {
                        successAction(result[0]);
                    }
                    console.log(JSON.stringify(response));
                    Helpers.progressBox.stop();
                },
                errorCallback: function (dataError) {
                    Helpers.progressBox.stop();
                    errorAction(dataError);
                    //showError("there was an error when attempting to load historical");
                }
            });
        };

        callService();

        //var result = new PopupDataModel({
        //    nameRecipient: "claudio",
        //    countryName: "argentina",
        //    cardNumberMasked: "****-****-****-1234",
        //    phoneRecipient: "3111144444",
        //    address: {
        //        address1: "dir1", address2: "dir2", address3: "dir3", address4: "dir4"
        //    }
        //});

        //successAction(result);
    };
    var showPopup = function (data) {
        var popupView = new PopupView({ model: new PopupDataModel(data) });
        popupView.render();
        popupView.show();
    };
    var resultNotFound = function (inputData) {
        showError("Recipient not found");
    };
    initialize = function ($view, $modRecipient) {
        view = $view;
        moduleRecipient = $modRecipient;
        setupObjects();
        setupValidation();
    }
    render = function () {

    };
    var setHandlers = function () {
        view.find("#btnSearch").click(function () {

            if (formSubmit.valid() == false) return;
            //llamar a servicio de busqueda
            //si hay resultados mostrar popup.
            //si no hay resultados??
            var model = SearchModel.createrFromInput();
            if (model.isValid()) {
                doSearch(model.attributes, showPopup, resultNotFound);
            } else {
                console.log(model.validationError);
                showError(model.validationError);
            }

        });
        view.find("#CloseAddRecipientCardStep1").click(function () {
            $.mobile.changePage(localStorage.AddRecipientBackTo);
        });
    };
    return {
        initialize: initialize,
        render: render,
        setHandlers: setHandlers,
        setDisplayError: function (showErr) {
            showError = showErr;
        }
    };
})();


$(document).on('pageshow', '#addMtCard', function (e) {
    var view = $('#addMtCard');

    var app = jnbs.MTCardRecipient;
    app.initialize(view, jnbs.addRecipients);
    app.setHandlers();
    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));

    app.render();
});
﻿jnbs.addRecipients1 = (function () {
    var showError, view, context, recipeModule, RecipientModel,
        firstName, lastName, middleName, formSubmit;

    var saveCurrentData = function () {

        var model = new RecipientModel();
        model.set({
            firstName: firstName.val(),
            lastName: lastName.val(),
            middleName: middleName.val(),
            title: view.find("#selectTitle option:selected").val()
        });

        return model;
    };

    var setHandlers = function () {
        view.find("#nextStep2").click(function () {

            if (formSubmit.valid() == false) return;

            var modelInput = saveCurrentData();
            if (modelInput.isValid()) {
                sessionStorage.personFirstName = $('#firstName').val();
                sessionStorage.personLastName = $('#lastName').val();
                sessionStorage.personMiddleName = $('#middleName').val();
                sessionStorage.personTitle = $("#selectTitle option:selected").val();
                
                recipeModule.addData(modelInput.attributes);
                $.mobile.changePage("AddRecipientPersonStep2.html");
            }
            else {
                console.log(modelInput.validationError);
            }
        });
        view.find("#CloseAddRecipientsStep1").click(function () {
            $.mobile.changePage(localStorage.AddRecipientBackTo);
        });
    };

    var setupClassess = function () {
        RecipientModel = Backbone.Model.extend({

            validate: function (attrs) {
                if (!attrs.firstName)
                    return "First Name is mandatory";
                if (!attrs.lastName)
                    return "Last Name is mandatory";
                if (!attrs.title || attrs.title == "-1")
                    return "Title is mandatory";

            }
        });
    }
    var setupValidation = function () {

        formSubmit.validate({
            rules: {
                selectTitle: "required",
                firstName: "required",
                lastName: "required"
            },
            messages: {
                selectTitle: "title is required",
                firstName: "First Name is required",
                lastName: "Last name is required"

            },

            errorElement: "em",
            errorPlacement: function (error, element) {

                error.addClass("error-form");

                element.parent().parent().append(error);
            },

            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
            }
        });
    };

    var init = function ($view, $mainModule, $context) {
        view = $view;
        context = $context;
        recipeModule = $mainModule;
        firstName = view.find("#firstName");
        lastName = view.find("#lastName");
        middleName = view.find("#middleName");
        formSubmit = view.find("#formSubmit");
        setupClassess();
        setHandlers();

        setupValidation();
    };

    var render = function () {

    };

    return {
        init: init,
        render: render,
        setDisplayError: function (showErr) {
            showError = showErr;
        }
    };
})();


$(document).on('pageshow', '#addRecipientStep1', function (e) {

    var view = $("#addRecipientStep1");

    var app = jnbs.addRecipients1;
    app.init(view, jnbs.addRecipients, {});
    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));


    app.render();

    if (sessionStorage.personFirstName != undefined) {
        $('#addRecipientStep1 #firstName').val(sessionStorage.personFirstName);
        $('#addRecipientStep1 #lastName').val(sessionStorage.personLastName);
        $('#addRecipientStep1 #middleName').val(sessionStorage.personMiddleName);        
        $("#addRecipientStep1 #selectTitle").val(sessionStorage.personTitle);
        $("#addRecipientStep1 #selectTitle").selectmenu("refresh");
        $("#addRecipientStep1 #selectTitle").change();
    }
});
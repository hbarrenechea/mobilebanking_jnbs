﻿jnbs.accountPayment = jnbs.accountPayment || {};


jnbs.accountPayment.models = (function () {
    var viewModelStep1 = Backbone.Model.extend({
        defaults: {
            institutionId: "",
            accountCountryId: "",
            accountNumber: "",
            accountType: "",
            accountCurrency: "",
            institutionBranch: ""
        },

        validate: function (attrs) {
            if (!attrs.institutionId || attrs.institutionId == "-1")
                return "Institution is a mandatory field";
            if (!attrs.accountNumber)
                return "Account number is a mandatory field";
            if (!attrs.accountType || attrs.accountType == "-1")
                return "Account Type is a mandatory field";

            //if (!attrs.accountType)
            //    return "Account Type is a mandatory field";


        }
    });

    return {
        ViewModelStep1: viewModelStep1
    };
})();

jnbs.accountPayment.addAccountStep1 = (function () {
    var tmpItemAccounType = _.template("<option <%= other %> value='<%= value %>'><%= description %></option>"),
        vcCurrency = null,
        vcCountries = null,
    showError, view, init, setHandlers, saveAccountTypes, cbBranch,
    populateInstitution, populateInstitutionType, getAccountTypes, populateInstitutionBranch,
        cbInstitution, cbAccountType, handlerSelectInstitution, saveDataView,
        populateCountry, populateCurrency;

    //saveDataView = function (id,data) {
    //    CustomStorage.session.add(id, JSON.stringify(data));
    //}

    setHandlers = function () {

        view.find("#btnNext1").click(function () {

            var cbCountry = view.find('#cbCountry');

            var model = new jnbs.accountPayment.models.ViewModelStep1({
                institutionId: cbInstitution.find("option:selected").val(),
                accountCountryId: cbCountry.find("option:selected").val()||"",
                accountNumber: view.find("#accountNumber").val(),
                accountType: cbAccountType.find("option:selected").val(),
                accountCurrency: view.find("#cbAccountCurrency").find("option:selected").val()||"",
                institutionBranch: cbBranch.find("option:selected").val()||""
            });

            if (model.isValid()) {

                sessionStorage.accountAccountNumber = $('#addAccountDataStep1 #accountNumber').val();
                
                sessionStorage.accountCbInstitution = $("#addAccountDataStep1 #cbInstitution").val();
                sessionStorage.accountCbCountry = $("#addAccountDataStep1 #cbCountry").val();
                sessionStorage.accountCbAccountType = $("#addAccountDataStep1 #cbAccountType").val();
                sessionStorage.accountCbAccountCurrency = $("#addAccountDataStep1 #cbAccountCurrency").val();
                sessionStorage.accountCbBranch = $("#addAccountDataStep1 #cbBranch").val();

                jnbs.accountPayment.saveData.saveLocalData("addAccountStep1",model.attributes);
                //recipeModule.addData(modelInput.attributes);
                $.mobile.changePage("addAccountDataStep2View.html");
            }
            else {
                showError(model.validationError);
                console.log(model.validationError);
            }
        });
        view.find("#addAccountDataCloseStep1").click(function () {
            $.mobile.changePage(localStorage.AddRecipientBackTo);
        });
    };
    populateCurrency = function () {

        TransferHelpers.fillCurrency(view.find("#cbAccountCurrency"), null, null, null, vcCurrency);

    };

    populateCountry = function () {
        vcCountries = vcCountries || new ViewConector({
            container: view.find('#cbCountry'),
            viewContentType: ViewContentType.SELECT,
            actionName: 'CountryLookup',
            actionParameter: null,
            typeAction: "GET",
            configSettings: { value: "CountryCode", text: "CountryName" }
        });

        vcCountries.execute(null, function (error) {
            showError("error when try to loading country");
        });
    };
    saveAccountTypes = function (lst) {
        CustomStorage.session.add("InstitutionAccountTypes", JSON.stringify(lst));
    };
    getAccountTypes = function () {
        return CustomStorage.session.get("InstitutionAccountTypes", function (data) {
            return JSON.parse(data);
        });

    }

    populateInstitutionType = function (list) {

        cbAccountType.empty();
        cbAccountType.append(tmpItemAccounType({ value: -1, description: "Select", other: "disabled selected" }));
        list.map(function (item) {
            return tmpItemAccounType({
                value: item.AccountTypeId,
                description: item.AccountTypeName,
                other: ""
            });
        }).forEach(function (item) {
            cbAccountType.append(item);
        });

        cbAccountType.val("-1").change();
    };
    populateInstitutionBranch = function (list) {

        cbBranch.empty();
        cbBranch.append(tmpItemAccounType({ value: -1, description: "Select", other: "disabled selected" }));
        list.map(function (item) {
            return tmpItemAccounType({
                value: item.BranchId,
                description: item.BranchName,
                other: ""
            });
        }).forEach(function (item) {
            cbBranch.append(item);
        });

        cbBranch.val("-1").change();
    };
    populateInstitution = function () {
        var paramGet = {
            institutionTypeId: Enumerations.getInstitutionTypeValueByName("Financial"),
            countryId: (localStorage.CountryCode.toUpperCase() == "AR" ? "JM" : localStorage.CountryCode)
        };
        var vcInstitution = new ViewConector({
            container: cbInstitution,
            viewContentType: ViewContentType.SELECT,
            actionName: 'SystemConfiguration?' + $.param(paramGet),
            actionParameter: null,
            typeAction: "GET",
            configSettings: { value: "InstitutionId", text: "InstitutionName" }
        });

        vcInstitution.execute(function (response) {
            var filtered = response.map(function (item) {
                return _.pick(item, "InstitutionId", "InstitutionAccountType", "InstitutionBranches");
            });
            saveAccountTypes(filtered);
            handlerSelectInstitution();
            cbInstitution.change();
            //  cbAccountType.change();
            //console.log(response);
            // populateInstitutionType(response.)

            $("#addAccountDataStep1 #cbAccountType").val(sessionStorage.accountCbAccountType);
            $("#addAccountDataStep1 #cbAccountType").selectmenu("refresh");
            $("#addAccountDataStep1 #cbAccountType").change();

            $("#addAccountDataStep1 #cbAccountCurrency").val(sessionStorage.accountCbAccountCurrency);
            $("#addAccountDataStep1 #cbAccountCurrency").selectmenu("refresh");
            $("#addAccountDataStep1 #cbAccountCurrency").change();

            $("#addAccountDataStep1 #cbBranch").val(sessionStorage.accountCbBranch);
            $("#addAccountDataStep1 #cbBranch").selectmenu("refresh");
            $("#addAccountDataStep1 #cbBranch").change();

        }, function (error) {
            showError("error when try to loading institutions");
        });
    };
    handlerSelectInstitution = function () {
        cbInstitution.on('change', function () {
            var value = $(this).val();
            var result = _.chain(getAccountTypes())
                .filter(function (item) {
                    return item.InstitutionId == value;
                }).first().value();


            populateInstitutionType(result.InstitutionAccountType);
            populateInstitutionBranch(result.InstitutionBranches);

        });
    };
    init = function ($view) {
        view = $view;
        cbInstitution = view.find('#cbInstitution');
        cbAccountType = view.find('#cbAccountType');
        cbBranch = view.find('#cbBranch');

        populateInstitution();
        setHandlers();
        populateCurrency();
        populateCountry();
    }
    return {
        setDisplayError: function (err) {
            showError = err;
        },
        init: init,
    
    };
})();


$(document).on('pageshow', '#addAccountDataStep1', function (e) {

    var view = $("#addAccountDataStep1");

    var appPage1 = jnbs.accountPayment.addAccountStep1;

    appPage1.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    appPage1.init(view);

    if (sessionStorage.accountAccountNumber != undefined) {
        $('#addAccountDataStep1 #accountNumber').val(sessionStorage.accountAccountNumber);        

        $("#addAccountDataStep1 #cbInstitution").val(sessionStorage.accountCbInstitution);
        $("#addAccountDataStep1 #cbInstitution").selectmenu("refresh");
        $("#addAccountDataStep1 #cbInstitution").change();

        $("#addAccountDataStep1 #cbCountry").val(sessionStorage.accountCbCountry);
        $("#addAccountDataStep1 #cbCountry").selectmenu("refresh");
        $("#addAccountDataStep1 #cbCountry").change();

        
    }

});
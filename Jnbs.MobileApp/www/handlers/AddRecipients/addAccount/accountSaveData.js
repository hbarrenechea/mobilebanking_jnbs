﻿jnbs.accountPayment = jnbs.accountPayment || {};
jnbs.accountPayment.saveData = (function () {
    var saveDataView = function (id, data) {
        CustomStorage.session.add(id, JSON.stringify(data));
    };
    var getLocalData = function (id) {
        return CustomStorage.session.get(id, function (d) {
            return JSON.parse(d);
        });

    };
    var post = function (data, callSuccess, callError) {
        var modelObject = {
            "Account": {
                "MTSId": null,
                "AccountNumber": data.account.accountNumber,
                "AccountType": data.account.accountType,
                "PrimAccountHolderFirstName": data.accountHolder1.firstName,
                "PrimAccountHolderSurname": data.accountHolder1.lastName,
                "Email": null,
                "SecAccountHolderFirstName": data.accountHolder2.firstName,
                "SecAccountHolderSurname": data.accountHolder2.lastName,
                "AccountCurrency": data.account.accountCurrency,
                "AccountInstitution": data.account.institutionId,
                "Gender": null,
                "MobileNumber": null,
                "WorkNumber": null,
                "DOB": null,
                "OldAccountNumber": null,
                "OtherInstitutionName": null,
                "InstitutionBranchId": data.account.institutionBranch,
                "TelephoneNumber": data.accountHolder1.telephone,
                "Address": {
                    "Address1": data.accountHolder1.address,
                    "Address2": data.accountHolder1.cityOrTown,
                    "Address3": data.accountHolder1.stateId,
                    "Address4": data.accountHolder1.postalCode,
                    "AddressCountry": (data.accountHolder1.countryId.toUpperCase() == "AG" ? "JM" : data.accountHolder1.countryId)
                }
            },

            "SenderMTSId": localStorage.MtsId,
            "RecipientMTSId": null
        };

        var urlService = "PreviousRecipientAccounts";


        ServiceConnection.getData({
            actionName: urlService,
            formData: JSON.stringify(modelObject),
            typeAction: "POST",
            headerContentType: "application/json",
            successCallback: function (response, status, xhr) {
                callSuccess(response);

            },
            errorCallback: function (dataError) {

                callError(dataError);
                //  showError("has error when try save Bill");
            }
        });
    };

    return {
        saveLocalData: saveDataView,
        saveRemote: post,
        getLocalData: getLocalData
    };
})();
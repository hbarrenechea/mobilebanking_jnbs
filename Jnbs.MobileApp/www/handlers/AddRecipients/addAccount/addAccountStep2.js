﻿jnbs.accountPayment.addAccountStep2 = (function () {
    var showError, view, setupValidation, formSubmit;
   
    setupValidation=function () {

        formSubmit.validate({
            rules: {
                telephone: "required",
                firstName: "required",
                lastName: "required"
            },
            messages: {
                telephone: "telephone is required",
                firstName: "First Name is required",
                lastName: "Last name is required"

            },

            errorElement: "em",
            errorPlacement: function (error, element) {

                error.addClass("error-form");

                element.parent().parent().append(error);
            },

            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
            }
        });
    }

    var init = function ($view) {
        view = $view;
       
        formSubmit = view.find("#formSubmit");
        setupValidation();
        view.find("#btnNext2").click(function () {
            if (formSubmit.valid() == false) return;

            var firstName = view.find("#firstName").val();
            var lastName = view.find("#lastName").val();
            var telephone = view.find("#telephone").val();
            sessionStorage.accountfirstName = firstName;
            sessionStorage.accountlastName = lastName;
            sessionStorage.accounttelephone = telephone;
            jnbs.accountPayment.saveData.saveLocalData("addAccountStep2", {
                firstName: firstName,
                lastName: lastName,
                telephone: telephone
            });

            $.mobile.changePage("addAccountDataStep3View.html");
        });

    }
    return {
        setDisplayError: function (err) {
            showError = err;
        },
        init: init
    };
})();

$(document).on('pageshow', '#addAccountDataStep2', function (e) {

    var view = $("#addAccountDataStep2");

    view.find("#addAccountDataCloseStep2").click(function () {
        $.mobile.changePage(localStorage.AddRecipientBackTo);
    });
    var appPage1 = jnbs.accountPayment.addAccountStep2;

    appPage1.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    appPage1.init(view);

    if (sessionStorage.accountAccountNumber != undefined) {
        $('#addAccountDataStep2 #firstName').val(sessionStorage.accountfirstName);
        $('#addAccountDataStep2 #lastName').val(sessionStorage.accountlastName);
        $('#addAccountDataStep2 #telephone').val(sessionStorage.accounttelephone);       
    }

});
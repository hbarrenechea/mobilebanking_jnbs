﻿jnbs.accountPayment.addAccountStep4 = (function () {
    var showError, view, setupValidation, formSubmit;

    setupValidation = function () {

        formSubmit.validate({
            rules: {
                firstName: "required",
                lastName: "required"
            },
            messages: {
                firstName: "First Name is required",
                lastName: "Last name is required"

            },

            errorElement: "em",
            errorPlacement: function (error, element) {

                error.addClass("error-form");

                element.parent().parent().append(error);
            },

            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
            }
        });
    }

    var init = function ($view) {
        view = $view;

        formSubmit = view.find("#formSubmit");
        setupValidation();
        view.find("#btnSave").click(function () {
            if (formSubmit.valid() == false) return;
            var md = jnbs.accountPayment.saveData;
            var firstName = view.find("#firstName").val();
            var lastName = view.find("#lastName").val();
            sessionStorage.accountfirstName2 = $('#addAccountDataStep4 #firstName').val();
            sessionStorage.accountlastName2 = $('#addAccountDataStep4 #lastName').val();
            //md.saveData.saveLocalData("addAccountStep4", {
            //    firstName: firstName,
            //    lastName: lastName,
            //});

            var accountHolder1 = {};
            accountHolder1 = _.extend(accountHolder1, md.getLocalData("addAccountStep2"), md.getLocalData(
                "addAccountStep3"));
            var data =
            {
                account: md.getLocalData("addAccountStep1"),
                accountHolder1: accountHolder1,
                accountHolder2: {
                    firstName: firstName,
                    lastName: lastName
                }

            };
            //  $.mobile.changePage("addAccountDataStep3View.html");
            Helpers.progressBox.start("processing");
            jnbs.accountPayment.saveData.saveRemote(data, function (response) {
                Helpers.progressBox.stop();

                if (response.Success == true) {

                    $.mobile.changePage("../AddRecipientsMainMenu.html");
                }
                else {
                    showError(response.Message || "Error");
                }
            }, function (err) {
                Helpers.progressBox.stop();
                showError("has error when try save a new Account institution");
                console.log("Error saving new Account: " + JSON.stringify(err));

            });
        });

    };

    return {
        setDisplayError: function (errFx) {
            showError = errFx;
        },
        init: init
    };
})();


$(document).on('pageshow', '#addAccountDataStep4', function (e) {

    var view = $("#addAccountDataStep4");
    view.find("#addAccountDataCloseStep4").click(function () {
        $.mobile.changePage(localStorage.AddRecipientBackTo);
    });
    var appPage1 = jnbs.accountPayment.addAccountStep4;

    appPage1.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    appPage1.init(view);

    if (sessionStorage.accountfirstName != undefined) {
        $('#addAccountDataStep4 #firstName').val(sessionStorage.accountfirstName2);
        $('#addAccountDataStep4 #lastName').val(sessionStorage.accountlastName2);
    }
});
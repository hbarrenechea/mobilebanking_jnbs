﻿jnbs.accountPayment.addAccountStep3 = (function () {
    var showError = null, view = null, formSubmit,
        populateStateByCountry, setupValidation, populateCountry;

    setupValidation = function () {

        formSubmit.validate({
            rules: {
                address: "required",
                cityOrTown: "required",
                selectCountry: "required"
            },
            messages: {
                address: "address is required",
                cityOrTown: "city Name is required",
                selectCountry: "country is required"

            },

            errorElement: "em",
            errorPlacement: function (error, element) {

                error.addClass("error-form");

                element.parent().parent().append(error);
            },

            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
            }
        });
    };
    populateCountry = function () {
        var vcCountries = new ViewConector({
            container: view.find('#selectCountry'),
            viewContentType: ViewContentType.SELECT,
            actionName: 'CountryLookup',
            actionParameter: null,
            typeAction: "GET",
            configSettings: { value: "CountryCode", text: "CountryName" }
        });

        vcCountries.execute(null, function (error) {
            showError("error when try to loading country");
        });
    };
    populateStateByCountry = function (countryId) {
        if (countryId == "-1") return;
        var p1 = $.param({ countryId: countryId });
        var urlBase = "CountryLookup";
        var urlService = urlBase + "?" + p1;
        // <option value="-1" disabled selected>Select</option>
        var cb = view.find('#selectStates');
        cb.empty();
        cb.append('<option value="-1" disabled selected>Select</option>');
        var vc = new ViewConector({
            container: view.find('#selectStates'),
            viewContentType: ViewContentType.SELECT,
            actionName: urlService,
            actionParameter: null,
            typeAction: "GET",
            configSettings: { value: "StateId", text: "StateName" }
        });

        vc.execute(function (resp) {
            if (resp && Array.isArray(resp) && resp.length == 0) {
                showError("The selected country not associated states");
            }
        }, function (error) {
            showError("error when try to loading states");
        });
    };

    var init = function ($view) {
        view = $view;
        formSubmit = view.find("#formSubmit");
        populateCountry();
        setupValidation();
        view.find("#selectCountry").change(function () {
            var countryId = view.find("#selectCountry option:selected").val();
            populateStateByCountry(countryId);
        });

        view.find("#btnNext3").click(function () {
            if (formSubmit.valid() == false) return;

            var currentData = {
                countryId: view.find("#selectCountry option:selected").val(),
                stateId: view.find("#selectStates option:selected").val(),
                address: view.find("#address").val()||"",
                cityOrTown: view.find("#cityOrTown").val(),
                postalCode: view.find("#postalCode").val()||""

            };

            sessionStorage.accountAddress = $('#addAccountDataStep3 #address').val();
            sessionStorage.accountCityOrTown = $('#addAccountDataStep3 #cityOrTown').val();
            sessionStorage.accountPostalCode = $('#addAccountDataStep3 #postalCode').val();
            sessionStorage.accountAddress = $('#addAccountDataStep3 #address').val();

            jnbs.accountPayment.saveData.saveLocalData("addAccountStep3", currentData);
            $.mobile.changePage("addAccountDataStep4View.html");
        });
    };
    return {
        setDisplayError: function (errFx) {
            showError = errFx;
        },
        init: init
    };
})();




$(document).on('pageshow', '#addAccountDataStep3', function (e) {

    var view = $("#addAccountDataStep3");
    view.find("#addAccountDataCloseStep3").click(function () {
        $.mobile.changePage(localStorage.AddRecipientBackTo);
    });
    var appPage1 = jnbs.accountPayment.addAccountStep3;

    appPage1.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    appPage1.init(view);

    if (sessionStorage.accountAccountNumber != undefined) {
        $('#addAccountDataStep3 #address').val(sessionStorage.accountAddress);
        $('#addAccountDataStep3 #cityOrTown').val(sessionStorage.accountCityOrTown);
        $('#addAccountDataStep3 #postalCode').val(sessionStorage.accountPostalCode);
        $('#addAccountDataStep3 #address').val(sessionStorage.accountAddress);

        $("#addAccountDataStep3 #selectCountry").val(sessionStorage.accountSelectCountry);
        $("#addAccountDataStep3 #selectCountry").selectmenu("refresh");
        $("#addAccountDataStep3 #selectCountry").change();

        $("#addAccountDataStep3 #selectStates").val(sessionStorage.accountSelectStates);
        $("#addAccountDataStep3 #selectStates").selectmenu("refresh");
        $("#addAccountDataStep3 #selectStates").change();
    }
});
﻿jnbs.recipientDetails = (function() {
    var view, listModule,DetailsView;
    var setupClasses = function() {
        DetailsView = Backbone.View.extend({
            tagName: "li",
            className: "ui-li-has-thumb ui-nodisc-icon ui-alt-icon",
            template: Handlebars.compile(view.find("#recipient-details-template").html()),
            render:function() {
                var dataHtml = this.template(this.model.attributes);
                this.$el.html(dataHtml);
                return this;
            }
        });
    };
    var init = function($view, $ListModule) {
        view = $view;
        listModule = $ListModule;
        setupClasses();
    };

    var render = function () {
        var model = listModule.getModel().RecipientModel;
        var currentModel = new model(listModule.getRecipientSelected());
        var detailView = new DetailsView({ model: currentModel });
       var html = detailView.render().el;
       view.find("#content-detail").html(html);
   }
    return {
        init: init,
        render:render
    };
})();


$(document).on('pageshow', '#recipientDetails', function (e) {

    var view = $("#recipientDetails");

    var app = jnbs.recipientDetails;
   
    app.init(view, jnbs.recipientList);
    //app.setHandlers();
    app.render();
});
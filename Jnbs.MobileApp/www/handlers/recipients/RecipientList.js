﻿jnbs.recipientList = (function () {
    var showError, view, context, RecipientModel, RecipientList,
        RecipientView, RecipientListView, setupObjectsMVC,
        responseRecipient;
    //var showError=function(msg) {
    //    ErrorBox.show(view.find("#errorContent"), msg);
    //}
    var saveCurrentRecipient = function (data) {
        sessionStorage.recipientSelected = JSON.stringify(data);
    };
    var init = function ($view) {
        view = $view;
        setupObjectsMVC();
        Helpers.setUserNameInMenu(view);
    };

    setupObjectsMVC = function () {
        RecipientModel = Backbone.Model.extend({
            defaults: {
                nameRecipient: "",
                mailRecipient: "",
                phone: "",
                address: {},
                countryName: ""
            }

        });
        RecipientList = Backbone.Collection.extend({ model: RecipientModel });
        RecipientView = Backbone.View.extend({
            template: Handlebars.compile(view.find("#item-recipient-template").html()),
            tagName: "li",
            className: "ui-li-has-thumb ui-nodisc-icon ui-alt-icon",
            events: {
                "click": "showDetails"
            },
            render: function () {
                var dataHtml = this.template(this.model.attributes);
                this.$el.html(dataHtml);
                return this;
            },
            showDetails: function (e) {
                // showError( "hubo un error");
                saveCurrentRecipient(this.model.attributes);
                $.mobile.changePage("RecipientDetails.html");
            }
        });


        RecipientListView = Backbone.View.extend({
            el: view.find("#recipientsContainer"),
            render: function () {
                var that = this;
                that.$el.empty();
                this.collection.forEach(function (row) {
                    var recipView = new RecipientView({ model: row });
                    var htmlItem = recipView.render().el;
                    that.$el.append(htmlItem);
                });
            }
        });
    };
    var setHandlers = function () {
        view.find("#btnRefresh").click(function () {
            handlerEventsBuss.refreshAll();
        });
    };
    var drawRecipients = function (listRecipientsRaw, callbackEnd) {
        var listModel = listRecipientsRaw.map(function (item) {
            var address = item.Address;
            var getAddressItems = function () {
                // var arr = [];

                return {
                    address1: address.Address1 || "-",
                    address2: address.Address2 || "-",
                    address3: address.Address3 || "-",
                    address4: address.Address4 || "-",
                    addressCountry:address.AddressCountry||"-"
                };
            };
            var model = new RecipientModel({
                nameRecipient: (item.FirstName || "-") + " " + (item.LastName || "-"),
                mailRecipient: item.EmailAddress || "-",
                phone: item.ContactTelephone1 || "-",
                address: getAddressItems(),
                countryName: address.AddressCountry || "-"
            });
            return model;
        });

        var list = new RecipientList(listModel);
        var mainView = new RecipientListView({ collection: list });
        mainView.render();

        if (callbackEnd) callbackEnd();
    };
    var populateWithRecipients = function (callBack) {
        if (CustomStorage.session.exist(keyRecipientListSession)) {
            var response = JSON.parse(CustomStorage.session.get(keyRecipientListSession));
            callBack(response, function () {
                Helpers.progressBox.stop();
            });

            return;
        }

        var urlBase = "previousrecipients";
        var urlService = urlBase + "?" + $.param({ personId: localStorage.MtsId, searchStatus: 2 });
        Helpers.progressBox.start("getting recipients");
        ServiceConnection.getData({
            actionName: urlService,
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {
                  CustomStorage.session.add(keyRecipientListSession, JSON.stringify(response));
                callBack(response, function () {
                    Helpers.progressBox.stop();
                });
                // console.log(response);

            },
            errorCallback: function (dataError) {
                //alert();
                Helpers.progressBox.stop();

                showError("there was an error when attempting to load recipients");
            }
        });
    };
    var render = function () {
        populateWithRecipients(drawRecipients);
    };
    var getRecipientSelected = function () {
        return JSON.parse(sessionStorage.recipientSelected);
    };
    var exportModel = function () {
        return {
            RecipientModel: RecipientModel
        };
    }
    return {
        init: init,
        setHandlers: setHandlers,
        render: render,
        getRecipientSelected: getRecipientSelected,
        getModel: exportModel,
        setDisplayError: function (showErr) {
            showError = showErr;
        }
    };
})();

//recipient-list
$(document).on('pageshow', '#recipient-list', function (e) {

    var view = $("#recipient-list");

    var app = jnbs.recipientList;
    app.init(view, jnbs.addRecipients, {});
    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    app.setHandlers();
    app.render();
});
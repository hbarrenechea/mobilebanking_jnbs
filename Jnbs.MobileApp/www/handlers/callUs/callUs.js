﻿
$(document).on('pageshow', '#callUs', function (e) {

    var vcTelephone =  new ViewConector({
        actionName: "NSystemConfiguration?countryid="+localStorage.CountryCode+"&regionid=", // agregar url
        actionParameter: null,
        viewContentType: ViewContentType.FORM,
        container: $('#tel-form'),
        typeAction: 'GET',
        messageLoading: "loading..."
    });
    
    vcTelephone.execute(function (response) {
        $("#tel-link").attr('href', 'tel: +' + response.Phone);
        $("#place").text(localStorage.CountryName);
    }, function () {
        showError("there was an error");
    });
});
﻿
jnbs.home = (function () {
    var view, redirectLogin,
           leftMenu = $("#leftmenu");
    var init = function ($view) {
        view = $view;
    };

    redirectLogin = function () {
        if ($.mobile.activePage.attr('id').indexOf("customerRegisterPage4") > -1)
            executeLogin("../login/postLoginView.html");
        else
            executeLogin("views/login/postLoginView.html");
    };
    var getRefreshToken = function () {
        var url = envOpenIdQA.UrlBase + 'core/connect/token';
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": url,
            "method": "POST",
            "headers": {
                "authorization": "Basic c2FicmE6Z3J1cG9zYWJyYQ==",
                "content-type": "application/x-www-form-urlencoded"
            },
            "data": {
                "grant_type": "authorization_code",
                "code": localStorage.refresh_token,
                "redirect_uri": "http://localhost:21575/index.html"
            }
        }

        $.ajax(settings).done(function (response, status, xhr) {
            var expiresTime = new Date();
            expiresTime.setSeconds(expiresTime.getSeconds() + parseInt(response.expires_in));
            localStorage.expires_in = expiresTime;
            localStorage.access_token = response.access_token;
            localStorage.refresh_token = response.refresh_token;
            console.log(localStorage.access_token + " refresh");
        }).error(function (error) {
            ErrorBox.show($("#errorContent"), "there was a problem with the connection, please try again");
        });

    };

    var executeLogin = function (urlNext) {
        jnbs.ConnectionLogin.login(function (token, code, expires) {
            // redirect to main view
            localStorage.access_token = token;
            localStorage.refresh_token = code;
            var expiresTime = new Date();
            expiresTime.setSeconds(expiresTime.getSeconds() + parseInt(expires));
            localStorage.expires_in = expiresTime;
            getRefreshToken();
            // $.mobile.changePage("../Send/Send.html");
            $.mobile.changePage(urlNext);
        }, function (error, response) {
            alert(error);
        });
    };
    var eventHandler = function () {
        view.find("#btnLogin").click(function () {

            redirectLogin();

        });

        view.find("#tracking").click(function () {

            executeLogin("views/login/postLoginView.html?redirectToHistoryFilter=1");

        });


        leftMenu.find("#btnRefresh").click(function () {
            handlerEventsBuss.refreshAll();
            leftMenu.panel("close");
        });

        leftMenu.find("#btnLogout").click(function () {
            localStorage.clear();
            sessionStorage.clear();
            var urlIndex = "../../index.html";
            $.mobile.changePage(urlIndex);
        });



    };

    return {
        initialize: init,
        setEventHandlers: eventHandler,
        showLoginView: redirectLogin
    };
})();



$(document).one('pageshow', '#home', function (e) {
    var view = $('#home');
    var app = jnbs.home;

    app.initialize(view);

    app.setEventHandlers();

});


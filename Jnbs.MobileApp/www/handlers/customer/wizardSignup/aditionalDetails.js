﻿var vcFrecuency;
var showErrorContext;
var setupValidation, formSubmit;


$(document).on('pageshow', '#aditional-details', function (e) {
  
    populateFrecuency();
    var view = $("#aditional-details");
    formSubmit = view.find("#formSubmit");
    var userPerson = CustomStorage.session.get("personDetail", function (d) { return JSON.parse(d); });
    view.find("#mobileNumber").val(userPerson.ContactTelephone1);
    showErrorContext = ErrorBox.getPartialErrorAction(view.find("#errorContent"))
    view.find("#finish").click(function () {
        if (formSubmit.valid()) {
            var userPerson = CustomStorage.session.get("personDetail", function (d) { return JSON.parse(d); });

            var celu = view.find("#mobileNumber").val();
            var otherTel = view.find("#otherPhoneNumber").val();
            var occupation = view.find("#occupation").val();
            var purp = view.find("#purpose").val();
            var frecuency = view.find("#frecuency option:selected").val();

            userPerson.MobileTelephone = celu || "";
            userPerson.ContactTelephone1 = otherTel || celu;
            userPerson.ExpectedTransactionFrequency = frecuency;
            userPerson.Occupation = occupation;
            userPerson.NatureAndPurposeOfTransfers = purp || "";


            createUser(userPerson);
        };
    });
    setupValidation();
    
});



setupValidation = function () {

    formSubmit.validate({
        rules: {
            mobileNumber: "required",
            occupation: "required",
            frecuency: "required",
            purpose: "required",
        },
        messages: {
            mobileNumber: "Mobile Number is required",
            occupation: "Occupation is required",
            frecuency: "Frecuency date is required",
            purpose: "Purpose is required",
        },

        errorElement: "em",
        errorPlacement: function (error, element) {

            error.addClass("error-form");

            element.parent().parent().append(error);
        },

        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });
};

var populateFrecuency = function () {
    var vcFrecuency = vcFrecuency || new ViewConector({
        container: $('#frecuency'),
        viewContentType: ViewContentType.SELECT,
        actionName: 'RefenceData?referenceType=IntendedFrequencyOfUse',
        actionParameter: null,
        typeAction: "GET",
        configSettings: { value: "Id", text: "Name" }
    });

    vcFrecuency.execute(null, function (error) {
        showErrorContext("error when try to loading frecuency");
    });
}

var createUser = function (recipient) {

    var user = {
        "OpenId": localStorage.OpenIdUser,
        "PersonId": recipient.PersonId,
        "Title": recipient.Title,
        "FirstName": recipient.FirstName,
        "MiddleName": recipient.MiddleName,
        "LastName": recipient.LastName,
        "EmailAddress": recipient.EmailAddress,
        "ContactTelephone1": recipient.ContactTelephone1,
        "ContactTelephone2": recipient.ContactTelephone2,
        "MobileTelephone": recipient.MobileTelephone,
        "Dob": recipient.DOB,
        "PlaceOfBirth": recipient.PlaceOfBirth,
        "CustomerCategory": recipient.CustomerCategory,
        "Address": {
            "Address1": recipient.Address.Address1,
            "Address2": recipient.Address.Address2,
            "Address3": recipient.Address.Address3,
            "Address4": recipient.Address.Address4,
            "AddressCountry": recipient.Address.AddressCountry
        },
        "SourceOfFunds": recipient.SourceOfFunds,
        "Nationality": recipient.Nationality,
        "Occupation": recipient.Occupation,
        "Ssn": recipient.SSN,
        "SsntrnRequired": recipient.SSNTRNRequired,
        "Identification": {
            "IdentificationType": recipient.Identification.IdentificationType,
            "IdentificationNumber": recipient.Identification.IdentificationNumber,
            "IdentificationIssuingCountry": recipient.Identification.IdentificationIssuingCountry,
            "IdentificationIssueDate": recipient.Identification.IdentificationIssueDate,
            "IdentificationExpirationDate": recipient.Identification.IdentificationExpirationDate,
            "AdditionalIdentificationNumber": recipient.Identification.AdditionalIdentificationNumber
        },
        "AddressVerificationDate": recipient.AddressVerificationDate,
        "IdVerificationDate": recipient.IdVerificationDate,
        "AggregatedRiskType": recipient.AggregatedRiskType,
        "MTCardNumber": recipient.MTCardNumber,
        "NatureAndPurposeOfTransfers": recipient.NatureAndPurposeOfTransfers,
        "ExpectedTransactionFrequency": recipient.ExpectedTransactionFrequency,
        "Status": recipient.Status,
        "HasCreditCard": false
    };
    var Type = localStorage.IsDynamiclyLoaded == "true" ? "PUT" : "POST";
    Helpers.progressBox.start();
    ServiceConnection.getData({
        actionName: "emoneyprofile",
        formData: JSON.stringify(user),
        typeAction: Type,
        headerContentType: "application/json",
        successCallback: function (response, status, xhr) {
            Helpers.progressBox.stop();
            $.mobile.changePage("../../login/postLoginView.html");
        },
        errorCallback: function (dataError) {
            Helpers.progressBox.stop();
            showErrorContext("there was an error when attempting to load user");
        }
    });
};

var createAccount = function () {
    var urlService = "accounts?openId=" + localStorage.OpenIdUser
    ServiceConnection.getData({
        actionName: urlService,
        formData: null,
        typeAction: "POST",
        successCallback: function (response, status, xhr) {
            Helpers.progressBox.stop();
            $.mobile.changePage("../../index.html");
        },
        errorCallback: function (dataError) {
            Helpers.progressBox.stop();
            showErrorContext("there was an error when attempting to load user account");
        }
    });
}
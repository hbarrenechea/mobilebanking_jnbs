﻿jnbs.customer.personDetails = (function () {
    var view, person, populate, populateCountry, cbNations,
        cbTitles,
        formSubmit, setupValidation;

    var save = function (data) {
        CustomStorage.session.add("personDetail", JSON.stringify(data));
    };
    populate = function () {
        view.find("#firstName").val(person.FirstName);
        view.find("#middleName").val(person.MiddleName || "");
        view.find("#lastName").val(person.LastName);
        view.find("#titles").val(person.Title).change();
        //view.find("#birth").val(person.birth);
        populateCountry();
    };

    setupValidation = function () {

        formSubmit.validate({
                rules: {
                        titles: "required",
                        firstName: "required",
                        lastName: "required",
                        birth: "required",
                        nations: "required",
        },
                messages: {
                        titles: "Titles is required",
                        firstName: "First name is required",
                        lastName: "Last name is required",
                        birth: "Birth date is required",
                        nations: "Nations is required",
        },

                errorElement: "em",
                errorPlacement: function (error, element) {

                error.addClass("error-form");

                element.parent().parent().append(error);
        },

                highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
                unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
        });
    };

    populateCountry = function () {
        var vcCountries = new ViewConector({
                container: view.find('#nations'),
                viewContentType: ViewContentType.SELECT,
                actionName: 'CountryLookup',
                actionParameter: null,
                typeAction: "GET",
                configSettings: {
                    value: "CountryCode", text: "CountryName"
                }
        });

        vcCountries.execute(function () {
            if (person.Nationality)
                cbNations.val(person.Nationality).change();


        }, function (error) {
            //  showError("error when try to loading country");
        });
    };
    var init = function ($view, $person) {
        view = $view;
        cbTitles = view.find("#titles");
        formSubmit = view.find("#formSubmit");
        cbNations = view.find("#nations");
        person = $person;
        populate();
        view.find("#next").click(function () {
            if (formSubmit.valid()) {
                person.Title = cbTitles.find("option:selected").val();
                person.FirstName = view.find("#firstName").val();
                person.MiddleName = view.find("#middleName").val();
                person.LastName = view.find("#lastName").val();
                person.Nationality = cbNations.find("option:selected").val();
                person.DOB = view.find("#birth").val();
                var mailDefault = jnbs.customer.questionOne.getDataPerson().userLogged.email;
                person.EmailAddress = person.EmailAddress || mailDefault;
                save(person);

                $.mobile.changePage("address.html");
        };
        });
        setupValidation();
    };
    return {
            init: init
    };
})();

$(document).on('pageshow', '#registerPersonDetails', function (e) {
    var view = $("#registerPersonDetails");

    jnbs.customer.personDetails.init(view, jnbs.customer.questionTwo.getDataPersonDetail());
});
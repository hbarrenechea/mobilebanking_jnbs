﻿jnbs.customer.questionTwo = (function () {
    var save, view, userData, showError,
        sendTransactionTest, get, setupValidation, formSubmit;

    get = function () {
        return CustomStorage.session.get("personDetail", function (d) { return JSON.parse(d); });
    };

    save = function (data) {
        CustomStorage.session.add("personDetail", JSON.stringify(data));
    };

    setupValidation = function () {

        formSubmit.validate({
            rules: {
                transactionNumber: "required",
                transactionAmount: "required",
            },
            messages: {
                transactionNumber: "Transaction Number is required",
                transactionAmount: "Transaction Amount is required",
            },

            errorElement: "em",
            errorPlacement: function (error, element) {

                error.addClass("error-form");

                element.parent().parent().append(error);
            },

            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
            }
        });
    };

    sendTransactionTest = function (transactionId, amount) {

      //  firstName=Ramiro&lastName=Allen&transactionId=4204845863&amount=12.00
        var url = "emoneyprofile?" + $.param({
            firstName: userData.userLogged.given_name,
            lastName: userData.userLogged.family_name,
            transactionId: transactionId,
            amount: amount
        });

        Helpers.progressBox.start();
        ServiceConnection.getData({
            actionName: url,
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {
                Helpers.progressBox.stop();
                console.log(JSON.stringify(response));

                save(response);

                //firstName && lastName
                //CustomStorage.session.add("preRegisterUserData", JSON.stringify(data));
                $.mobile.changePage("personalDetails.html");
                localStorage.IsDynamiclyLoaded = true;

            },
            errorCallback: function (dataError) {

                Helpers.progressBox.stop();
                //customErrorDisplay("post-register: error calling userInfo");
                ErrorBox.show(view.find("#errorContent"),  "error calling userInfo");
                console.log(textError(dataError));

                toogleButtonAgain(true);


            }
        });

    };
    var init = function ($view, $userData) {
        view = $view;
        formSubmit = view.find("#formSubmit");
        userData = $userData;//CustomStorage.session.get("preRegisterUserData", function (d) { return JSON.parse(d); });
        view.find("#btnNext").click(function () {
            if (formSubmit.valid()) {
                var trId = view.find("#transactionNumber").val();
                var amount = view.find("#transactionAmount").val();

                sendTransactionTest(trId, amount);
            };
        });
        setupValidation();

    };
    return {
        init: init,
        getDataPersonDetail: get,
        setDisplayError: function (showErr) {
            showError = showErr;
        }
    };

})();

$(document).on('pageshow', '#registerQuestion2', function (e) {
    var view = $("#registerQuestion2");
    //jnbs.customer.questionTwo.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    jnbs.customer.questionTwo.init(view, jnbs.customer.questionOne.getDataPerson());
});
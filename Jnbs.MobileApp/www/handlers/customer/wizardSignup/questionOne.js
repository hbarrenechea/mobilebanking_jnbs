﻿//NATHANIEL   NATION          1234567890123456
//WENDY         GABAY            3216549870123456
//ANNMARIE   BUCHANON     4044223578967525
//WENDY         GABAY             6985478590123456

jnbs.customer.questionOne = (function () {
    var view, setupValidation, formSubmit, getDataPersonDetail,
        userData,
        initPerson;

    var getDataPerson = function () {

        return CustomStorage.session.get("preRegisterUserData", function (d) { return JSON.parse(d); });
    };
    initPerson = function (userData) {
       
        var json = '{"OpenId":null,"PersonId":null,"Title":"' + userData.userLogged.nickname + '","FirstName":"' + userData.userLogged.given_name + '","MiddleName":null,"LastName":"' + userData.userLogged.family_name + '","EmailAddress":"' + userData.userLogged.email + '","ContactTelephone1":"' + userData.userLogged.phone_number + '","ContactTelephone2":null,"MobileTelephone":null,"DOB":null,"PlaceOfBirth":null,"CustomerCategory":2,"Address":{"Address1":null,"Address2":null,"Address3":null,"Address4":null,"AddressCountry":null},"SourceOfFunds":null,"Nationality":null,"Occupation":"","SSN":null,"SSNTRNRequired":false,"Identification":{"IdentificationType":0,"IdentificationNumber":null,"IdentificationIssuingCountry":null,"IdentificationIssueDate":"0001-01-01T00:00:00","IdentificationExpirationDate":"0001-01-01T00:00:00","AdditionalIdentificationNumber":null},"AddressVerificationDate":null,"IdVerificationDate":null,"AggregatedRiskType":null,"MTCardNumber":"","NatureAndPurposeOfTransfers":"","ExpectedTransactionFrequency":null,"Status":null}';
        save(JSON.parse(json));
    };
    var getInfo = function () {

        var url = "emoneyprofile?" + $.param({
            firstName: userData.userLogged.given_name,
            lastName: userData.userLogged.family_name,
            loyaltyCard: $("#card-number").val(), 
            searchType: 0
        });
        Helpers.progressBox.start();
        ServiceConnection.getData({
            actionName: url,
            formData: null,
            typeAction: "GET",
            successCallback: function (response, status, xhr) {
                console.log(JSON.stringify(response));
                save(response);
                $.mobile.changePage("personDetails.html");
                localStorage.IsDynamiclyLoaded = true;
            },
            errorCallback: function (dataError) {
                Helpers.progressBox.stop();
                ErrorBox.show($("#errorContent"), "error getting information")
                console.log(textError(dataError));
            }
        });
    }
    setupValidation = function () {

        formSubmit.validate({
            rules: {
                "card-number": "required",
            },
            messages: {
                "card-number": "Card number is required",
            },

            errorElement: "em",
            errorPlacement: function (error, element) {

                error.addClass("error-form");

                element.parent().parent().append(error);
            },

            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
            }
        });
    };

    var init = function ($view) {
        view = $view;
        formSubmit = view.find("#formSubmit");
        userData = getDataPerson();
        initPerson(userData);
        
        view.find("#logout").click(function () {
            $.mobile.changePage("../../../index.html");
        });
        view.find("#question-one-next").click(function () {
            if (formSubmit.valid()) {
                getInfo();
            };
        });
        setupValidation();
    };
    save = function (data) {
        CustomStorage.session.add("personDetail", JSON.stringify(data));
    };

    getDataPersonDetail = function (data) {
        CustomStorage.session.get("personDetail", function (d) { return JSON.parse(d);});
    };
   
    return {
        init: init,
        getDataPerson: getDataPerson,
        getDataPersonDetail:getDataPersonDetail
    };

})();

$(document).on('pageshow', '#registerQuestion1', function (e) {
    localStorage.IsDynamiclyLoaded = false;
    var view = $("#registerQuestion1");
    jnbs.customer.questionOne.init(view);
    
});
﻿jnbs.customer.addressUser = (function () {
    var view, populate, populateCountry, showError,
        populateStateByCountry, person, formSubmit, setupValidation;

    var save = function (data) {
        CustomStorage.session.add("personDetail", JSON.stringify(data));
    };

    setupValidation = function () {

        formSubmit.validate({
            rules: {
                selectCountry: "required",
                selectStates: "required",
                cityOrTown: "required",
                street: "required",
                postalCode: "required",
            },
            messages: {
                selectCountry: "Country is required",
                selectStates: "States is required",
                cityOrTown: "City is required",
                street: "Street is required",
                postalCode: "Postal Code is required",
            },

            errorElement: "em",
            errorPlacement: function (error, element) {

                error.addClass("error-form");

                element.parent().parent().append(error);
            },

            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
            }
        });
    };


    populateCountry = function () {
        var vcCountries = new ViewConector({
            container: view.find('#selectCountry'),
            viewContentType: ViewContentType.SELECT,
            actionName: 'CountryLookup',
            actionParameter: null,
            typeAction: "GET",
            configSettings: { value: "CountryCode", text: "CountryName" }
        });

        vcCountries.execute(null, function (error) {
            showError("error when try to loading country");
        });
    };
    populateStateByCountry = function (countryId) {
        if (countryId == "-1") return;
        var p1 = $.param({ countryId: countryId });
        var urlBase = "CountryLookup";
        var urlService = urlBase + "?" + p1;
        // <option value="-1" disabled selected>Select</option>
        var cb = view.find('#selectStates');
        cb.empty();
        cb.append('<option value="-1" disabled selected>Select</option>');
        var vc = new ViewConector({
            container: view.find('#selectStates'),
            viewContentType: ViewContentType.SELECT,
            actionName: urlService,
            actionParameter: null,
            typeAction: "GET",
            configSettings: { value: "StateId", text: "StateName" }
        });

        vc.execute(function (resp) {
            if (resp && Array.isArray(resp) && resp.length == 0) {
                showError("The selected country not associated states");
            }
        }, function (error) {
            showError("error when try to loading states");
        });
    };

    var init = function ($view, $person) {
        view = $view;
        person = $person;
        formSubmit = view.find("#formSubmit");
        view.find("#selectCountry").change(function () {
            var countryId = view.find("#selectCountry option:selected").val();
            populateStateByCountry(countryId);
        });
        view.find("#next").click(function () {
            if (formSubmit.valid()) {
                person.Address.Address1 = view.find("#street").val();
                person.Address.Address2 = view.find("#cityOrTown").val();
                person.Address.Address3 = view.find("#selectStates option:selected").text();
                person.Address.Address4 = view.find("#postalCode").val();
                person.Address.AddressCountry = view.find("#selectCountry option:selected").val();
                save(person);
                $.mobile.changePage("identification.html");
            };
        });
        populateCountry();
        setupValidation();
    };
    return {
        init: init,
        setDisplayError: function (showErr) {
            showError = showErr;
        }
    };
})();


$(document).on('pageshow', '#registrationCustomAddress', function (e) {
    var view = $("#registrationCustomAddress");
    var app = jnbs.customer.addressUser;
    app.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    app.init(view, jnbs.customer.questionTwo.getDataPersonDetail());

});


﻿jnbs.customer.identification = (function () {
    var view, populate, populateCountry, person,save, setupValidation, formSubmit;

    save = function (data) {
        CustomStorage.session.add("personDetail", JSON.stringify(data));
    };

    setupValidation = function () {

        formSubmit.validate({
            rules: {
                typeDoc: "required",
                numberId: "required",
                issueCountry: "required",
                expireDate: "required",
                issueDate: "required",
            },
            messages: {
                typeDoc: "Type Doc is required",
                numberId: "Number is required",
                issueCountry: "Issuing Country is required",
                expireDate: "Expire date is required",
                issueDate: "Issuing date is required",
            },

            errorElement: "em",
            errorPlacement: function (error, element) {

                error.addClass("error-form");

                element.parent().parent().append(error);
            },

            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
            }
        });
    };

    populateCountry = function () {
        var vcCountries = new ViewConector({
            container: view.find('#issueCountry'),
            viewContentType: ViewContentType.SELECT,
            actionName: 'CountryLookup',
            actionParameter: null,
            typeAction: "GET",
            configSettings: { value: "CountryCode", text: "CountryName" }
        });

        vcCountries.execute(function () {
            //if (person.Nationality)
            //    cbNations.val(person.Nationality).change();


        }, function (error) {
            //  showError("error when try to loading country");
        });
    };
    populate = function () {
        populateCountry();
    };


    var init = function ($view, $person) {
        view = $view;
        person = $person;
        formSubmit = view.find("#formSubmit");
        populate();
        view.find("#next").click(function () {
            if (formSubmit.valid()) {
                person.Identification.IdentificationType = view.find("#typeDoc").val();
                person.Identification.IdentificationNumber = view.find("#numberId").val();
                person.Identification.IdentificationIssuingCountry = view.find("#issueCountry option:selected").val();
                person.Identification.IdentificationIssueDate = view.find("#issueDate").val()||Date.now();
                person.Identification.IdentificationExpirationDate = view.find("#expireDate").val() || Date.now();

                save(person);

                $.mobile.changePage("aditionalDetails.html");
            };
        });
        setupValidation();
    };

    return {

        init: init
    };
}
)();


$(document).on('pageshow', '#identificationRegisterUser', function (e) {
    var view = $("#identificationRegisterUser");

    jnbs.customer.identification.init(view, jnbs.customer.questionTwo.getDataPersonDetail());
});
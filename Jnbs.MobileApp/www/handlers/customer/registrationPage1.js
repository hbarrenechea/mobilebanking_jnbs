jnbs.customer = jnbs.customer || {};
jnbs.customer.registrationPage1 = (function () {
    var view, init, setupValidation, tools,
        formSubmit, setHandlers;


    setHandlers = function () {
        view.find("#nextStep1").click(function () {
            if (formSubmit.valid() == false) return;
            var data = {
                Title: view.find("#selectTitle option:selected").val(),
                First: view.find("#firstName").val(),
                MiddleName: view.find("#middleName").val(),
                Last: view.find("#lastName").val(),
                PhoneNumber: view.find("#phoneNumber").val()
            };

            tools.addDataReg(data);

            $.mobile.changePage("customerRegisterPage2.html");
        });
    };
    setupValidation = function () {

        formSubmit.validate({
            rules: {
                "card-number": "required",
            },
            messages: {
                "card-number": "card number is required",
            },

            errorElement: "em",
            errorPlacement: function (error, element) {

                error.addClass("error-form");

                element.parent().parent().append(error);
            },

            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
            }
        });
    };

    init = function ($view, $tools) {
        view = $view;
        tools = $tools;
        formSubmit = view.find("#formSubmit");
        setupValidation();
        setHandlers();
    };

    return {
        init: init,
        setDisplayError: function (showErr) {
            showError = showErr;
        }
    };
})();

$(document).on('pageshow', '#customerRegisterPage1', function (e) {
    var view = $("#customerRegisterPage1");
    var appPage = jnbs.customer.registrationPage1;
    appPage.setDisplayError(view.find("#errorContent"));
    appPage.init(view, jnbs.customer.registrationTools);


});
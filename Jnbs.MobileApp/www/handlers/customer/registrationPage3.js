﻿jnbs.customer = jnbs.customer || {};
jnbs.customer.registrationPage3 = (function () {
    var setHandlers = null, view = null,
        showError,
        tools = null;

    setHandlers = function () {
        view.find("#btnAgree").click(function () {

          

            //showErrorList(["err1", "err2", "err3"]);
            tools.registerUser(function (resp) {

                if (resp.Succeeded && resp.Succeeded == true) {
                    var userId = resp.Message;
                    tools.saveUserId(userId);
                    $.mobile.changePage("customerRegisterPage4.html");

                } else {
                    var errors = resp.Errors;
                    ErrorBox.show($("#errorContent"), resp.Errors);
                   
                }
            }, function (err) {
                showError("had error when trying to register user");
            });

        });
    };
    init = function ($view, $tools) {
        view = $view;
        tools = $tools;
        //formSubmit = view.find("#formSubmit");
        //setupValidation();
        setHandlers();
    };

    return {
        init: init,
        setDisplayError: function (showErr) {
            showError = showErr;
        }
    };
})();

$(document).on('pageshow', '#customerRegisterPage3', function (e) {
    var view = $("#customerRegisterPage3");
    var appPage = jnbs.customer.registrationPage3;
    appPage.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    appPage.init(view, jnbs.customer.registrationTools);


});
jnbs.customer = jnbs.customer || {};
jnbs.customer.registrationPage2 = (function() {
    var view, init, setupValidation,
        tools,formSubmit, setHandlers;


    setHandlers = function () {
        view.find("#nextStep2").click(function () {
            if (formSubmit.valid() == false) return;

            var data = {
                EmailAddress: view.find("#email1").val(),
                Password: view.find("#password1").val(),
             
            };

            tools.addDataReg(data);
            $.mobile.changePage("customerRegisterPage3.html");
        });
    };
     setupValidation = function () {

        formSubmit.validate({
            rules: {
                email1: {"required":true,email:true},
                email2: { "required": true, email: true,equalTo:"#email1" },
                password1: "required",
                password2: { "required": true, equalTo: "#password1" },
                //textCaptch:"required"
            },
            messages: {
                email1: "email is required",
                email2: "email is required",
                password1: "password is required",
                password2:"password are not equals",
                //textCaptch:"write text of image"
            },

            errorElement: "em",
            errorPlacement: function (error, element) {

                error.addClass("error-form");

                element.parent().parent().append(error);
            },

            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
            }
        });
    };

    init=function($view,$tools){
        view = $view;
        tools=$tools;
        formSubmit = view.find("#formSubmit");
        setupValidation();
        setHandlers();
    };

    return {
        init: init,
        setDisplayError: function (showErr) {
            showError = showErr;
        }
    };
})();

$(document).on('pageshow', '#customerRegisterPage2', function(e) {
    var view = $("#customerRegisterPage2");
    var appPage = jnbs.customer.registrationPage2;
    appPage.setDisplayError(view.find("#errorContent"));
    appPage.init(view,jnbs.customer.registrationTools);


});
﻿jnbs.customer = jnbs.customer || {};
jnbs.customer.registrationPage4 = (function () {
    var setHandlers = null, view = null,
        showError,
        tools = null;

    setHandlers = function () {
        view.find("#confirm").click(function () {

                var code = view.find("#codeSent").val();

            tools.verifyCode(code, function (response) {
                if (response.Succeeded && response.Succeeded == true) {
                    jnbs.home.showLoginView();
                } else {

                    // tools.showErrorList(view.find("#errorContent"),response.Errors)
                    showError(tools.getFirstError(response.Errors));
                }

            }, function () {
                showError("error checking code verification");
            })
        });
    };
    init = function ($view, $tools) {
        view = $view;
        tools = $tools;
        //formSubmit = view.find("#formSubmit");
        //setupValidation();
        setHandlers();
    };

    return {
        init: init,
        setDisplayError: function (showErr) {
            showError = showErr;
        }
    };
})();

$(document).on('pageshow', '#customerRegisterPage4', function (e) {
    var view = $("#customerRegisterPage4");
    var appPage = jnbs.customer.registrationPage4;
    appPage.setDisplayError(ErrorBox.getPartialErrorAction(view.find("#errorContent")));
    appPage.init(view, jnbs.customer.registrationTools);


});
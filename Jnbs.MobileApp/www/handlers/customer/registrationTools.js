﻿jnbs.customer = jnbs.customer || {};
jnbs.customer.registrationTools = (function () {

    var dataId = "dataRegistration", verifyCode,
        get, getUserId;

    getUserId = function () {
        return CustomStorage.session.get("userIdAfterRegister");
    };
    verifyCode = function (code, cbSuccess, cbError) {
        var urlService = "signup?" + $.param({
            userid: getUserId(),
            code: code
        });
        Helpers.progressBox.start("checking code");
        ServiceConnection.getData({
            actionName: urlService,
            // formData: JSON.stringify(data),
            headerContentType: "application/json",
            typeAction: "PUT",
            useHeaderDefault: false,
            successCallback: function (response, status, xhr) {

                Helpers.progressBox.stop();
                console.log("result checking code -> " + JSON.stringify(response));
                cbSuccess(response);

            },
            errorCallback: function (dataError) {

                Helpers.progressBox.stop();
                cbError(dataError);
                console.log("user data param: " + JSON.stringify(data));
                console.log("register error: " + JSON.stringify(dataError));

            }
        });
    };

    var addDataRegistration = function (data) {
        var dataToSave = {};
        if (CustomStorage.session.exist(dataId)) {
            var dataPrev = CustomStorage.session.get(dataId, function (d) { return JSON.parse(d); });
            var dataPrev = _.extend(dataPrev, data);
            dataToSave = dataPrev;
        } else {
            dataToSave = data;
        }

        CustomStorage.session.add(dataId, JSON.stringify(dataToSave));

    };

    get = function () {
        return CustomStorage.session.get(dataId, function (d) {
            return JSON.parse(d);
        });
    };

    var registerUser = function (cbSuccess, cbError) {
        var data = get();
        Helpers.progressBox.start("Recording");
        ServiceConnection.getData({
            actionName: "signup",
            formData: JSON.stringify(data),
            headerContentType: "application/json",
            typeAction: "POST",
            useHeaderDefault: false,
            successCallback: function (response, status, xhr) {

                Helpers.progressBox.stop();
                console.log("result Register-> " + JSON.stringify(response));
                cbSuccess(response);

            },
            errorCallback: function (dataError) {

                Helpers.progressBox.stop();
                cbError(dataError);
                console.log("user data param: " + JSON.stringify(data));
                console.log("register error: " + JSON.stringify(dataError));

            }
        });
    };
    return {
        addDataReg: addDataRegistration,
        //     getDataReg: get,
        clear: function () {
            CustomStorage.session.remove(dataId);
        },

        registerUser: registerUser,
        saveUserId: function (userId) {
            CustomStorage.session.add("userIdAfterRegister", userId);
        },
        verifyCode: verifyCode,
         getFirstError : function (errors) {
             return errors[0] || "-";

            
        }

    };
})();